export default IMAGE ={
    
    isee6:require('./img/isee6.png'),
    currentLocation:require('./img/currentLocation.png'),
    
    logoAll:require('./img/logoAll.png'),
    logoKhac:require('./img/logoKhac.png'),
    logoCafe:require('./img/logoCafe.jpg'),
    logoHomeStay:require('./img/logoHomeStay.jpg'),
    logoRapCP:require('./img/logoRapCP.png'),
    logoTraSua:require('./img/logoTraSua.png'),

    pinCaPhe:require('./img/pinCaPhe.png'),
    pinHomeStay:require('./img/pinHomeStay.png'),
    pinKhac:require('./img/pinKhac.png'),
    pinRapCP:require('./img/pinRapCP.png'),
    pinTraSua:require('./img/pinTraSua.png'),
    iconAccount:require('./img/iconAccount.png'),
};