import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    TextInput,
    Image,
    FlatList,
    Dimensions,
} from 'react-native';

import geolib from "geolib"
import MapView from 'react-native-maps';
import Detail from './Detail';
import styles from './styles';
import { LIST_TRA_SUA, LIST_HOT, LIST_CA_PHE_MAPS, LIST_HOMESTAY_MAPS, LIST_RAP_CP_MAPS, LIST_ALL } from '../../config/default';
import { calCoordinates, calPinColor, calimagePin, WIDTH } from '../../config/Function';
import FluteButtonForMaps from '../../common/FluteButtonForMaps';
import FluteButton from '../../common/FluteButton';
import ExpanderMaps from '../../common/ExpanderMaps';
import PlaceAutoComplete from "./PlaceAutoComplete"
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import RNGooglePlaces from 'react-native-google-places';
import { Button } from '@shoutem/ui';
import SearchButton from './searchBar';
import Icon from 'react-native-vector-icons/MaterialIcons';


var self;
const screen = Dimensions.get('window');

export default class ScreenMaps extends Component {

    constructor(props) {
        super(props);
        self = this;
        this.placeNear = [];
        this.distance = 2000;
        this.state = {
            isLoading: true,
            search: '',
            textString: '',
            listShow: false,
            showDetail: false,
            region: {
                latitude: 20.9822573,
                longitude: 105.7888341,
                latitudeDelta: this.distance * 3 / 111000,
                longitudeDelta: this.distance * 3 / 111000,
            },
            location: {
                latitude: 20.9822573,
                longitude: 105.7888341,
            }
        }
    }

    componentDidMount() {
        return fetch('https://api.jsonbin.io/b/5ce8f524bc2a75194e4d9fcd')
            .then(response => response.json())
            .then(responseJson => {
                self.setState(
                    {
                        isLoading: false,
                        dataSource: responseJson,
                    },
                    function () {
                        self.arrayholder = responseJson;
                    }
                );
            })
            .catch(error => {
                console.error(error);
            });
    }

    SearchFilterFunction(text) {
        self.setState({
            textString: text,
            listShow: true,
        })
        if (text.length == 0) {
            self.setState({
                listShow: false,
            })
            return;
        }
        //passing the inserted text in textinput
        const newData = self.arrayholder.filter(function (item) {
            //applying filter for the inserted text in search bar
            const itemData = item.title ? item.title.toUpperCase() : ''.toUpperCase();
            const textData = text.toUpperCase();
            return itemData.indexOf(textData) > -1;
        });
        self.setState({
            //setting the filtered newData on datasource
            //After setting the data it will automatically re-render the view
            dataSource: newData,
            search: text,
        });
    }
    ListViewItemSeparator = () => {
        //Item sparator view
        return (
            <View
                style={{
                    height: 1,
                    width: '90%',
                }}
            />
        );
    };

    clearList = () => {
        self.setState(
            {
                isLoading: false,
                dataSource: [],
                arrayholder: [],
            },
        );
    }

    hideList = () => {
        self.setState({
            listShow: false,
        })
    }

    renderFlatlist() {
        console.log(this.state.listShow);
        if (this.state.listShow)
            return (
                <FlatList
                    data={self.state.dataSource}
                    ItemSeparatorComponent={(text) => self.ListViewItemSeparator(text)}
                    renderItem={({ item }) => (
                        // Single Comes here which will be repeatative for the FlatListItems
                        <TouchableOpacity style={{
                            backgroundColor: '#F0FFFF',
                            borderRadius: 10,
                            height: 50,
                            width: screen.width * 90 / 100,
                            flexDirection: 'row',
                        }}

                            onPress={async () => { // 'details' is provided when fetchDetails = true  
                                console.log(item);
                                var latitude = item.lati;
                                var longitude = item.long;
                                await self.setState({
                                    region: {
                                        latitude: latitude,
                                        longitude: longitude,
                                        latitudeDelta: this.distance * 3 / 111000,
                                        longitudeDelta: this.distance * 3 / 111000,
                                    },
                                    location: {
                                        latitude: latitude,
                                        longitude: longitude,
                                    },
                                    listShow: false,
                                });
                                console.log(this.state.region.latitude);
                                console.log(this.state.region.longitude);
                            }}
                        >
                                    <View style={{
                                        height: 50,
                                        justifyContent: 'center',
                                        alignItems: 'center',
                                    }}>
                                        <Icon
                                            name="place"
                                            iconSize={30}
                                            size={30}
                                        />
                                    </View>
                                    <View
                                        style={{
                                            height: 50,
                                            justifyContent: 'center',
                                            alignItems: 'center',
                                        }}
                                    >
                                        <Text>{item.title}</Text>
                                    </View>
                        </TouchableOpacity >
                    )
                            }
                    enableEmptySections={true}
                            style={{ top: '10%' }}
                            keyExtractor={(item, index) => index.toString()}
                        />
                    );
                    else
                    return ;
    }

    calPlaceNear = (data) => {
        navigator.geolocation.getCurrentPosition(
            (position) => {
                let arrayIdnear = [];
                for (let marker of data) {
                    let coordinates = calCoordinates(marker.coordinates);
                    if (geolib.getDistance(position.coords, {
                        latitude: coordinates.latitude,
                        longitude: coordinates.longitude
                    }) < this.distance) {
                        arrayIdnear.push(marker);
                    }
                }
                this.placeNear = arrayIdnear;
                this.setState({
                    region: {
                        latitude: position.coords.latitude,
                        longitude: position.coords.longitude,
                        latitudeDelta: this.distance * 3 / 111000,
                        longitudeDelta: this.distance * 3 / 111000,
                    },
                    location: {
                        latitude: position.coords.latitude,
                        longitude: position.coords.longitude
                    },
                });
            },
            error => Alert.alert(error.message),
            { enableHighAccuracy: false, timeout: 3000 }
        );
    }
    componentWillMount() {
        let arr = LIST_HOT;
        arr = arr.concat(LIST_CA_PHE_MAPS);
        arr = arr.concat(LIST_TRA_SUA)
        arr = arr.concat(LIST_HOMESTAY_MAPS)
        arr = arr.concat(LIST_RAP_CP_MAPS)
        this.calPlaceNear(arr);
    }
    flitter = (id) => {
        if (id == 0) this.calPlaceNear(LIST_ALL);
        if (id == 2) this.calPlaceNear(LIST_CA_PHE_MAPS);
        if (id == 3) this.calPlaceNear(LIST_TRA_SUA);
        if (id == 4) this.calPlaceNear(LIST_HOMESTAY_MAPS);
        if (id == 5) this.calPlaceNear(LIST_RAP_CP_MAPS);
        if (id == 6) this.calPlaceNear(LIST_HOT);
    }
    expanderMaps = (data) => {
        this.distance = data;
        this.calPlaceNear(LIST_ALL);
    }
    onRegionChange(data) {

    }
    showDetail(Places) {
        this.setState({
            Place: Places,
            showDetail: true
        })
    }
    onPress(data) {
        this.setState({
            showDetail: false,
        })
    }

    render() {
        return (
            <View style={{ width: '100%', height: '100%', justifyContent: 'flex-end', alignItems: 'center', }}>


                <View style={styles.container}>
                    <MapView style={styles.map}
                        region={this.state.region}
                        onPress={this.onPress.bind(this)}
                    >
                        <MapView.Circle
                            center={this.state.location}
                            radius={this.distance}
                            strokeColor="rgba(0,112,255,0.3)"
                            fillColor="rgba(0,122,255,0.1)"
                            strokeWidth={2}
                        >
                        </MapView.Circle>
                        <MapView.Marker
                            key={this.state.location.latitude}
                            coordinate={this.state.location}
                            title={'Vị trí của tôi'}
                            description={''} >
                        </MapView.Marker>
                        {this.placeNear.map((value, index) => {
                            let marker = calCoordinates(value.coordinates);
                            let imagePin = calimagePin(value.type);
                            return (
                                <MapView.Marker
                                    key={marker.latitude} coordinate={marker}
                                    onPress={(e) => { e.stopPropagation(); this.showDetail(value) }}
                                >
                                    <Image
                                        resizeMode={'stretch'}
                                        style={{
                                            width: WIDTH(30),
                                            height: WIDTH(40)
                                        }}
                                        source={imagePin}
                                    />
                                </MapView.Marker>)
                        })}

                    </MapView>
                    <FluteButtonForMaps
                        onButton={this.flitter}
                    />
                    <ExpanderMaps
                        onButton={this.expanderMaps}
                    />
                    {this.state.showDetail &&
                        <Detail
                            navigation={this.props.navigation}
                            Place={this.state.Place} />}
                </View>
                <View style={{
                    width: '90%',
                    top: 10,
                    height: '10%',
                    position: 'absolute',
                    borderRadius: 10,
                }}>
                    <SearchButton
                        SearchFilterFunction={self.SearchFilterFunction}
                        hideList={self.hideList}
                    />
                </View>
                {this.renderFlatlist()}
            </View>

        )
    }
}