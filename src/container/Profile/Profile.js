import React, { Component } from 'react';
import { Text, View, StyleSheet, ScrollView, Dimensions, TouchableOpacity, AsyncStorage,ActivityIndicator } from 'react-native';
import { connect } from 'react-redux';
import { Image } from 'react-native-elements';
import { Card, CardItem, Left, Body, Right } from 'native-base'
import Ionicons from 'react-native-vector-icons/Ionicons';

import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import AntDesign from 'react-native-vector-icons/AntDesign';
import styles from './styles';
import { INIT_STORGE } from '../../config/default';

const { height, width } = Dimensions.get('window');

class Profile extends Component {
        render() {
                const { account } = this.props
                return (
                        <ScrollView style={styles.ctnStyle}>
                                <View style={styles.headerStyle}>
                                        <Text style={styles.textSetting}>Tài khoản</Text>
                                </View>
                                <View style={styles.bodyStyle}>
                                        <View style={styles.ctnviewImage}>
                                                <TouchableOpacity style={styles.viewImage}>
                                                <Image
                                                                PlaceholderContent={<ActivityIndicator />}
                                                                style={styles.imageStyle}
                                                                source={{
                                                                        uri: account.avatar != undefined ? account.avatar : 'https://scontent.fhan2-2.fna.fbcdn.net/v/t1.0-9/37024009_10155962446819234_7583110716408725504_n.jpg?_nc_cat=110&_nc_oc=AQmg0qXOnvdySChvDoaj3VS5Am_6vc9CaDvWdP8b6kuX4D6FWIykQsN0qUD2Dq8gOPQ&_nc_ht=scontent.fhan2-2.fna&oh=fac2491cc7491f243fc8e5b6f677c315&oe=5D733C08'
                                                                }} />
                                                </TouchableOpacity>
                                                <TouchableOpacity style={styles.viewPen}>
                                                        <SimpleLineIcons name="pencil" color="#1C1C1C" size={15} />
                                                </TouchableOpacity>
                                        </View>
                                        <View style={styles.viewAcount}>
                                                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                                        <View style={{ flex: 1, borderColor: '#ddd', borderWidth: 1 }}></View>
                                                        <View style={styles.nameAcount}><Text style={styles.Textname}>{account.hoVaTen != undefined ? account.hoVaTen : 'Ngô Quốc Dũng'}</Text></View>
                                                        <View style={{ flex: 1, borderColor: '#ddd', borderWidth: 1 }}></View>
                                                </View>
                                                <View style={styles.levelMember}><Text style={styles.text}>Hight Member</Text></View>
                                        </View>
                                </View>
                                <TouchableOpacity>
                                        <Card>
                                                <CardItem>
                                                        <Left>
                                                                <Text style={styles.textChoice}>{account.hoVaTen != undefined ? account.hoVaTen : 'Ngô Quốc Dũng'}</Text>
                                                        </Left>
                                                        <Right>
                                                                <SimpleLineIcons name="user-following" color="#1C1C1C" size={22 / 640 * height} />
                                                        </Right>
                                                </CardItem>
                                        </Card>
                                </TouchableOpacity>
                                <TouchableOpacity>
                                        <Card>
                                                <CardItem>
                                                        <Left>
                                                                <Text style={styles.textChoice}>{account.username != undefined ? account.username : 'dung@.com'}</Text>
                                                        </Left>
                                                        <Right>
                                                                <Ionicons name="ios-people" color="#1C1C1C" size={22 / 640 * height} />
                                                        </Right>
                                                </CardItem>
                                        </Card>
                                </TouchableOpacity>
                                <TouchableOpacity>
                                        <Card>
                                                <CardItem>
                                                        <Left>
                                                                <Text style={styles.textChoice}>{account.soDienThoai != undefined ? account.soDienThoai : '0985061316'}</Text>
                                                        </Left>
                                                        <Right>
                                                                <AntDesign name="phone" color="#1C1C1C" size={22 / 640 * height} />
                                                        </Right>
                                                </CardItem>
                                        </Card>
                                </TouchableOpacity>
                                <TouchableOpacity
                                        onPress={async () => {
                                                await AsyncStorage.multiRemove([INIT_STORGE]);
                                                await this.props.navigation.dispatch({
                                                        type: 'Navigation/RESET',
                                                        actions: [{
                                                                type: 'Navigate',
                                                                routeName: 'SignIn'
                                                        }], index: 0
                                                });
                                        }}
                                >
                                        <Card>
                                                <CardItem>
                                                        <Left>
                                                                <Text style={styles.textChoice}>Đăng xuất</Text>
                                                        </Left>
                                                        <Right>
                                                                <AntDesign name="poweroff" color="#1C1C1C" size={22 / 640 * height} />
                                                        </Right>
                                                </CardItem>
                                        </Card>
                                </TouchableOpacity>
                        </ScrollView>
                )
        }
}
function mapStateToProps(state) {
        return {
                account: state.accountReducer.account,
        };
}

export default connect(mapStateToProps, {
})(Profile);