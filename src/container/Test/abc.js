export const LIST_CAFE = [
    {
        imageMain: 'https://www.couturetravelcompany.com/wp-content/uploads/2018/10/hienpham_.jpg',
        namePlace: 'Cup of Tea Cafe & Bistro',
        coordinates: '21.0251967,105.7966571',
        address: '317 Nguyễn Khang, Yên Hoà, Cầu Giấy, Hà Nội',
        describe: 'Trill Group với tổ hợp cafe, bể bơi, phòng tập gym... - là một trong những quán cà phê đẹp ở Hà Nội có view cực chất.Nếu bạn thích trải nghiệm cảm giác vừa nhâm nhi tách trà ấm nóng hay chocolate ngọt ngào, vừa ngắm Hà Nội từ trên cao thì Trill Group là sự lựa chọn không thể lý tưởng hơn.Ngoài yếu tố không gian view và không gian, đồ ăn, đồ uống ở đây cũng khá đầy đủ. Mức giá đồ ở đây trung bình từ 40k/ món.',
        listImage: 'https://images.foody.vn/images/14369204_1513957175297868_7285838755494100992_n.jpg;https://images.foody.vn/images/trillgroup.jpg;https://images.foody.vn/images/14449222_1037816323006133_4293765315042476032_n.jpg;https://images.foody.vn/images/14134940_1584382345199200_778613661_n.jpg;https://images.foody.vn/images/13774392_971524286308546_353305157_n.jpg', time: '21 giờ trước',
    },
    {
        imageMain: 'https://www.couturetravelcompany.com/wp-content/uploads/2018/10/1658_17882898_529408880781045_1811213216533970944_n1.jpg',
        namePlace: 'Cafe Lissom Parlour',
        coordinates: '21.0251967,105.7966571', address: '317 Nguyễn Khang, Yên Hoà, Cầu Giấy, Hà Nội', describe: 'Trill Group với tổ hợp cafe, bể bơi, phòng tập gym... - là một trong những quán cà phê đẹp ở Hà Nội có view cực chất.Nếu bạn thích trải nghiệm cảm giác vừa nhâm nhi tách trà ấm nóng hay chocolate ngọt ngào, vừa ngắm Hà Nội từ trên cao thì Trill Group là sự lựa chọn không thể lý tưởng hơn.Ngoài yếu tố không gian view và không gian, đồ ăn, đồ uống ở đây cũng khá đầy đủ. Mức giá đồ ở đây trung bình từ 40k/ món.', listImage: 'https://images.foody.vn/images/14369204_1513957175297868_7285838755494100992_n.jpg;https://images.foody.vn/images/trillgroup.jpg;https://images.foody.vn/images/14449222_1037816323006133_4293765315042476032_n.jpg;https://images.foody.vn/images/14134940_1584382345199200_778613661_n.jpg;https://images.foody.vn/images/13774392_971524286308546_353305157_n.jpg', time: '21 giờ trước',
    },
    {
        imageMain: 'https://www.couturetravelcompany.com/wp-content/uploads/2018/10/cafegardenista.jpg',
        namePlace: 'The YLang',
        coordinates: '21.0251967,105.7966571', address: '317 Nguyễn Khang, Yên Hoà, Cầu Giấy, Hà Nội', describe: 'Trill Group với tổ hợp cafe, bể bơi, phòng tập gym... - là một trong những quán cà phê đẹp ở Hà Nội có view cực chất.Nếu bạn thích trải nghiệm cảm giác vừa nhâm nhi tách trà ấm nóng hay chocolate ngọt ngào, vừa ngắm Hà Nội từ trên cao thì Trill Group là sự lựa chọn không thể lý tưởng hơn.Ngoài yếu tố không gian view và không gian, đồ ăn, đồ uống ở đây cũng khá đầy đủ. Mức giá đồ ở đây trung bình từ 40k/ món.', listImage: 'https://images.foody.vn/images/14369204_1513957175297868_7285838755494100992_n.jpg;https://images.foody.vn/images/trillgroup.jpg;https://images.foody.vn/images/14449222_1037816323006133_4293765315042476032_n.jpg;https://images.foody.vn/images/14134940_1584382345199200_778613661_n.jpg;https://images.foody.vn/images/13774392_971524286308546_353305157_n.jpg', time: '21 giờ trước',
    },
    {
        imageMain: 'https://www.couturetravelcompany.com/wp-content/uploads/2018/10/38536397_753505978314556_1332924576119652352_o-1.jpg',
        namePlace: 'C’est Si Bon',
        coordinates: '21.0251967,105.7966571', address: '317 Nguyễn Khang, Yên Hoà, Cầu Giấy, Hà Nội', describe: 'Trill Group với tổ hợp cafe, bể bơi, phòng tập gym... - là một trong những quán cà phê đẹp ở Hà Nội có view cực chất.Nếu bạn thích trải nghiệm cảm giác vừa nhâm nhi tách trà ấm nóng hay chocolate ngọt ngào, vừa ngắm Hà Nội từ trên cao thì Trill Group là sự lựa chọn không thể lý tưởng hơn.Ngoài yếu tố không gian view và không gian, đồ ăn, đồ uống ở đây cũng khá đầy đủ. Mức giá đồ ở đây trung bình từ 40k/ món.', listImage: 'https://images.foody.vn/images/14369204_1513957175297868_7285838755494100992_n.jpg;https://images.foody.vn/images/trillgroup.jpg;https://images.foody.vn/images/14449222_1037816323006133_4293765315042476032_n.jpg;https://images.foody.vn/images/14134940_1584382345199200_778613661_n.jpg;https://images.foody.vn/images/13774392_971524286308546_353305157_n.jpg', time: '21 giờ trước',
    },
    {
        imageMain: 'https://www.couturetravelcompany.com/wp-content/uploads/2018/10/coffeeholiicc1-1493201399280.jpg',
        namePlace: 'Flatform',
        coordinates: '21.0251967,105.7966571', address: '317 Nguyễn Khang, Yên Hoà, Cầu Giấy, Hà Nội', describe: 'Trill Group với tổ hợp cafe, bể bơi, phòng tập gym... - là một trong những quán cà phê đẹp ở Hà Nội có view cực chất.Nếu bạn thích trải nghiệm cảm giác vừa nhâm nhi tách trà ấm nóng hay chocolate ngọt ngào, vừa ngắm Hà Nội từ trên cao thì Trill Group là sự lựa chọn không thể lý tưởng hơn.Ngoài yếu tố không gian view và không gian, đồ ăn, đồ uống ở đây cũng khá đầy đủ. Mức giá đồ ở đây trung bình từ 40k/ món.', listImage: 'https://images.foody.vn/images/14369204_1513957175297868_7285838755494100992_n.jpg;https://images.foody.vn/images/trillgroup.jpg;https://images.foody.vn/images/14449222_1037816323006133_4293765315042476032_n.jpg;https://images.foody.vn/images/14134940_1584382345199200_778613661_n.jpg;https://images.foody.vn/images/13774392_971524286308546_353305157_n.jpg', time: '21 giờ trước',
    },
];
export const LIST_TRA_SUA = [
    {
        imageMain: 'https://cdn.jamja.vn/blog/wp-content/uploads/2018/08/bobapop-dao-tan.jpg',
        namePlace: 'Trà sữa Bobapop – Đào Tấn',
        coordinates: '21.0251967,105.7966571', 
        address: '317 Nguyễn Khang, Yên Hoà, Cầu Giấy, Hà Nội', describe: 'Trill Group với tổ hợp cafe, bể bơi, phòng tập gym... - là một trong những quán cà phê đẹp ở Hà Nội có view cực chất.Nếu bạn thích trải nghiệm cảm giác vừa nhâm nhi tách trà ấm nóng hay chocolate ngọt ngào, vừa ngắm Hà Nội từ trên cao thì Trill Group là sự lựa chọn không thể lý tưởng hơn.Ngoài yếu tố không gian view và không gian, đồ ăn, đồ uống ở đây cũng khá đầy đủ. Mức giá đồ ở đây trung bình từ 40k/ món.', listImage: 'https://images.foody.vn/images/14369204_1513957175297868_7285838755494100992_n.jpg;https://images.foody.vn/images/trillgroup.jpg;https://images.foody.vn/images/14449222_1037816323006133_4293765315042476032_n.jpg;https://images.foody.vn/images/14134940_1584382345199200_778613661_n.jpg;https://images.foody.vn/images/13774392_971524286308546_353305157_n.jpg', time: '21 giờ trước',
    },
    {
        imageMain: 'https://cdn.jamja.vn/blog/wp-content/uploads/2018/08/tocotoco-hangbai.jpg',
        namePlace: 'Trà sữa TocoToco – Hàng Bài',
        coordinates: '21.0251967,105.7966571', address: '317 Nguyễn Khang, Yên Hoà, Cầu Giấy, Hà Nội', describe: 'Trill Group với tổ hợp cafe, bể bơi, phòng tập gym... - là một trong những quán cà phê đẹp ở Hà Nội có view cực chất.Nếu bạn thích trải nghiệm cảm giác vừa nhâm nhi tách trà ấm nóng hay chocolate ngọt ngào, vừa ngắm Hà Nội từ trên cao thì Trill Group là sự lựa chọn không thể lý tưởng hơn.Ngoài yếu tố không gian view và không gian, đồ ăn, đồ uống ở đây cũng khá đầy đủ. Mức giá đồ ở đây trung bình từ 40k/ món.', listImage: 'https://images.foody.vn/images/14369204_1513957175297868_7285838755494100992_n.jpg;https://images.foody.vn/images/trillgroup.jpg;https://images.foody.vn/images/14449222_1037816323006133_4293765315042476032_n.jpg;https://images.foody.vn/images/14134940_1584382345199200_778613661_n.jpg;https://images.foody.vn/images/13774392_971524286308546_353305157_n.jpg', time: '21 giờ trước',
    },
    {
        imageMain: 'https://cdn.jamja.vn/blog/wp-content/uploads/2018/08/gongcha-lythuongkiet.jpg',
        namePlace: 'Trà sữa Gong Cha – Lý Thường Kiệt',
        coordinates: '21.0251967,105.7966571', address: '317 Nguyễn Khang, Yên Hoà, Cầu Giấy, Hà Nội', describe: 'Trill Group với tổ hợp cafe, bể bơi, phòng tập gym... - là một trong những quán cà phê đẹp ở Hà Nội có view cực chất.Nếu bạn thích trải nghiệm cảm giác vừa nhâm nhi tách trà ấm nóng hay chocolate ngọt ngào, vừa ngắm Hà Nội từ trên cao thì Trill Group là sự lựa chọn không thể lý tưởng hơn.Ngoài yếu tố không gian view và không gian, đồ ăn, đồ uống ở đây cũng khá đầy đủ. Mức giá đồ ở đây trung bình từ 40k/ món.', listImage: 'https://images.foody.vn/images/14369204_1513957175297868_7285838755494100992_n.jpg;https://images.foody.vn/images/trillgroup.jpg;https://images.foody.vn/images/14449222_1037816323006133_4293765315042476032_n.jpg;https://images.foody.vn/images/14134940_1584382345199200_778613661_n.jpg;https://images.foody.vn/images/13774392_971524286308546_353305157_n.jpg', time: '21 giờ trước',
    },
    {
        imageMain: 'https://cdn.jamja.vn/blog/wp-content/uploads/2018/07/tra-sua-royaltea-thai-phien1.jpg',
        namePlace: 'Trà sữa Royaltea – Thái Phiên',
        coordinates: '21.0251967,105.7966571', address: '317 Nguyễn Khang, Yên Hoà, Cầu Giấy, Hà Nội', describe: 'Trill Group với tổ hợp cafe, bể bơi, phòng tập gym... - là một trong những quán cà phê đẹp ở Hà Nội có view cực chất.Nếu bạn thích trải nghiệm cảm giác vừa nhâm nhi tách trà ấm nóng hay chocolate ngọt ngào, vừa ngắm Hà Nội từ trên cao thì Trill Group là sự lựa chọn không thể lý tưởng hơn.Ngoài yếu tố không gian view và không gian, đồ ăn, đồ uống ở đây cũng khá đầy đủ. Mức giá đồ ở đây trung bình từ 40k/ món.', listImage: 'https://images.foody.vn/images/14369204_1513957175297868_7285838755494100992_n.jpg;https://images.foody.vn/images/trillgroup.jpg;https://images.foody.vn/images/14449222_1037816323006133_4293765315042476032_n.jpg;https://images.foody.vn/images/14134940_1584382345199200_778613661_n.jpg;https://images.foody.vn/images/13774392_971524286308546_353305157_n.jpg', time: '21 giờ trước',
    },
    {
        imageMain: 'https://cdn.jamja.vn/blog/wp-content/uploads/2018/07/quan-tra-sua-theteapub14.jpg',
        namePlace: 'Trà sữa TheTeaPub – Lò Sũ',
        coordinates: '21.0251967,105.7966571', address: '317 Nguyễn Khang, Yên Hoà, Cầu Giấy, Hà Nội', describe: 'Trill Group với tổ hợp cafe, bể bơi, phòng tập gym... - là một trong những quán cà phê đẹp ở Hà Nội có view cực chất.Nếu bạn thích trải nghiệm cảm giác vừa nhâm nhi tách trà ấm nóng hay chocolate ngọt ngào, vừa ngắm Hà Nội từ trên cao thì Trill Group là sự lựa chọn không thể lý tưởng hơn.Ngoài yếu tố không gian view và không gian, đồ ăn, đồ uống ở đây cũng khá đầy đủ. Mức giá đồ ở đây trung bình từ 40k/ món.', listImage: 'https://images.foody.vn/images/14369204_1513957175297868_7285838755494100992_n.jpg;https://images.foody.vn/images/trillgroup.jpg;https://images.foody.vn/images/14449222_1037816323006133_4293765315042476032_n.jpg;https://images.foody.vn/images/14134940_1584382345199200_778613661_n.jpg;https://images.foody.vn/images/13774392_971524286308546_353305157_n.jpg', time: '21 giờ trước',
    },
]
export const LIST_RAP_CP = [
    {
        imageMain: 'https://dulichtoday.vn/wp-content/uploads/2018/09/Cafe-Phim-Ha-Dong.jpg',
        namePlace: 'Cafe phim Hà Đông',
        coordinates: '21.0251967,105.7966571', address: '317 Nguyễn Khang, Yên Hoà, Cầu Giấy, Hà Nội', describe: 'Trill Group với tổ hợp cafe, bể bơi, phòng tập gym... - là một trong những quán cà phê đẹp ở Hà Nội có view cực chất.Nếu bạn thích trải nghiệm cảm giác vừa nhâm nhi tách trà ấm nóng hay chocolate ngọt ngào, vừa ngắm Hà Nội từ trên cao thì Trill Group là sự lựa chọn không thể lý tưởng hơn.Ngoài yếu tố không gian view và không gian, đồ ăn, đồ uống ở đây cũng khá đầy đủ. Mức giá đồ ở đây trung bình từ 40k/ món.', listImage: 'https://images.foody.vn/images/14369204_1513957175297868_7285838755494100992_n.jpg;https://images.foody.vn/images/trillgroup.jpg;https://images.foody.vn/images/14449222_1037816323006133_4293765315042476032_n.jpg;https://images.foody.vn/images/14134940_1584382345199200_778613661_n.jpg;https://images.foody.vn/images/13774392_971524286308546_353305157_n.jpg', time: '21 giờ trước',
    },
    {
        imageMain: 'https://dulichtoday.vn/wp-content/uploads/2018/09/Cafe-Phim-T-Box.jpg',
        namePlace: 'T-Box Cafe Phim',
        coordinates: '21.0251967,105.7966571', address: '317 Nguyễn Khang, Yên Hoà, Cầu Giấy, Hà Nội', describe: 'Trill Group với tổ hợp cafe, bể bơi, phòng tập gym... - là một trong những quán cà phê đẹp ở Hà Nội có view cực chất.Nếu bạn thích trải nghiệm cảm giác vừa nhâm nhi tách trà ấm nóng hay chocolate ngọt ngào, vừa ngắm Hà Nội từ trên cao thì Trill Group là sự lựa chọn không thể lý tưởng hơn.Ngoài yếu tố không gian view và không gian, đồ ăn, đồ uống ở đây cũng khá đầy đủ. Mức giá đồ ở đây trung bình từ 40k/ món.', listImage: 'https://images.foody.vn/images/14369204_1513957175297868_7285838755494100992_n.jpg;https://images.foody.vn/images/trillgroup.jpg;https://images.foody.vn/images/14449222_1037816323006133_4293765315042476032_n.jpg;https://images.foody.vn/images/14134940_1584382345199200_778613661_n.jpg;https://images.foody.vn/images/13774392_971524286308546_353305157_n.jpg', time: '21 giờ trước',
    },
    {
        imageMain: 'https://dulichtoday.vn/wp-content/uploads/2018/09/Cafe-Phim-3D-Kinomax.jpg',
        namePlace: 'Kinomax Cafe Phim 3D/HD',
        coordinates: '21.0251967,105.7966571', address: '317 Nguyễn Khang, Yên Hoà, Cầu Giấy, Hà Nội', describe: 'Trill Group với tổ hợp cafe, bể bơi, phòng tập gym... - là một trong những quán cà phê đẹp ở Hà Nội có view cực chất.Nếu bạn thích trải nghiệm cảm giác vừa nhâm nhi tách trà ấm nóng hay chocolate ngọt ngào, vừa ngắm Hà Nội từ trên cao thì Trill Group là sự lựa chọn không thể lý tưởng hơn.Ngoài yếu tố không gian view và không gian, đồ ăn, đồ uống ở đây cũng khá đầy đủ. Mức giá đồ ở đây trung bình từ 40k/ món.', listImage: 'https://images.foody.vn/images/14369204_1513957175297868_7285838755494100992_n.jpg;https://images.foody.vn/images/trillgroup.jpg;https://images.foody.vn/images/14449222_1037816323006133_4293765315042476032_n.jpg;https://images.foody.vn/images/14134940_1584382345199200_778613661_n.jpg;https://images.foody.vn/images/13774392_971524286308546_353305157_n.jpg', time: '21 giờ trước',
    },
    {
        imageMain: 'https://dulichtoday.vn/wp-content/uploads/2018/09/Cafe-Phim-3D-Paradise-Cafe-Phim.jpg',
        namePlace: 'Paradise Coffee & Film',
        coordinates: '21.0251967,105.7966571', address: '317 Nguyễn Khang, Yên Hoà, Cầu Giấy, Hà Nội', describe: 'Trill Group với tổ hợp cafe, bể bơi, phòng tập gym... - là một trong những quán cà phê đẹp ở Hà Nội có view cực chất.Nếu bạn thích trải nghiệm cảm giác vừa nhâm nhi tách trà ấm nóng hay chocolate ngọt ngào, vừa ngắm Hà Nội từ trên cao thì Trill Group là sự lựa chọn không thể lý tưởng hơn.Ngoài yếu tố không gian view và không gian, đồ ăn, đồ uống ở đây cũng khá đầy đủ. Mức giá đồ ở đây trung bình từ 40k/ món.', listImage: 'https://images.foody.vn/images/14369204_1513957175297868_7285838755494100992_n.jpg;https://images.foody.vn/images/trillgroup.jpg;https://images.foody.vn/images/14449222_1037816323006133_4293765315042476032_n.jpg;https://images.foody.vn/images/14134940_1584382345199200_778613661_n.jpg;https://images.foody.vn/images/13774392_971524286308546_353305157_n.jpg', time: '21 giờ trước',
    },
    {
        imageMain: 'https://dulichtoday.vn/wp-content/uploads/2018/09/Cafe-Phim-3D-Ho-tung-mau-2.jpg',
        namePlace: 'Cafe phim 3D Hồ Tùng Mậu',
        coordinates: '21.0251967,105.7966571', address: '317 Nguyễn Khang, Yên Hoà, Cầu Giấy, Hà Nội', describe: 'Trill Group với tổ hợp cafe, bể bơi, phòng tập gym... - là một trong những quán cà phê đẹp ở Hà Nội có view cực chất.Nếu bạn thích trải nghiệm cảm giác vừa nhâm nhi tách trà ấm nóng hay chocolate ngọt ngào, vừa ngắm Hà Nội từ trên cao thì Trill Group là sự lựa chọn không thể lý tưởng hơn.Ngoài yếu tố không gian view và không gian, đồ ăn, đồ uống ở đây cũng khá đầy đủ. Mức giá đồ ở đây trung bình từ 40k/ món.', listImage: 'https://images.foody.vn/images/14369204_1513957175297868_7285838755494100992_n.jpg;https://images.foody.vn/images/trillgroup.jpg;https://images.foody.vn/images/14449222_1037816323006133_4293765315042476032_n.jpg;https://images.foody.vn/images/14134940_1584382345199200_778613661_n.jpg;https://images.foody.vn/images/13774392_971524286308546_353305157_n.jpg', time: '21 giờ trước',
    },
]
export const LIST_HOME_STAY = [

    {
        imageMain: 'https://dulichtoday.vn/wp-content/uploads/2018/04/Mei-Hideaway.jpg',
        namePlace: 'Mei Hideaway',
        coordinates: '21.0251967,105.7966571', address: '317 Nguyễn Khang, Yên Hoà, Cầu Giấy, Hà Nội', describe: 'Trill Group với tổ hợp cafe, bể bơi, phòng tập gym... - là một trong những quán cà phê đẹp ở Hà Nội có view cực chất.Nếu bạn thích trải nghiệm cảm giác vừa nhâm nhi tách trà ấm nóng hay chocolate ngọt ngào, vừa ngắm Hà Nội từ trên cao thì Trill Group là sự lựa chọn không thể lý tưởng hơn.Ngoài yếu tố không gian view và không gian, đồ ăn, đồ uống ở đây cũng khá đầy đủ. Mức giá đồ ở đây trung bình từ 40k/ món.', listImage: 'https://images.foody.vn/images/14369204_1513957175297868_7285838755494100992_n.jpg;https://images.foody.vn/images/trillgroup.jpg;https://images.foody.vn/images/14449222_1037816323006133_4293765315042476032_n.jpg;https://images.foody.vn/images/14134940_1584382345199200_778613661_n.jpg;https://images.foody.vn/images/13774392_971524286308546_353305157_n.jpg', time: '21 giờ trước',
    },
    {
        imageMain: 'https://dulichtoday.vn/wp-content/uploads/2018/04/Lagom-Homestay-1.jpg',
        namePlace: 'Lagom Homestay',
        coordinates: '21.0251967,105.7966571', address: '317 Nguyễn Khang, Yên Hoà, Cầu Giấy, Hà Nội', describe: 'Trill Group với tổ hợp cafe, bể bơi, phòng tập gym... - là một trong những quán cà phê đẹp ở Hà Nội có view cực chất.Nếu bạn thích trải nghiệm cảm giác vừa nhâm nhi tách trà ấm nóng hay chocolate ngọt ngào, vừa ngắm Hà Nội từ trên cao thì Trill Group là sự lựa chọn không thể lý tưởng hơn.Ngoài yếu tố không gian view và không gian, đồ ăn, đồ uống ở đây cũng khá đầy đủ. Mức giá đồ ở đây trung bình từ 40k/ món.', listImage: 'https://images.foody.vn/images/14369204_1513957175297868_7285838755494100992_n.jpg;https://images.foody.vn/images/trillgroup.jpg;https://images.foody.vn/images/14449222_1037816323006133_4293765315042476032_n.jpg;https://images.foody.vn/images/14134940_1584382345199200_778613661_n.jpg;https://images.foody.vn/images/13774392_971524286308546_353305157_n.jpg', time: '21 giờ trước',
    },
    {
        imageMain: 'https://scontent.fhan1-1.fna.fbcdn.net/v/t1.0-9/36652441_403569323485035_1877483015723024384_n.jpg?_nc_cat=106&_nc_oc=AQnV4Ujj8ZnEoFuw82dSqlGG7Ct6hNPUIDEJjIzuZz_wL42i5YWECHwFg7qX7EfIWnE&_nc_ht=scontent.fhan1-1.fna&oh=7779b2bd318820e45df4f45b3ab3616f&oe=5D3BA4C3',
        namePlace: 'Arena Village',
        coordinates: '21.0251967,105.7966571', address: '317 Nguyễn Khang, Yên Hoà, Cầu Giấy, Hà Nội', describe: 'Trill Group với tổ hợp cafe, bể bơi, phòng tập gym... - là một trong những quán cà phê đẹp ở Hà Nội có view cực chất.Nếu bạn thích trải nghiệm cảm giác vừa nhâm nhi tách trà ấm nóng hay chocolate ngọt ngào, vừa ngắm Hà Nội từ trên cao thì Trill Group là sự lựa chọn không thể lý tưởng hơn.Ngoài yếu tố không gian view và không gian, đồ ăn, đồ uống ở đây cũng khá đầy đủ. Mức giá đồ ở đây trung bình từ 40k/ món.', listImage: 'https://images.foody.vn/images/14369204_1513957175297868_7285838755494100992_n.jpg;https://images.foody.vn/images/trillgroup.jpg;https://images.foody.vn/images/14449222_1037816323006133_4293765315042476032_n.jpg;https://images.foody.vn/images/14134940_1584382345199200_778613661_n.jpg;https://images.foody.vn/images/13774392_971524286308546_353305157_n.jpg', time: '21 giờ trước',
    },
    {
        imageMain: 'https://dulichtoday.vn/wp-content/uploads/2018/04/Tropical-Nordic-House.jpg',
        namePlace: 'Tropical Nordic House',
        coordinates: '21.0251967,105.7966571', address: '317 Nguyễn Khang, Yên Hoà, Cầu Giấy, Hà Nội', describe: 'Trill Group với tổ hợp cafe, bể bơi, phòng tập gym... - là một trong những quán cà phê đẹp ở Hà Nội có view cực chất.Nếu bạn thích trải nghiệm cảm giác vừa nhâm nhi tách trà ấm nóng hay chocolate ngọt ngào, vừa ngắm Hà Nội từ trên cao thì Trill Group là sự lựa chọn không thể lý tưởng hơn.Ngoài yếu tố không gian view và không gian, đồ ăn, đồ uống ở đây cũng khá đầy đủ. Mức giá đồ ở đây trung bình từ 40k/ món.', listImage: 'https://images.foody.vn/images/14369204_1513957175297868_7285838755494100992_n.jpg;https://images.foody.vn/images/trillgroup.jpg;https://images.foody.vn/images/14449222_1037816323006133_4293765315042476032_n.jpg;https://images.foody.vn/images/14134940_1584382345199200_778613661_n.jpg;https://images.foody.vn/images/13774392_971524286308546_353305157_n.jpg', time: '21 giờ trước',
    },
    {
        imageMain: 'https://dulichtoday.vn/wp-content/uploads/2018/04/Au-frais.jpg',
        namePlace: 'Au frais',
        coordinates: '21.0251967,105.7966571', address: '317 Nguyễn Khang, Yên Hoà, Cầu Giấy, Hà Nội', describe: 'Trill Group với tổ hợp cafe, bể bơi, phòng tập gym... - là một trong những quán cà phê đẹp ở Hà Nội có view cực chất.Nếu bạn thích trải nghiệm cảm giác vừa nhâm nhi tách trà ấm nóng hay chocolate ngọt ngào, vừa ngắm Hà Nội từ trên cao thì Trill Group là sự lựa chọn không thể lý tưởng hơn.Ngoài yếu tố không gian view và không gian, đồ ăn, đồ uống ở đây cũng khá đầy đủ. Mức giá đồ ở đây trung bình từ 40k/ món.', listImage: 'https://images.foody.vn/images/14369204_1513957175297868_7285838755494100992_n.jpg;https://images.foody.vn/images/trillgroup.jpg;https://images.foody.vn/images/14449222_1037816323006133_4293765315042476032_n.jpg;https://images.foody.vn/images/14134940_1584382345199200_778613661_n.jpg;https://images.foody.vn/images/13774392_971524286308546_353305157_n.jpg', time: '21 giờ trước',
    },
]