import React, { Component } from 'react'
import {
  Text, View, StyleSheet, Image, TouchableOpacity, ImageBackground,
  TextInput, Keyboard, TouchableWithoutFeedback, Alert, AsyncStorage
} from 'react-native';

import { connect } from 'react-redux';
import { SocialIcon } from 'react-native-elements';
//import

import IMAGE from '../../assets/image';
import STYLES from '../../config/styles.config';
import colors from '../../config/colors';

import {INIT_STORGE} from '../../config/default';
import { LOGIN_SERVICE, FetchJson } from '../../servers/serivce.config';
import { WIDTH, HEIGHT } from '../../config/Function';
import {changeAccount} from '../../actions/actionCreators';
import { LoadingComponent } from '../../common/LoadingComponet';
import {loginFb} from './functionSignIn'
type Props = {
  title: string,
}
var self;
class SignIn extends Component<Props> {
  constructor(props) {
    super(props);
    self=this;
    this.state = {
      username: '',
      isLoading:false,
      password: '',
      bottom: 267 * STYLES.heightScreen / 640,
      heightImage: 90,
    }
  }
  componentWillMount() {
    this.keyboardWillShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardWillShow);
    this.keyboardWillShowListener = Keyboard.addListener('keyboardDidHide', this._keyboardWillHide);
  }
  componentWillUnmount() {
    this.keyboardWillShowListener.remove();
    this.keyboardWillShowListener.remove();
  }
  _keyboardWillShow = (event) => {
    this.setState({
      heightImage: 45
    });
  };

  _keyboardWillHide = (event) => {
    this.setState({
      heightImage: 90
    });

  };
  skip=(account)=> {
    const { dispatch } = this.props.navigation;
    AsyncStorage.multiSet([
      [INIT_STORGE, JSON.stringify(account)],
    ]);
    this.props.changeAccount(account);
    dispatch({
      type: 'Navigation/RESET',
      actions: [{
        type: 'Navigate',
        routeName: 'Home'
      }], index: 0
    });
  }
  onButton() {
    var account = {
      username: this.state.username,
      password: this.state.password,
    }
    this.setState({isLoading:true})
    FetchJson(LOGIN_SERVICE, account).then((response: any) => {
      this.setState({isLoading:false})
      if (response.hasOwnProperty('status') && response.status) {
         this.skip(response.rows[0]);
      } else {
        Alert.alert(
          'Thông báo',
          'Sai tài khoản hoặc mật khẩu',
          [
            { text: 'OK' }
          ],
          { cancelable: false }
        )
      }
    })
  }
  render() {
    return (
      <TouchableWithoutFeedback
        style={{ flex: 1 }} onPress={() => Keyboard.dismiss()}>
        <ImageBackground
          style={{
            width: STYLES.widthScreen,
            height: STYLES.heightScreen,
            alignItems: 'center',
           justifyContent:'center',
          }}
          source={IMAGE.isee6}
        >
        <Text style={{
          fontSize:this.state.heightImage,
          fontFamily:'Playlist-Script',
          color:'white',
        }}>
               I See
        </Text>
          <View style={[styles.frameSignIn]}>
            <View style={styles.viewUser}>
              <TextInput
                style={styles.textEmail}
                placeholder='Tài khoản'
                autoCapitalize = 'none'
                placeholderTextColor={"#ffff"}
                returnKeyType='next'
                underlineColorAndroid={'transparent'}
                onChangeText={(username) => this.setState({ username })}
                value={this.state.username}
              />
            </View>
            <View style={styles.viewUser}>
              <TextInput
                style={styles.textEmail}
                placeholder='Mật khẩu'
                autoCapitalize = 'none'
                placeholderTextColor={"#ffff"}
                returnKeyType='next'
                secureTextEntry={true}
                underlineColorAndroid={'transparent'}
                onChangeText={(password) => this.setState({ password })}
                value={this.state.password}
              />
            </View>
            <TouchableOpacity 
              onPress={()=>{
                  this.props.navigation.navigate("SignUp");
              }}
              style={{
                width: WIDTH(250),
                alignItems: 'flex-end',
                marginTop: HEIGHT(5),
              }}>
              <Text style={styles.textNonal}>
                Tạo tài khoản?
                    </Text>
            </TouchableOpacity>
          </View>
          <View style={styles.viewButton}>
            <TouchableOpacity
              style={styles.button}
              onPress={() => this.onButton()}
            >
              <Text style={{ fontSize: STYLES.fontSizeNormalText, color: 'black' }}>
                ĐĂNG NHẬP
                  </Text>
            </TouchableOpacity>
          </View>
          <Text style={[styles.textNonal,{
                  fontFamily:'Playlist-Script',
                  fontSize:STYLES.fontSizeText}]}>
                hoặc
          </Text>

          <View>
      </View>
          <TouchableOpacity 
              onPress={()=>loginFb(self)}
              style={styles.viewSkip}>
                <SocialIcon
                  style={{
                    width:WIDTH(250),
                    height:HEIGHT(45),
                    borderRadius:0,
                  }}
                  title='Đăng nhập với Facebook'
                  button
                  type='facebook'
                />
          </TouchableOpacity>
          <LoadingComponent
                isLoading={this.state.isLoading}
            />
        </ImageBackground>
      </TouchableWithoutFeedback>
    )
  }
}


function mapStateToProps(state) {
    return {
      account: state.accountReducer.account,
    };
}

export default connect(mapStateToProps, {
    changeAccount
})(SignIn);


const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent:'center',
  },
  textNonal: {
    color: 'white',
    fontSize: STYLES.fontSizeText,
  },
  text: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: STYLES.fontSizeText,
  },
  image: {
    width: 120 * STYLES.widthScreen / 360,
    height: 80 * STYLES.heightScreen / 640,
  },
  viewOfimage: {
    width: STYLES.widthScreen,
    height: 313 * STYLES.heightScreen / 640,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colors.colorPink,
    shadowColor: '#E5E5E5',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 3,
    shadowRadius: 2,
    elevation: 2,
  },
  frameSignIn: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 0,
    marginBottom: 35 * STYLES.heightScreen / 640,
  },
  viewButton: {
    width: 250 * STYLES.widthScreen / 360,
    height: 52 * STYLES.heightScreen / 640,
    shadowColor: '#8B8989',
    shadowOffset: { width: 0, height: 3 },
    shadowOpacity: 3,
    shadowRadius: 2,
    elevation: 5,
  },
  button: {
    width: 250 * STYLES.widthScreen / 360,
    height: 40 * STYLES.heightScreen / 640,
    backgroundColor: colors.white,
    justifyContent: 'center',
    alignItems: 'center',
  },
  viewUser: {
    width: 250 * STYLES.widthScreen / 360,
    height: 60 * STYLES.heightScreen / 640,
    borderBottomWidth: 1.5,
    borderColor: '#ffff',
    justifyContent: 'center',
    marginBottom: 5 * STYLES.heightScreen / 640,
  },
  textEmail: {
    color: 'white',
    fontSize: STYLES.fontSizeText,
    width: 250 * STYLES.widthScreen / 360,
    height: 40 * STYLES.heightScreen / 640,
  },
  viewSkip: {
    flex: 0,
    marginTop:HEIGHT(10),
  }
})

