import { StyleSheet } from 'react-native';
//configs
import STYLES from '../../config/styles.config';
import colors from '../../config/colors.config';

const MARGINTOP = 30;
const sizeIconLeft = STYLES.heightItemUnit * 0.8;

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.backgroundColor
    },
    textAccept: {
        marginTop: (65 * STYLES.heightScreen / 830) * 0.5,
        fontSize: STYLES.fontSizeSubText,
        width: STYLES.widthScreen * 0.85,
        color: colors.colorWhite,
    },
    iconTab: {
        height: (24 * STYLES.heightScreen / 640),
        width: (20 * STYLES.heightScreen / 640),
    },
    buttonShowDrawer: Object.assign({
        ...STYLES.centerItem,
        width: (65 * STYLES.heightScreen / 830),
        height: (65 * STYLES.heightScreen / 830),
    }),

    frame: {
        marginTop: (65 * STYLES.heightScreen / 830) * 0.65,
        justifyContent: 'center',
        alignItems: 'center'
    },
    textEmail: {
        width: STYLES.widthScreen * 0.85,
        height: (65 * STYLES.heightScreen / 830) * 0.85,
        backgroundColor: '#398D6F',
        paddingLeft: STYLES.widthScreen * 0.05,
        borderRadius: 5,
        marginTop: (65 * STYLES.heightScreen / 830) * 0.2,
        fontSize: STYLES.fontSizeNormalText,
        color: colors.white
    },
    viewLostPassword: {
        width: STYLES.widthScreen * 0.85,
        justifyContent: 'center',
        alignItems: 'flex-end',
    },
    textLostPassword: {
        color: colors.colorWhite,
        fontSize: STYLES.fontSizeNormalText
    },
    buttonSignIn: {
        marginTop: (65 * STYLES.heightScreen / 830) * 0.65,
        width: STYLES.widthScreen * 0.5,
        height: (65 * STYLES.heightScreen / 830),
        borderRadius: (65 * STYLES.heightScreen / 830) / 2,
        backgroundColor: colors.colorHeader,
        justifyContent: 'center',
        alignItems: 'center',
        shadowColor: '#dcdcdc',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 3,
    },
    textSignIn: {
        color: colors.colorWhite,
        fontSize: STYLES.fontSizeText,
        textAlign: "center",
        textAlignVertical: "center"
    },
    button: {
        marginTop: (65 * STYLES.heightScreen / 830) * 0.5,
        backgroundColor: colors.colorHeader,
        borderRadius: (65 * STYLES.heightScreen / 830) * 0.8,
        height: (65 * STYLES.heightScreen / 830) * 0.8,
        width: STYLES.widthScreen * 0.65,
        justifyContent: 'center',
        alignItems: 'center'
    },
    viewButton: {
        flexDirection: "row",
        width: STYLES.widthScreen,
        height: 75 * STYLES.heightScreen / 830,
        justifyContent: 'center',
        backgroundColor: '#30885F',
        alignItems: 'flex-end',
    },
    textButton: {
        color: colors.colorWhite,
        textAlignVertical: "center",
        textAlign: "center",
        fontSize: STYLES.fontSizeText,
    },
    buttonNext: {
        height: (65 * STYLES.heightScreen / 830) * 0.8,
        width: STYLES.widthScreen * 0.65,
        justifyContent: 'center',
        alignItems: 'center'
    },
    textNext: {
        color: colors.white,
        fontSize: STYLES.fontSizeText,
        textDecorationLine: "underline"
    },
    textUsage: {
        color: colors.white,
        fontSize: STYLES.fontSizeText,
        flexWrap: "wrap"
    },
    header: {
        height: (65 * STYLES.heightScreen / 830),
        width: STYLES.widthScreen,
        flexDirection: 'row',
        backgroundColor: colors.colorRed,
        borderRadius: 2,
        borderColor: '#dcdcdc',
        borderWidth: 0.5,
        shadowColor: '#dcdcdc',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 3,
    },
    body: {
        flex: 1,
        alignItems: "center",
    },
    ButtonFirst: {
        height: (156 * STYLES.widthScreen / 360) * 156 / 648,
        width: 156 * STYLES.widthScreen / 360,
    },
    textButtonFirst: {
        fontSize: STYLES.fontSizeLabel,
        color: colors.white,
        marginLeft: 8 * STYLES.widthScreen / 360,
    },
    buttonFirst: {
        height: (60 * STYLES.heightScreen / 830),
        width: STYLES.widthScreen * 0.4,
        marginRight: STYLES.widthScreen * 0.05,
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        backgroundColor: colors.colorGray,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row'
    },
    lineHoac: {
        marginTop: (65 * STYLES.heightScreen / 830) * 0.3,
        width: STYLES.widthScreen * 0.85,
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
    },
    hoac: {
        fontSize: STYLES.fontSizeNormalText,
        color: '#64B9BA'
    },
    line: {
        height: (65 * STYLES.heightScreen / 830) * 0.005,
        borderBottomWidth: 0.3,
        borderColor: colors.colorGray,
        width: STYLES.widthScreen * 0.35,
        backgroundColor: '#62B3B3',
    },
    facebookandgoogle: {
        marginTop: (65 * STYLES.heightScreen / 830) * 0.3,
        flexDirection: "row",
        width: STYLES.widthScreen * 0.9,
        height: (65 * STYLES.heightScreen / 830),
        // justifyContent: "space-between",
        justifyContent: 'center',
        alignItems: 'center',
    },
    buttonSecond: {
        // height: (37 * STYLES.heightScreen / 640),
        // width: 160*STYLES.widthScreen /360,
        // borderRadius: 8,
        // backgroundColor: '#095DA1',
        // justifyContent: 'center',
        // alignItems: 'center',
        // flexDirection: 'row'
    },
    logo: Object.assign({
        ...STYLES.centerItem,
        flex: 1,
        paddingRight: (65 * STYLES.heightScreen / 830),
    }),
    textLogo: {
        fontSize: STYLES.fontSizeLabel,
        color: colors.white,
    },
    chapterIContainer: Object.assign({
        ...STYLES.centerItem,
        width: STYLES.widthScreen,
    }),
    itemChapterContainer: {
        width: STYLES.widthChapter,
        height: 'auto'
    },
    chapterNameContainer: Object.assign({
        ...STYLES.centerItem,
        width: STYLES.widthChapter,
        height: STYLES.heightItemUnit
    }),
    buttonLearnImmediately: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    textButtonLearnImmediately: {
        fontSize: STYLES.fontSizeNormalText,
        color: colors.gray
    }
});