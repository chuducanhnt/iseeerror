import { LoginManager, AccessToken } from 'react-native-fbsdk';
export const initUserFacebook=(self,token,id)=> {
    self.setState({isLoading:true})
		return fetch('https://graph.facebook.com/v3.2/' + id +"?fields=id,name"+ '&access_token=' + token)
			.then((response) => response.json())
			.then((json) => {
        self.setState({isLoading:false})
        let account={
          "hoVaTen": json.name,
          "avatar": 'https://graph.facebook.com/v3.2/' + id +"/picture?type=large&width=360&height=360",
          "soDienThoai": null,
          "gioiTinh": null,
          "tinhThanhPho": null,
          "quocGia": null,
          "ngaySinh": null,
          "password": "123",
          "username": json.id+'@facebook.com',
          "token": token
      }
      self.skip(account);
	});
}
export const loginFb=async(self)=>{
    await LoginManager.logInWithReadPermissions(["public_profile"]).then(
    function(result) {
      if (result.isCancelled) {
      } else {
        AccessToken.getCurrentAccessToken().then(
          (data) => {
            const { accessToken, userID } = data;
            initUserFacebook(self,accessToken, userID);
          }
        )
      }
    },
    function(error) {
      console.log("Login fail with error: " + error);
    }
  );
}