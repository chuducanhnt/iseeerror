/**
 * This component to show account
 * @huanhtm
 */
import React, { Component } from 'react'
import {
    View, StyleSheet, Image, ScrollView, Modal, Linking
} from 'react-native'

import ImageViewer from 'react-native-image-zoom-viewer';
import Swiper from 'react-native-swiper';
import { connect } from 'react-redux';
//import config
import IMAGE from '../../assets/image';
import styles from './styles'

//import common
import { ImageBackground, Text, NavigationBar, Title, Button, Caption, Heading, Subtitle } from '@shoutem/ui';

import HeaderReal from '../../common/HeaderReal';
import { CHI_TIET } from '../../config/default';
import { WIDTH, HEIGHT } from '../../config/Function';
import ItemForDetails from '../../common/ItemForDetails';
import FluteButton from '../../common/FluteButton';
import ItemImage from '../../common/ItemImage';
var dataFake = {
    namePlace: 'Cầu Long Biên',
    address: '2VV5+FV Bồ Đề, Long Biên, Hà Nội, Việt Nam',
    coordinates: '21.0437409,105.8575513',
    describe: 'Cầu Long Biên là cây cầu thép đầu tiên bắc qua sông Hồng, nối hai quận Hoàn Kiếm và Long Biên (Hà Nội), do Pháp xây dựng (1898-1920).Cầu dài 2.290 m vốn quen thuộc trong mắt người dân và khách du lịch từ góc nhìn ngang, hay góc hất từ dưới sông Hồng.Vì thế, có được một góc nhìn cầu Long Biên từ trên cao luôn là niềm mơ ước của nhiều người, đặc biệt những người yêu thích nhiếp ảnh.',
    listImage: 'https://we25.vn/media/Images/0(3)/12(2).jpg;https://we25.vn/media/Images/00/11(2).jpg;https://we25.vn/media/Images/0(1)/10(5).jpg;https://we25.vn/media/Images/)0/Serein_2.jpg;https://we25.vn/media/Images/00/Serein.jpg;https://we25.vn/media/Images/00000/9.jpg;https://we25.vn/media/Images/00000/8.jpg;https://we25.vn/media/Images/0(2/7(3).jpg;https://we25.vn/media/Images/0(2/2(11).jpg',
    imageMain: 'https://we25.vn/media/Images/0(3)/12(2).jpg',
    phoneNumber: '',
    type: 6,
    time: '21 giờ trước',
};
class Details extends Component<Props> {
    constructor(props) {
        super(props);
        this.state = {
            isModal: false,
            idImage: 0,
        }
    }
    onTouchable = (val, id) => {
        this.setState({ isModal: val, idImage: id })
    }
    render() {
        let content = this.props.navigation.getParam('content');
        // let content = dataFake;
        let listImage = content.listImage.split(";");
        let images = [];
        let listLeft = [];
        let listRight = [];
        listImage.map((value, index) => {
            if (index != 0) {
                if ((index - 1) % 2 == 0)
                    listLeft.push(
                        <ItemImage
                            key={index}
                            marginTop={WIDTH(3)}
                            widthImage={WIDTH(177)}
                            content={value}
                            onButton={() => this.onTouchable(true, index)}
                            index={index - 1}
                        />
                    )
                else
                    listRight.push(
                        <ItemImage
                            key={index}
                            marginTop={WIDTH(3)}
                            widthImage={WIDTH(177)}
                            content={value}
                            onButton={() => this.onTouchable(true, index)}
                            index={index - 1}
                        />
                    )
            }
            images.push({ url: value })
        });
        return (
            <View style={{
                flex: 1,
            }}>
                <ScrollView
                    contentContainerStyle={{
                        paddingBottom: HEIGHT(20),
                    }}
                    style={styles.container}>
                    <Modal
                        onRequestClose={() => this.onTouchable(false, 0)}
                        visible={this.state.isModal} transparent={true}>
                        <ImageViewer
                            index={this.state.idImage}
                            onSwipeDown={() => {
                                this.onTouchable(false, 0)
                            }}
                            enableSwipeDown={true}
                            imageUrls={images} />
                    </Modal>
                    <ItemForDetails
                        onShowMaps={() => {
                            let res = 'google.navigation:q=' + content.coordinates;
                            Linking.openURL(res);
                        }}
                        onTouchable={this.onTouchable}
                        onButton={() => this.props.navigation.goBack()}
                        linkImage={listImage[0]}
                        index={0}
                        content={content}
                    />

                    <View style={{
                        width: WIDTH(360),
                        flexDirection: 'row',
                        justifyContent: 'center',
                    }}>
                        <View style={{
                            flex: 0,
                            flexDirection: 'column',
                            width: WIDTH(180),
                        }}>
                            {listLeft}
                        </View>
                        <View style={{
                            flex: 0,
                            flexDirection: 'column',
                            width: WIDTH(180),
                        }}>
                            {listRight}
                        </View>
                    </View>
                </ScrollView>
                <FluteButton
                    onButton={() => this.props.navigation.navigate("Profile")}
                />
            </View>
        )
    }
}
function mapStateToProps(state) {
    return {
        count: state.rootReducer.count,
    };
}

export default connect(mapStateToProps, {
})(Details);