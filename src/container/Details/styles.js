import {
    StyleSheet,
 } from 'react-native'
 
 //import
 import IMAGE from '../../assets/image';
 import STYLES from '../../config/styles.config';
 import colors from '../../config/colors';
import { HEIGHT, WIDTH } from '../../config/Function';
 
 export default styles = StyleSheet.create({
     container: {
       flex:1,
     },
     cntMain:{
         flex:0,
         marginTop: HEIGHT(20),
         paddingHorizontal: WIDTH(10),
     }
 })
 
 