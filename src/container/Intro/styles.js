'use strict';
import {  StyleSheet, } from 'react-native';

//import config
import STYLES from '../../config/styles.config';
import Colors from '../../config/colors';

export default styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: Colors.lightGrayEEE,
      justifyContent: 'center',
      alignItems: 'center',
    },
    logoContainer: Object.assign(
      STYLES.centerItem,
      { flex: 0, }
    ),
    image: {
      width: STYLES.widthScreen * (179 / 360),
      height: STYLES.heightScreen * (133 / 640),
    },
    label: Object.assign(
      STYLES.centerItem,
      { flex: 0 ,}
    ),
    textLabel: {
      color: Colors.white,
      fontSize: STYLES.fontSizeLabel,
      fontFamily: 'Roboto-Regular',
      fontWeight: 'bold',
    }
  });