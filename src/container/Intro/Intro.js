
'use strict';
import { Image, StyleSheet, StatusBar, View, Text,AsyncStorage } from 'react-native';
import * as React from 'react';
import * as Animatable from 'react-native-animatable';
import { connect } from 'react-redux';
//import config

import {INIT_STORGE} from '../../config/default';
import IMAGE from '../../assets/image'
import styles from './styles';
import { changeAccount } from '../../actions/actionCreators';
import Colors from '../../config/colors';

class Intro extends React.PureComponent<Props> {
  constructor(props) {
    super(props);
    this.changeScreen = this.changeScreen.bind(this);
  }
  async componentWillMount(){
    let routeName='Home';
    await AsyncStorage.multiGet([
      INIT_STORGE
  ], async (err, results) => {
      let account=JSON.parse(results[0][1]);
      if (account!=undefined){
        routeName='Home';
        await this.props.changeAccount(account);
      }
      else {
        await this.props.changeAccount({});
      }
  });
    this.changeScreen(routeName);
  }
  changeScreen(routeName) {
    const { dispatch } = this.props.navigation;
      setTimeout(() => {
        dispatch({
          type: 'Navigation/RESET',
          actions: [{
              type: 'Navigate',
              routeName: routeName
          }], index: 0
      });
      }, 3500);
  }
  render() {
    return (
      <View style={styles.container}>
         <StatusBar backgroundColor={Colors.lightGrayEEE} />
        {/* <Animatable.View
          animation="bounceIn"
          direction="alternate"
          duration={4000}
          style={styles.logoContainer}>
          <Image
            resizeMode={'contain'}
            source={IMAGE.isee1}
            style={styles.image}
          />
        </Animatable.View> */}
        <Animatable.View
          animation="bounceInLeft"
          direction="alternate"
          duration={2500}
          style={styles.label}>
          <Text style={{
            color:'gray',
            fontSize:90,
            fontFamily:'Playlist-Script',
          }}>{'I See'}</Text>
        </Animatable.View>
      </View>
    );
  }
}
function mapStateToProps(state) {
  return { 
    count: state.rootReducer.count,
    account: state.accountReducer.account,
  };
}

export default connect(mapStateToProps, {
  changeAccount
})(Intro);

