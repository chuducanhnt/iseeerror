
import React, { Component } from 'react'
import { createAppContainer, createMaterialTopTabNavigator } from 'react-navigation';

import { HEIGHT, WIDTH } from '../../config/Function';
import STYLES from '../../config/styles.config';
import HotTrend from './HotTrend';
import TraSua from './TraSua';
import CaPhe from './CaPhe';
import Homestay from './Homestay';
import RapCp from './RapCp';
import Khac from './Khac';
const TabNavigatorRouter = createMaterialTopTabNavigator({
    HotTrend: { 
        screen: ({ navigation,screenProps }) => <HotTrend screenProps={screenProps} navigation={navigation} />,
        navigationOptions: {
			title:'Hot trend',
		  } 
    },
    CaPhe: { 
        screen: ({ navigation,screenProps }) => <CaPhe screenProps={screenProps} navigation={navigation} />,
        navigationOptions: {
			title:'Cà phê',
		  } 
    },
    TraSua: { 
        screen: ({ navigation,screenProps }) => <TraSua screenProps={screenProps} navigation={navigation} />,
        navigationOptions: {
			title:'Trà sữa',
		  } 
    },
    Homestay: { 
        screen: ({ navigation,screenProps }) => <Homestay screenProps={screenProps} navigation={navigation} />,
        navigationOptions: {
			title:'Homestay',
		  } 
    },
    RapCp: { 
        screen: ({ navigation,screenProps }) => <RapCp screenProps={screenProps} navigation={navigation} />,
        navigationOptions: {
			title:'Rạp chiếu phim',
		  } 
    },
    Khac: { 
        screen: ({ navigation,screenProps }) => <Khac screenProps={screenProps} navigation={navigation} />,
        navigationOptions: {
			title:'Khác',
		  } 
    },
},
    {
		headerMode: 'none',
		navigationOptions: {
			header: null
        },
        animationEnabled:true,
		lazy: true,
        tabBarOptions: {
            upperCaseLabel:false,
            scrollEnabled:true,
            activeTintColor: 'red',
            inactiveTintColor: 'black',
            indicatorStyle:{
                backgroundColor:'#ffff',
            },
            labelStyle:{
                fontSize:STYLES.fontSizeNormalText,
            },
            tabStyle:{
                backgroundColor:'#EFEFEF',
                borderRadius: HEIGHT(10),
                maxHeight: HEIGHT(30),
                maxWidth:WIDTH(130),
                // flex:0,
                marginRight:WIDTH(10),
                justifyContent: 'center',
                alignItems: 'center',
            },
            style:{
                backgroundColor:'#ffff',
                paddingHorizontal:WIDTH(10),
                paddingVertical:HEIGHT(10),
                flex:0,
                elevation:0,
            }
        },
    }
);
const TabView=createAppContainer(TabNavigatorRouter);
export default TabView;