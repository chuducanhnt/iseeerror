/**
 * This component to show account
 * @huanhtm
 */
import React, { Component } from 'react'

import { connect } from 'react-redux';



import { LIST_CA_PHE_MAPS } from '../../config/default';
import SampleViewAll from '../../common/SampleViewAll';


class CaPhe extends Component<Props> {
    render() {
        return (
            <SampleViewAll
                screenProps={this.props.screenProps}
                content={
                    {
                        title: 'Quan ca phe',
                        data: LIST_CA_PHE_MAPS,
                    }
                }
            />
        )
    }
}
function mapStateToProps(state) {
    return {
        count: state.rootReducer.count,
    };
}

export default connect(mapStateToProps, {
})(CaPhe);