/**
 * This component to show account
 * @huanhtm
 */
import React, { Component } from 'react'
import {
    View, StatusBar
} from 'react-native'

import { connect } from 'react-redux';

import { LIST_CA_PHE_MAPS, LIST_TRA_SUA, LIST_HOME_STAY, LIST_RAP_CP, LIST_HOT,TITLE_HOME, LIST_ALL } from '../../config/default';
import { incCount, decCount } from '../../actions/actionCreators'
import colors from '../../config/colors';
import FluteButton from '../../common/FluteButton';
import { LoadingComponent } from '../../common/LoadingComponet';
import ListImage from '../../common/Home/ListImage';

class HotTrend extends Component<Props> {
    state = {
        search: '',
        idTitle:0,
        isLoading:false
    };
    componentWillMount(){
        clearTimeout(this.interval);
        this.interval = setTimeout(() => {
            this.setState({isLoading:false})
        }, 1000);
    }
    updateSearch = search => {
        this.setState({ search });
    };
    onPost = (content) => {
        this.props.screenProps.navigate('Details',{content:content})
    }
    onViewAll = (id) => {
        if (id==1) this.props.screenProps.navigate('ViewAll',{content:{
            title:'Quán cà phê',
            data:LIST_CA_PHE_MAPS,
        }})
        if (id==2) this.props.screenProps.navigate('ViewAll',{content:{
            title:'Quán trà sữa',
            data:LIST_TRA_SUA,
        }})
        if (id==3) this.props.screenProps.navigate('ViewAll',{content:{
            title:'Homestay',
            data:LIST_HOME_STAY,
        }})
        if (id==4) this.props.screenProps.navigate('ViewAll',{content:{
            title:'Rạp chiếu phim mini',
            data:LIST_RAP_CP,
        }})
        if (id==5) this.props.screenProps.navigate('ViewAll',{content:{
            title:'Địa điểm khác',
            data:LIST_HOT,
        }})
    }
    
    render() {
        const { search } = this.state;
        return (
            <View style={{
                flex:1,
            }}>
                <StatusBar backgroundColor={colors.lightGrayEEE} />
                <ListImage
                    data={LIST_ALL}
                    onButton={this.onPost}
                />
                <LoadingComponent
                    isLoading={this.state.isLoading}
                />
                <FluteButton
                    onButton={()=> this.props.screenProps.navigate("Profile")}
                />
            </View>
        )
    }
}
function mapStateToProps(state) {
    return {
        count: state.rootReducer.count,
        account: state.accountReducer.account,
    };
}

export default connect(mapStateToProps, {
    incCount, decCount
})(HotTrend);
