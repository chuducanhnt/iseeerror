/**
 * This component to show account
 * @huanhtm
 */
import React, { Component } from 'react'

import { connect } from 'react-redux';



import { LIST_HOT } from '../../config/default';
import SampleViewAll from '../../common/SampleViewAll';


class Khac extends Component<Props> {
    render() {
        return (
            <SampleViewAll
                screenProps={this.props.screenProps}
                content={
                    {
                        title: 'Quan ca phe',
                        data: LIST_HOT,
                    }
                }
            />
        )
    }
}
function mapStateToProps(state) {
    return {
        count: state.rootReducer.count,
    };
}

export default connect(mapStateToProps, {
})(Khac);