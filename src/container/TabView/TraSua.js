/**
 * This component to show account
 * @huanhtm
 */
import React, { Component } from 'react'

import { connect } from 'react-redux';



import { LIST_TRA_SUA } from '../../config/default';
import SampleViewAll from '../../common/SampleViewAll';


class TraSua extends Component<Props> {
    render() {
        return (
            <SampleViewAll
                screenProps={this.props.screenProps}
                content={
                    {
                        title: 'Quan ca phe',
                        data: LIST_TRA_SUA,
                    }
                }
            />
        )
    }
}
function mapStateToProps(state) {
    return {
        count: state.rootReducer.count,
    };
}

export default connect(mapStateToProps, {
})(TraSua);