export const TRANG_CHU = 'Trang chủ';
export const CHI_TIET = 'Chi tiết';
export const NAME_PART2 = 'Part 2: Listening';
export const NAME_PART3 = 'Part 3: Dictation';
export const NAME_PART4 = 'Part 4: Pronunciation';

//AsyncStorage

export const TITLE_HOME = [
    'Hot trend','Cà phê','Trà sữa',
    'Homestay','Rạp chiếu phim','Khác'
];
export const INIT_STORGE = '@INIT_STORGE';
export const LIST_ALL = [
    {
        namePlace: 'Cầu Long Biên',
        address: '2VV5+FV Bồ Đề, Long Biên, Hà Nội, Việt Nam',
        coordinates: '21.0437409,105.8575513',
        describe: 'Cầu Long Biên là cây cầu thép đầu tiên bắc qua sông Hồng, nối hai quận Hoàn Kiếm và Long Biên (Hà Nội), do Pháp xây dựng (1898-1920).Cầu dài 2.290 m vốn quen thuộc trong mắt người dân và khách du lịch từ góc nhìn ngang, hay góc hất từ dưới sông Hồng.Vì thế, có được một góc nhìn cầu Long Biên từ trên cao luôn là niềm mơ ước của nhiều người, đặc biệt những người yêu thích nhiếp ảnh.', 
        listImage: 'https://we25.vn/media/Images/0(3)/12(2).jpg;https://drive.google.com/uc?export=download&id=1tiDfu2T3dYvuQqD9p93yNamQpe_jSSnS;https://drive.google.com/uc?export=download&id=16WCpB0nkbMcffTdmi_1oqbYwXAb0huzN;https://we25.vn/media/Images/)0/Serein_2.jpg;https://we25.vn/media/Images/00/Serein.jpg;https://we25.vn/media/Images/00000/9.jpg;https://we25.vn/media/Images/00000/8.jpg;https://we25.vn/media/Images/0(2/7(3).jpg;https://drive.google.com/uc?export=download&id=111vdnlIr7s_btpDGqqNfIlWfrMUWujUF', 
        imageMain: 'https://we25.vn/media/Images/0(3)/12(2).jpg',
        phoneNumber:'',
        type:6,
        time: '21 giờ trước',
    },
    // {
    //     namePlace: 'Bãi đá sông Hồng',
    //     address: 'Ngõ 264 Âu Cơ, Nhật Tân, Tây Hồ, Hà Nội',
    //     coordinates: '21.078527,105.8330781',
    //     describe: 'Nếu có dịp ghé thăm bãi đá sông Hồng, bạn sẽ được tận hưởng một không gian thật thanh bình với trời trong, gió nhẹ cùng những hàng lau trắng bạt ngàn... Nơi đây sẽ là địa điểm không thể bỏ qua nếu bạn có ý định chụp ảnh để lưu giữ những khoảnh khắc tuyệt đẹp.',
    //     listImage: 'http://saigontv.news/wp-content/uploads/2018/05/chup-anh-50k3.jpg;http://saigontv.news/wp-content/uploads/2018/05/chup-anh-50k4.jpg;http://saigontv.news/wp-content/uploads/2018/05/chup-anh-50k6.jpg;http://saigontv.news/wp-content/uploads/2018/05/chup-anh-50k5.jpg;https://photo-1-baomoi.zadn.vn/w1000_r1/16/11/27/229/20936316/1_170310.jpg;https://photo-1-baomoi.zadn.vn/w1000_r1/16/11/27/229/20936316/11_165665.jpg;https://photo-1-baomoi.zadn.vn/w1000_r1/16/11/27/229/20936316/2_152514.jpg;http://bit.ly/2ZbuHpI;http://media.we25.vn/images/(((0/21827373_1997418677181565_1294420821376237568_n-1024x768.jpg', 
    //     imageMain: 'http://saigontv.news/wp-content/uploads/2018/05/chup-anh-50k3.jpg',
    //     type:6,
    //     time: '21 giờ trước',
    // },
    // {
    //     namePlace: 'Nhà thờ Lớn',
    //     address: '40 Nhà Chung, Hàng Trống, Hoàn Kiếm, Hà Nội, Việt Nam',
    //     coordinates: '21.028733,105.846754',
    //     describe: 'Nhà thờ Lớn Hà Nội hay còn gọi là Nhà thờ Chính tòa Hà Nội là một công trình kiến trúc đặc sắc gắn liền với người dân Thủ đô qua nhiều thế kỷ. Nhà thờ là nơi sinh hoạt tín ngưỡng của các tín đồ theo đạo Thiên Chúa và là một địa điểm tham quan thu hút đông đảo du khách ở Hà Nội.',
    //     listImage: 'http://www.vietfuntravel.com.vn/image/data/Ha-Noi/nha-tho-lon/Gio-le-nha-tho-Lon-Ha-Noi-5.png;http://static2.yan.vn/YanNews/2167221/201804/5-dia-diem-chup-anh-sieu-dep-ngay-tai-ha-noi-44be85e0.jpg;http://a9.vietbao.vn/images/vn999/190/2015/05/20150527-facebook24h-angela-phuong-trinh-tung-tang-o-h-224-noi-1.jpg;http://media.depplus.vn/images/site-1/20151120/web/nha-tho-nao-duoc-gioi-tre-pose-hinh-nhieu-nhat-241-233407.jpg;https://innotour.vn/wp-content/uploads/2017/11/5-nha-tho-dep-nhat-viet-nam-1.jpg;https://vngo.vn/upload/users/user1/image-1547700929.4291.jpg;https://we25.vn/media/images/36-pho-phuong-29.jpg', 
    //     imageMain: 'http://www.vietfuntravel.com.vn/image/data/Ha-Noi/nha-tho-lon/Gio-le-nha-tho-Lon-Ha-Noi-5.png',
    //     type:6,
    //     time: '21 giờ trước',
    // },
    {
        namePlace: 'Xóm đường tàu',
        address: 'Phố Khâm Thiêm',
        coordinates: '21.0190975,105.8341665',
        describe: 'Xóm đường tàu đẹp như y như phố cổ Thập Phần ở Đài Bắc, Đài Loan.Và trở thành điểm "check-in" mới của giới trẻ và các du khách nước ngoài.',
        listImage: 'https://photo-3-baomoi.zadn.vn/w1000_r1/2017_12_21_180_24364664/06932dd81399fac7a388.jpg;https://media.metrip.vn/tour/content/thecrystyle.jpg;https://static2.yan.vn/MYanNews/201810/kham-pha-xom-duong-tau-tai-ha-noi-2f0f0a21.jpg;https://images.foody.vn/images/blogs/foody-upload-api-foody-whoaaaaaaaaa-636625940623467616-180522135242.jpg;https://media.metrip.vn/tour/content/dianalidia.jpg;http://bizweb.dktcdn.net/thumb/grande/100/101/075/articles/xom.jpg?v=1539655243057;https://scontent.fhan2-4.fna.fbcdn.net/v/t1.0-9/48368646_2252243508325217_1864520224603111424_n.jpg?_nc_cat=105&_nc_oc=AQm0PhCx9wpNeRphVVDxOPAXFFuK5xRf0DKPWHM7pR50s4dQGGn2-sAQ7-s7jSpYumQ&_nc_ht=scontent.fhan2-4.fna&oh=75e68c1782eddf1bcf34989f95035a54&oe=5D73E6C8', 
        imageMain: 'https://photo-3-baomoi.zadn.vn/w1000_r1/2017_12_21_180_24364664/06932dd81399fac7a388.jpg',
        type:6,
        time: '21 giờ trước',
    },
    {
        namePlace: 'Hồ Hoàn Kiếm',
        address: 'Hàng Trống, Hoàn Kiếm, Hà Nội, Việt Nam',
        coordinates: '21.0287797,105.850176',
        describe: 'Dù một mình hay với ai, hãy đến và cảm nhận nét đặc trưng của Hà Nội. Nghệ thuật đường phố, trò chơi dân gian,những bản tình ca ngẫu hứng, tìm lại chính mình, tìm lại chút ân tình xưa cũ, trải lòng bên mặt hồ.Bên cạnh đó, khám phá các khu phố cổ cũng vô cùng độc đáo, chắc chắn sẽ là địa điểm được check-in nhiều nhất',
        listImage: 'https://blog.traveloka.com/source/uploads/sites/9/2018/11/dia-diem-song-ao-ha-noi-24.jpg;https://scontent.fhan2-1.fna.fbcdn.net/v/t1.0-9/55790030_1246737122142120_7910726071513776128_n.jpg?_nc_cat=103&_nc_oc=AQkVc4mLwWsloQH2gYN2LI94ZWmQx7C6Vr9xZ_ZznnzGPAn8lzGR39tJsIZFqaxh9_Q&_nc_ht=scontent.fhan2-1.fna&oh=30a1c2a3358f5f16c0234940e5bb1520&oe=5D422143;https://scontent.fhan2-4.fna.fbcdn.net/v/t1.0-9/56157759_1246737142142118_5927727301444763648_n.jpg?_nc_cat=104&_nc_oc=AQnIJaeY75zR9MyTBMjug3p04rtPglO6UDCFYD652HeHKWqfWRA7X1FQolHgsOQdcU4&_nc_ht=scontent.fhan2-4.fna&oh=96cd87c50b581049c2a86aa5fece1d57&oe=5D3947AC;http://streaming1.danviet.vn/upload/1-2017/images/2017-01-16/14845623287022-anh-7.jpg', 
        imageMain: 'https://blog.traveloka.com/source/uploads/sites/9/2018/11/dia-diem-song-ao-ha-noi-24.jpg',
        type:6,
        time: '21 giờ trước',
    },
    {
        namePlace: 'Aon Mall Long Biên',
        address: 'Số 27 đường Cổ Linh, Long Biên, Hà Nội, Việt Nam',
        coordinates: '21.0269951,105.8968216',
        describe: 'Không hẳn là chốn dành riêng cho giới trẻ, nhưng sẽ cực kỳ tiếc nuối nếu như bạn bỏ qua khu trung tâm thương mại Aon Mall mới mở với tổ hợp ăn - chơi - mua sắm hiện đại và rất hay ho này.Cộng thêm hơi thở đặc trưng của người Nhật đã biến nơi đây thành một địa điểm nhất định phải check-in.',
        listImage: 'https://www.chudu24.com/wp-content/uploads/2018/05/n_huy_29738396_1642623139187129_5433719626187931648_n.jpg?fbclid=IwAR0JBY4Ee0u6eBPt_Z_a9JwgW0t5aGR9FlKNptTjFkXuSovV8QGlxGjsMEo;https://kenh14cdn.com/2015/12247034-1072069992812474-7710240765990871577-n-1450763194986.jpg?fbclid=IwAR0f3SugVahYEbj9yf9jaih6xOfNkgRyGKNzAhnpMGIBu9Ir-Ilg1CBhjrQ;https://i-ione.vnecdn.net/2015/11/28/trung-tam-thuong-mai-aeon-mall-5486-5793-1448684897.jpg?fbclid=IwAR0M7POPwR1_ZxhOmmFrbMmfOs21HiVE0OdAi2Elp6RgAxbsb8RR-4uJOns;http://cdn01.diadiemanuong.com/ddau/640x/bai-gui-xe-o-aeon-mall-binh-tan-gay-sot-voi-diem-chup-hinh-cuc-chat-74a85901636046277465630559.jpg?fbclid=IwAR2zRFHZ0rXPI1sWJyPGZ21lFEPREpebO5JjYLaXiyciL51ie-w4-798hAQ;https://media.metrip.vn/tour/content/dggmui_29november_32377363_210036679604478_2294008188174860288_n.jpg?fbclid=IwAR1W3VqFgPM8YHbQtJkyfNUzjA-GIe0fvCIcB8dUxO-OT3f484TElbu1gEY', 
        imageMain: 'https://www.chudu24.com/wp-content/uploads/2018/05/n_huy_29738396_1642623139187129_5433719626187931648_n.jpg?fbclid=IwAR0JBY4Ee0u6eBPt_Z_a9JwgW0t5aGR9FlKNptTjFkXuSovV8QGlxGjsMEo',
        type:6,
        time: '21 giờ trước',
    },
    {
        namePlace: 'Trill Rooftop Cafe',
        address: 'Tầng 26, Tòa nhà Hei Tower, 1 Ngụy Như Kon Tum, Nhân Chính, Thanh Xuân, Hà Nội',
        coordinates: '21.0032581,105.8051799',
        describe: ' Trill Group với tổ hợp cafe, bể bơi, phòng tập gym... - là một trong những quán cà phê đẹp ở Hà Nội có view cực chất.Nếu bạn thích trải nghiệm cảm giác vừa nhâm nhi tách trà ấm nóng hay chocolate ngọt ngào, vừa ngắm Hà Nội từ trên cao thì Trill Group là sự lựa chọn không thể lý tưởng hơn.Ngoài yếu tố không gian view và không gian, đồ ăn, đồ uống ở đây cũng khá đầy đủ. Mức giá đồ ở đây trung bình từ 40k/ món.', 
        listImage: 'https://images.foody.vn/images/14369204_1513957175297868_7285838755494100992_n.jpg;https://images.foody.vn/images/trillgroup.jpg;https://images.foody.vn/images/14449222_1037816323006133_4293765315042476032_n.jpg;https://images.foody.vn/images/14134940_1584382345199200_778613661_n.jpg;https://images.foody.vn/images/13774392_971524286308546_353305157_n.jpg', 
        imageMain: 'https://images.foody.vn/images/14369204_1513957175297868_7285838755494100992_n.jpg',
        phoneNumber:'',
        type:2,
        time: '21 giờ trước',
    },
    {
        namePlace: 'Cup of Tea Cafe & Bistro CS1',
        address: '317 Nguyễn Khang – Cầu Giấy',
        coordinates: '21.0344745,105.8006222',
        describe: 'Đúng như tên gọi, đồ uống chính tại COT chính là trà và cafe, tuy nhiên nếu muốn có một bức hình sống ảo siêu chất thì bạn hãy gọi một ấm trà nóng nhé, vừa được chụp ảnh lại vừa được nhâm nhi tách trà nóng hổi thơm ngon, tội gì không thử phải không nào?Nếu như bạn còn đang đau đầu tìm cho mình các quán cafe đẹp ở Hà Nội để chụp ảnh thì chắc chắn đừng bỏ qua COT nhé. Tại đây có đến 3 tầng với các góc khác nhau, nhưng đảm bảo rằng góc nào lên hình cũng chỉ có đẹp trở lên mà thôi.', 
        listImage: 'https://www.couturetravelcompany.com/wp-content/uploads/2018/10/sontungg_ph1.jpg;https://www.couturetravelcompany.com/wp-content/uploads/2018/10/cup-of-tea-cafe-bistro-JZht3Q1uMuUTXB7e-1399632-1486879994.jpg;https://www.couturetravelcompany.com/wp-content/uploads/2018/10/hienpham_.jpg;https://www.couturetravelcompany.com/wp-content/uploads/2018/10/coffeeholiicc1.jpg;https://www.google.com/url?sa=i&source=images&cd=&cad=rja&uact=8&ved=2ahUKEwixmuGrldbhAhUKH3AKHVq0DCUQjRx6BAgBEAU&url=https%3A%2F%2Fwww.foody.vn%2Fha-noi%2Fcup-of-tea-cafe-bistro-nguyen-khang%2Fbinh-luan-2036848&psig=AOvVaw0yQm1mFdflvjrUibO_WvPF&ust=1555557473380534', 
        imageMain: 'https://www.couturetravelcompany.com/wp-content/uploads/2018/10/sontungg_ph1.jpg',
        phoneNumber:'',
        type:2,
        time: '21 giờ trước',
    },
    {
        imageMain: 'https://scontent.fhan2-4.fna.fbcdn.net/v/t1.0-9/19894929_1727851937254507_562592798261651451_n.jpg?_nc_cat=105&_nc_oc=AQkq6n1qIgPS-HKiR_XSdx6zl8eV8emmDf8DitWFv_oSI26th9DZaVwqaDh1pVPnmUE&_nc_ht=scontent.fhan2-4.fna&oh=5cb59e1939491c23de9228f67227de8a&oe=5D4163F0',
        namePlace: 'House of cha',
        phoneNumber:'024 6668 9669',
        coordinates: '20.9911435,105.795996',
        address: '182 Lương Thế Vinh', 
        type:3,
        describe: 'House of Cha - Ngôi nhà trà sữa hàng đầu Việt Nam. Nơi thư giãn, giải trí của mọi lứa tuổi và là nơi có những không gian check-in tuyệt vời nhất.', 
        listImage: 'https://scontent.fhan2-4.fna.fbcdn.net/v/t1.0-9/19894929_1727851937254507_562592798261651451_n.jpg?_nc_cat=105&_nc_oc=AQkq6n1qIgPS-HKiR_XSdx6zl8eV8emmDf8DitWFv_oSI26th9DZaVwqaDh1pVPnmUE&_nc_ht=scontent.fhan2-4.fna&oh=5cb59e1939491c23de9228f67227de8a&oe=5D4163F0;https://scontent.fhan2-2.fna.fbcdn.net/v/t1.0-9/26196123_1712890375417330_6363366011220792421_n.jpg?_nc_cat=106&_nc_oc=AQmiDHoe5HyHWsF8UcPEqOfn5kw6tXs5wogeWtp_SFkJuu8b_YXUu8c9KvGCYdKKwRg&_nc_ht=scontent.fhan2-2.fna&oh=1c0efdc00845b672eaa1c10ad66851fe&oe=5D2A8577;https://scontent.fhan2-1.fna.fbcdn.net/v/t1.0-9/27067159_1727855673920800_1490750529629525949_n.jpg?_nc_cat=102&_nc_oc=AQmngfrEMchuXeZvmqeWH_zIXS3z_a3OuY2ElofC2lf5H2C3qb3OrLO8p3zjJRX-w48&_nc_ht=scontent.fhan2-1.fna&oh=5b8497d805a8f964099f7e6f308b1fc8&oe=5D775640;https://scontent.fhan2-1.fna.fbcdn.net/v/t1.0-9/21430343_787903304722596_5819259620711457202_n.jpg?_nc_cat=102&_nc_oc=AQkFW_yg0DAzXT_g5RAy6eLDDluoIgbG0GPGnCMRFAfytldEUqPvq3zTXsgZsqt9WRY&_nc_ht=scontent.fhan2-1.fna&oh=100a39411f6ca734fe4010d9e0b71a0f&oe=5D6F986E;https://scontent.fhan2-4.fna.fbcdn.net/v/t1.0-9/26731167_857953407717585_6679630345867683902_n.jpg?_nc_cat=104&_nc_oc=AQntZ-6ejfMl6yAUJdUpspCAUbQsP8CtXX3j2P0EE8JMSgejBCXsoWK5tGqs66vtuNg&_nc_ht=scontent.fhan2-4.fna&oh=5f02511c455e095bc5ff5b472e8e6ae7&oe=5D33C2A4;https://scontent.fhan2-1.fna.fbcdn.net/v/t1.0-9/27067159_1727855673920800_1490750529629525949_n.jpg?_nc_cat=102&_nc_oc=AQmngfrEMchuXeZvmqeWH_zIXS3z_a3OuY2ElofC2lf5H2C3qb3OrLO8p3zjJRX-w48&_nc_ht=scontent.fhan2-1.fna&oh=5b8497d805a8f964099f7e6f308b1fc8&oe=5D775640', 
        time: '21 giờ trước',
    },
    {
        namePlace: 'Dingtea',
        address: '23 Phố Lê Đại Hành, Lê Đại Hành, Hai Bà Trưng, Hà Nội',
        coordinates: '21.0087887,105.8487055',
        describe: 'Ding tea Hà Nội là Fanpage chính thức chủ quản chuỗi cửa hàng Ding Tea tại khu vực Hà Nội, mọi thông tin chi tiết, thắc mắc về sản phẩm – dịch vụ sẽ được Update và giải đáp trực tiếp qua Fanpage & Tổng đài 1900 2003. Ding Tea ( Đỉnh Trà ) - 100% Authentic Taiwanese Bubble Tea Thương hiệu Trà sữa trân châu Đài Loan uy tín trên thị trường các nước Singapore, Malaysia, China, Indonesia, Phillipines, Japan... đã có mặt tại Việt Nam! Nguyên liệu sử dụng để pha chế tại toàn bộ các cơ sở Ding Tea Hà Nội đều được sản xuất với tiêu chuẩn riêng của Ding Tea, được kiểm tra và đóng gói dưới tên thương hiệu Ding Tea để đảm bảo ko sử dụng hàng trôi nổi hoặc nguyên liệu ko phải của Ding Tea. An toàn và nhập khẩu 100% từ Đài Loan.', 
        listImage: 'https://scontent.fhan2-2.fna.fbcdn.net/v/t1.0-9/51335597_2328643850488877_1753798574757380096_n.jpg?_nc_cat=111&_nc_oc=AQk5UXiC6Q2Fo5MCuhbGngEpZ-JS1ScQQ4_ZB8Y3Ha5_jBARIdkMXkMXyCZvyPrxZt4&_nc_ht=scontent.fhan2-2.fna&oh=81ce3c2172f20325ca28f7ece7a8ed15&oe=5D31F84B;https://scontent.fhan2-1.fna.fbcdn.net/v/t1.0-9/50567773_2308801679139761_1048719971598729216_n.jpg?_nc_cat=102&_nc_oc=AQkp6ihVL5duMb5zXgTHzjCOtbMMvt8pbjt1ScXxJYTbcnBGOU3HeTvRTPg0Nghhozw&_nc_ht=scontent.fhan2-1.fna&oh=225fc26f8f924d72390328faf735ba31&oe=5D3C714C;https://scontent.fhan2-1.fna.fbcdn.net/v/t1.0-9/50567773_2308801679139761_1048719971598729216_n.jpg?_nc_cat=102&_nc_oc=AQkp6ihVL5duMb5zXgTHzjCOtbMMvt8pbjt1ScXxJYTbcnBGOU3HeTvRTPg0Nghhozw&_nc_ht=scontent.fhan2-1.fna&oh=225fc26f8f924d72390328faf735ba31&oe=5D3C714C;https://scontent.fhan2-2.fna.fbcdn.net/v/t1.0-9/50416149_2305746799445249_1339172855887167488_n.jpg?_nc_cat=106&_nc_oc=AQnSfSgmAf5wfUEmYPgEt8OkrxUU5H-LlM698zefdFu93mTekUL7bFklvSR1_5wbK0g&_nc_ht=scontent.fhan2-2.fna&oh=98305d356197cbef79ebd591a4072661&oe=5D36524A;https://scontent.fhan2-4.fna.fbcdn.net/v/t1.0-9/27972250_1834816813204919_3645555182875474323_n.jpg?_nc_cat=100&_nc_oc=AQkRPgYMaEG2JC34fJiXn_-ujkQSjCyfA7Wg0nR9F-D11Jrh-fyYPOTyvRZIplus_R4&_nc_ht=scontent.fhan2-4.fna&oh=08e5d479e714dd5fd9867587e52a5839&oe=5D2D92DA', 
        imageMain: 'https://scontent.fhan2-2.fna.fbcdn.net/v/t1.0-9/51335597_2328643850488877_1753798574757380096_n.jpg?_nc_cat=111&_nc_oc=AQk5UXiC6Q2Fo5MCuhbGngEpZ-JS1ScQQ4_ZB8Y3Ha5_jBARIdkMXkMXyCZvyPrxZt4&_nc_ht=scontent.fhan2-2.fna&oh=81ce3c2172f20325ca28f7ece7a8ed15&oe=5D31F84B',
        phoneNumber:'1900 2003',
        type:3,
        time: '21 giờ trước',
    },
    {
        namePlace: 'Heekcaa',
        address: '42C Lý Thường Kiệt, Tràng Tiền, Hoàn Kiếm, Hà Nội',
        coordinates: '21.0166641,105.8410868',
        describe: 'Không gian được decor theo cách đơn giản, nhã nhặn nhưng ko kém phần đáng yêu. Giá dao động từ 30000đ - 70000đ', 
        listImage: 'https://scontent.fhan2-3.fna.fbcdn.net/v/t1.0-9/54432415_1637097529725956_7845135852115066880_n.jpg?_nc_cat=108&_nc_oc=AQl8eIlpa9_XV_NUwYd6MgnxSeJSHRV2SVSvmyEV9BtYSyGJ5LMgI5mCDFiqr8BpQtQ&_nc_ht=scontent.fhan2-3.fna&oh=cc9114089f8b31290f64525d625e9df6&oe=5D4440F2;https://scontent.fhan2-4.fna.fbcdn.net/v/t1.0-9/51308548_1583870301715346_7454797017957531648_n.jpg?_nc_cat=104&_nc_oc=AQlbGq3ccomlnKnQ8kd-XmTnbKiup-K9kwR4w-KmfjnLl9PxF3FjSwxTouXhIv9UAJw&_nc_ht=scontent.fhan2-4.fna&oh=4ce1eab0555b49978d3afc3c537ace27&oe=5D312430;https://scontent.fhan2-3.fna.fbcdn.net/v/t1.0-9/47220476_1502492386519805_1194961937285251072_n.jpg?_nc_cat=107&_nc_oc=AQnm4cLR2zE-kzRsIZ-n1ppxVpJadO1oo44iDI0EBocz0mY9K2BWi54YhYkJvn8pN6I&_nc_ht=scontent.fhan2-3.fna&oh=6b6a4a70597a585070a4244e77150414&oe=5D2F3CE0;https://scontent.fhan2-3.fna.fbcdn.net/v/t1.0-9/46497910_1487089754726735_1741055883531517952_n.jpg?_nc_cat=109&_nc_oc=AQmfZmK8mtBpmPjbUykyj2pvCnlIYnDYFmQkuy_Y96Zmk2f0xCFBd0wb1giro1MmNXk&_nc_ht=scontent.fhan2-3.fna&oh=e42f14bccf6ce093545ab16d185d00e7&oe=5D2C9A4E', 
        imageMain: 'https://scontent.fhan2-3.fna.fbcdn.net/v/t1.0-9/54432415_1637097529725956_7845135852115066880_n.jpg?_nc_cat=108&_nc_oc=AQl8eIlpa9_XV_NUwYd6MgnxSeJSHRV2SVSvmyEV9BtYSyGJ5LMgI5mCDFiqr8BpQtQ&_nc_ht=scontent.fhan2-3.fna&oh=cc9114089f8b31290f64525d625e9df6&oe=5D4440F2',
        phoneNumber:'0122 320 2020',
        type:3,
        time: '21 giờ trước',
    },
    {
        namePlace: 'Harmony Homestay',
        address: '36 Ngõ Phát Lộc, Quận Hoàn Kiếm, Hà Nội',
        coordinates: '21.0343805,105.8518779',
        describe: 'Dân du lịch chuyên nghiệp lỡ bị mê Hà Nội thường sẽ biết đến Harmony Homestay. Vì sao? Vị trí của homestay tại Hà Nội này thông ra 3 con phố sầm uất, là nơi tụ tập cuối tuần của giới trẻ Hà Thành gồm Lương Ngọc Quyến, Nguyễn Hữu Huân và Hàng Mắm, cách Hồ Gươm vài phút đi bộ. Khi thành phố lên đèn, các con phố này lại nhộn nhịp khó tả bởi hàng quán, quán vỉa hè cho đến các quán bar cực chất, cực tự do.', 
        listImage: 'https://blog.traveloka.com/source/uploads/sites/9/2018/11/homestay-tai-ha-noi-3-1.jpg;https://blog.traveloka.com/source/uploads/sites/9/2018/11/homestay-tai-ha-noi-3-2.jpg;https://blog.traveloka.com/source/uploads/sites/9/2018/11/homestay-tai-ha-noi-3-3.jpg;https://blog.traveloka.com/source/uploads/sites/9/2018/11/homestay-tai-ha-noi-3-4.jpg;https://blog.traveloka.com/source/uploads/sites/9/2018/11/homestay-tai-ha-noi-3-6.jpg', 
        imageMain: 'https://blog.traveloka.com/source/uploads/sites/9/2018/11/homestay-tai-ha-noi-3-1.jpg',
        phoneNumber:'',
        type:4,
        time: '21 giờ trước',
    },
    {
        namePlace: 'Cafe phim Hà Đông',
        address: 'Số 30 An Hòa, P. Mộ Lao, Hà Đông, Hà Nội',
        coordinates: '20.9838174,105.7877678',
        describe: 'Đây là địa điểm cafe phim 3D ở Hà Đông được nhiều cặp đôi lựa chọn. Trong không gian yên tĩnh và riêng tư của phòng phim riêng, bạn có thể thoải mái chuyện trò, bình luận sôi nổi về bộ phim đang chiếu mà không e ngại sẽ làm phiền tới bất cứ ai.', 
        listImage: 'https://dulichtoday.vn/wp-content/uploads/2018/09/Cafe-Phim-Ha-Dong.jpg;https://dulichtoday.vn/wp-content/uploads/2018/09/Cafe-Phim-Ha-Dong1-1.jpg;https://dulichtoday.vn/wp-content/uploads/2018/09/Cafe-Phim-3D-Kinomax.jpg', 
        imageMain: 'https://dulichtoday.vn/wp-content/uploads/2018/09/Cafe-Phim-Ha-Dong.jpg',
        phoneNumber:'',
        type:5,
        time: '21 giờ trước',
    }
]
export const LIST_HOT = [
    {
        namePlace: 'Cầu Long Biên',
        address: '2VV5+FV Bồ Đề, Long Biên, Hà Nội, Việt Nam',
        coordinates: '21.0437409,105.8575513',
        describe: 'Cầu Long Biên là cây cầu thép đầu tiên bắc qua sông Hồng, nối hai quận Hoàn Kiếm và Long Biên (Hà Nội), do Pháp xây dựng (1898-1920).Cầu dài 2.290 m vốn quen thuộc trong mắt người dân và khách du lịch từ góc nhìn ngang, hay góc hất từ dưới sông Hồng.Vì thế, có được một góc nhìn cầu Long Biên từ trên cao luôn là niềm mơ ước của nhiều người, đặc biệt những người yêu thích nhiếp ảnh.', 
        listImage: 'https://we25.vn/media/Images/0(3)/12(2).jpg;https://drive.google.com/uc?export=download&id=1tiDfu2T3dYvuQqD9p93yNamQpe_jSSnS;https://drive.google.com/uc?export=download&id=16WCpB0nkbMcffTdmi_1oqbYwXAb0huzN;https://we25.vn/media/Images/)0/Serein_2.jpg;https://we25.vn/media/Images/00/Serein.jpg;https://we25.vn/media/Images/00000/9.jpg;https://we25.vn/media/Images/00000/8.jpg;https://we25.vn/media/Images/0(2/7(3).jpg;https://drive.google.com/uc?export=download&id=111vdnlIr7s_btpDGqqNfIlWfrMUWujUF', 
        imageMain: 'https://we25.vn/media/Images/0(3)/12(2).jpg',
        phoneNumber:'',
        type:6,
        time: '21 giờ trước',
    },
    // {
    //     namePlace: 'Bãi đá sông Hồng',
    //     address: 'Ngõ 264 Âu Cơ, Nhật Tân, Tây Hồ, Hà Nội',
    //     coordinates: '21.078527,105.8330781',
    //     describe: 'Nếu có dịp ghé thăm bãi đá sông Hồng, bạn sẽ được tận hưởng một không gian thật thanh bình với trời trong, gió nhẹ cùng những hàng lau trắng bạt ngàn... Nơi đây sẽ là địa điểm không thể bỏ qua nếu bạn có ý định chụp ảnh để lưu giữ những khoảnh khắc tuyệt đẹp.',
    //     listImage: 'http://saigontv.news/wp-content/uploads/2018/05/chup-anh-50k3.jpg;http://saigontv.news/wp-content/uploads/2018/05/chup-anh-50k4.jpg;http://saigontv.news/wp-content/uploads/2018/05/chup-anh-50k6.jpg;http://saigontv.news/wp-content/uploads/2018/05/chup-anh-50k5.jpg;https://photo-1-baomoi.zadn.vn/w1000_r1/16/11/27/229/20936316/1_170310.jpg;https://photo-1-baomoi.zadn.vn/w1000_r1/16/11/27/229/20936316/11_165665.jpg;https://photo-1-baomoi.zadn.vn/w1000_r1/16/11/27/229/20936316/2_152514.jpg;http://bit.ly/2ZbuHpI;http://media.we25.vn/images/(((0/21827373_1997418677181565_1294420821376237568_n-1024x768.jpg', 
    //     imageMain: 'http://saigontv.news/wp-content/uploads/2018/05/chup-anh-50k3.jpg',
    //     type:6,
    //     time: '21 giờ trước',
    // },
    // {
    //     namePlace: 'Nhà thờ Lớn',
    //     address: '40 Nhà Chung, Hàng Trống, Hoàn Kiếm, Hà Nội, Việt Nam',
    //     coordinates: '21.028733,105.846754',
    //     describe: 'Nhà thờ Lớn Hà Nội hay còn gọi là Nhà thờ Chính tòa Hà Nội là một công trình kiến trúc đặc sắc gắn liền với người dân Thủ đô qua nhiều thế kỷ. Nhà thờ là nơi sinh hoạt tín ngưỡng của các tín đồ theo đạo Thiên Chúa và là một địa điểm tham quan thu hút đông đảo du khách ở Hà Nội.',
    //     listImage: 'https://drive.google.com/uc?export=download&id=1oMd1AYy20cHu2jLlKnHcl8YpblgfNd1r;http://static2.yan.vn/YanNews/2167221/201804/5-dia-diem-chup-anh-sieu-dep-ngay-tai-ha-noi-44be85e0.jpg;http://a9.vietbao.vn/images/vn999/190/2015/05/20150527-facebook24h-angela-phuong-trinh-tung-tang-o-h-224-noi-1.jpg;http://media.depplus.vn/images/site-1/20151120/web/nha-tho-nao-duoc-gioi-tre-pose-hinh-nhieu-nhat-241-233407.jpg;https://innotour.vn/wp-content/uploads/2017/11/5-nha-tho-dep-nhat-viet-nam-1.jpg;https://vngo.vn/upload/users/user1/image-1547700929.4291.jpg;https://we25.vn/media/images/36-pho-phuong-29.jpg', 
    //     imageMain: 'https://drive.google.com/uc?export=download&id=1oMd1AYy20cHu2jLlKnHcl8YpblgfNd1r',
    //     type:6,
    //     time: '21 giờ trước',
    // },
    {
        namePlace: 'Xóm đường tàu',
        address: 'Phố Khâm Thiêm',
        coordinates: '21.0190975,105.8341665',
        describe: 'Xóm đường tàu đẹp như y như phố cổ Thập Phần ở Đài Bắc, Đài Loan.Và trở thành điểm "check-in" mới của giới trẻ và các du khách nước ngoài.',
        listImage: 'https://photo-3-baomoi.zadn.vn/w1000_r1/2017_12_21_180_24364664/06932dd81399fac7a388.jpg;https://media.metrip.vn/tour/content/thecrystyle.jpg;https://static2.yan.vn/MYanNews/201810/kham-pha-xom-duong-tau-tai-ha-noi-2f0f0a21.jpg;https://images.foody.vn/images/blogs/foody-upload-api-foody-whoaaaaaaaaa-636625940623467616-180522135242.jpg;https://media.metrip.vn/tour/content/dianalidia.jpg;http://bizweb.dktcdn.net/thumb/grande/100/101/075/articles/xom.jpg?v=1539655243057;https://scontent.fhan2-4.fna.fbcdn.net/v/t1.0-9/48368646_2252243508325217_1864520224603111424_n.jpg?_nc_cat=105&_nc_oc=AQm0PhCx9wpNeRphVVDxOPAXFFuK5xRf0DKPWHM7pR50s4dQGGn2-sAQ7-s7jSpYumQ&_nc_ht=scontent.fhan2-4.fna&oh=75e68c1782eddf1bcf34989f95035a54&oe=5D73E6C8', 
        imageMain: 'https://photo-3-baomoi.zadn.vn/w1000_r1/2017_12_21_180_24364664/06932dd81399fac7a388.jpg',
        type:6,
        time: '21 giờ trước',
    },
    {
        namePlace: 'Hồ Hoàn Kiếm',
        address: 'Hàng Trống, Hoàn Kiếm, Hà Nội, Việt Nam',
        coordinates: '21.0287797,105.850176',
        describe: 'Dù một mình hay với ai, hãy đến và cảm nhận nét đặc trưng của Hà Nội. Nghệ thuật đường phố, trò chơi dân gian,những bản tình ca ngẫu hứng, tìm lại chính mình, tìm lại chút ân tình xưa cũ, trải lòng bên mặt hồ.Bên cạnh đó, khám phá các khu phố cổ cũng vô cùng độc đáo, chắc chắn sẽ là địa điểm được check-in nhiều nhất',
        listImage: 'https://blog.traveloka.com/source/uploads/sites/9/2018/11/dia-diem-song-ao-ha-noi-24.jpg;https://scontent.fhan2-1.fna.fbcdn.net/v/t1.0-9/55790030_1246737122142120_7910726071513776128_n.jpg?_nc_cat=103&_nc_oc=AQkVc4mLwWsloQH2gYN2LI94ZWmQx7C6Vr9xZ_ZznnzGPAn8lzGR39tJsIZFqaxh9_Q&_nc_ht=scontent.fhan2-1.fna&oh=30a1c2a3358f5f16c0234940e5bb1520&oe=5D422143;https://scontent.fhan2-4.fna.fbcdn.net/v/t1.0-9/56157759_1246737142142118_5927727301444763648_n.jpg?_nc_cat=104&_nc_oc=AQnIJaeY75zR9MyTBMjug3p04rtPglO6UDCFYD652HeHKWqfWRA7X1FQolHgsOQdcU4&_nc_ht=scontent.fhan2-4.fna&oh=96cd87c50b581049c2a86aa5fece1d57&oe=5D3947AC;http://streaming1.danviet.vn/upload/1-2017/images/2017-01-16/14845623287022-anh-7.jpg', 
        imageMain: 'https://blog.traveloka.com/source/uploads/sites/9/2018/11/dia-diem-song-ao-ha-noi-24.jpg',
        type:6,
        time: '21 giờ trước',
    },
    {
        namePlace: 'Aon Mall Long Biên',
        address: 'Số 27 đường Cổ Linh, Long Biên, Hà Nội, Việt Nam',
        coordinates: '21.0269951,105.8968216',
        describe: 'Không hẳn là chốn dành riêng cho giới trẻ, nhưng sẽ cực kỳ tiếc nuối nếu như bạn bỏ qua khu trung tâm thương mại Aon Mall mới mở với tổ hợp ăn - chơi - mua sắm hiện đại và rất hay ho này.Cộng thêm hơi thở đặc trưng của người Nhật đã biến nơi đây thành một địa điểm nhất định phải check-in.',
        listImage: 'https://www.chudu24.com/wp-content/uploads/2018/05/n_huy_29738396_1642623139187129_5433719626187931648_n.jpg?fbclid=IwAR0JBY4Ee0u6eBPt_Z_a9JwgW0t5aGR9FlKNptTjFkXuSovV8QGlxGjsMEo;https://kenh14cdn.com/2015/12247034-1072069992812474-7710240765990871577-n-1450763194986.jpg?fbclid=IwAR0f3SugVahYEbj9yf9jaih6xOfNkgRyGKNzAhnpMGIBu9Ir-Ilg1CBhjrQ;https://i-ione.vnecdn.net/2015/11/28/trung-tam-thuong-mai-aeon-mall-5486-5793-1448684897.jpg?fbclid=IwAR0M7POPwR1_ZxhOmmFrbMmfOs21HiVE0OdAi2Elp6RgAxbsb8RR-4uJOns;http://cdn01.diadiemanuong.com/ddau/640x/bai-gui-xe-o-aeon-mall-binh-tan-gay-sot-voi-diem-chup-hinh-cuc-chat-74a85901636046277465630559.jpg?fbclid=IwAR2zRFHZ0rXPI1sWJyPGZ21lFEPREpebO5JjYLaXiyciL51ie-w4-798hAQ;https://media.metrip.vn/tour/content/dggmui_29november_32377363_210036679604478_2294008188174860288_n.jpg?fbclid=IwAR1W3VqFgPM8YHbQtJkyfNUzjA-GIe0fvCIcB8dUxO-OT3f484TElbu1gEY', 
        imageMain: 'https://www.chudu24.com/wp-content/uploads/2018/05/n_huy_29738396_1642623139187129_5433719626187931648_n.jpg?fbclid=IwAR0JBY4Ee0u6eBPt_Z_a9JwgW0t5aGR9FlKNptTjFkXuSovV8QGlxGjsMEo',
        type:6,
        time: '21 giờ trước',
    },
];


export const LIST_CAFE = [
    {
        imageMain: 'https://www.couturetravelcompany.com/wp-content/uploads/2018/10/hienpham_.jpg',
        namePlace: 'Cup of Tea Cafe & Bistro',
        coordinates: '21.0251967,105.7966571',
        phoneNumber:'0839 931 716',
        address: '317 Nguyễn Khang, Yên Hoà, Cầu Giấy, Hà Nội',
        describe: 'Trill Group với tổ hợp cafe, bể bơi, phòng tập gym... - là một trong những quán cà phê đẹp ở Hà Nội có view cực chất.Nếu bạn thích trải nghiệm cảm giác vừa nhâm nhi tách trà ấm nóng hay chocolate ngọt ngào, vừa ngắm Hà Nội từ trên cao thì Trill Group là sự lựa chọn không thể lý tưởng hơn.Ngoài yếu tố không gian view và không gian, đồ ăn, đồ uống ở đây cũng khá đầy đủ. Mức giá đồ ở đây trung bình từ 40k/ món.',
        listImage: 'https://scontent.fhan2-4.fna.fbcdn.net/v/t1.0-9/19894929_1727851937254507_562592798261651451_n.jpg?_nc_cat=105&_nc_oc=AQkq6n1qIgPS-HKiR_XSdx6zl8eV8emmDf8DitWFv_oSI26th9DZaVwqaDh1pVPnmUE&_nc_ht=scontent.fhan2-4.fna&oh=5cb59e1939491c23de9228f67227de8a&oe=5D4163F0;https://scontent.fhan2-2.fna.fbcdn.net/v/t1.0-9/26196123_1712890375417330_6363366011220792421_n.jpg?_nc_cat=106&_nc_oc=AQmiDHoe5HyHWsF8UcPEqOfn5kw6tXs5wogeWtp_SFkJuu8b_YXUu8c9KvGCYdKKwRg&_nc_ht=scontent.fhan2-2.fna&oh=1c0efdc00845b672eaa1c10ad66851fe&oe=5D2A8577;https://scontent.fhan2-1.fna.fbcdn.net/v/t1.0-9/27067159_1727855673920800_1490750529629525949_n.jpg?_nc_cat=102&_nc_oc=AQmngfrEMchuXeZvmqeWH_zIXS3z_a3OuY2ElofC2lf5H2C3qb3OrLO8p3zjJRX-w48&_nc_ht=scontent.fhan2-1.fna&oh=5b8497d805a8f964099f7e6f308b1fc8&oe=5D775640;https://scontent.fhan2-1.fna.fbcdn.net/v/t1.0-9/21430343_787903304722596_5819259620711457202_n.jpg?_nc_cat=102&_nc_oc=AQkFW_yg0DAzXT_g5RAy6eLDDluoIgbG0GPGnCMRFAfytldEUqPvq3zTXsgZsqt9WRY&_nc_ht=scontent.fhan2-1.fna&oh=100a39411f6ca734fe4010d9e0b71a0f&oe=5D6F986E;https://scontent.fhan2-4.fna.fbcdn.net/v/t1.0-9/26731167_857953407717585_6679630345867683902_n.jpg?_nc_cat=104&_nc_oc=AQntZ-6ejfMl6yAUJdUpspCAUbQsP8CtXX3j2P0EE8JMSgejBCXsoWK5tGqs66vtuNg&_nc_ht=scontent.fhan2-4.fna&oh=5f02511c455e095bc5ff5b472e8e6ae7&oe=5D33C2A4;https://scontent.fhan2-1.fna.fbcdn.net/v/t1.0-9/27067159_1727855673920800_1490750529629525949_n.jpg?_nc_cat=102&_nc_oc=AQmngfrEMchuXeZvmqeWH_zIXS3z_a3OuY2ElofC2lf5H2C3qb3OrLO8p3zjJRX-w48&_nc_ht=scontent.fhan2-1.fna&oh=5b8497d805a8f964099f7e6f308b1fc8&oe=5D775640',  time: '21 giờ trước',
    },
    {
        imageMain: 'https://www.couturetravelcompany.com/wp-content/uploads/2018/10/1658_17882898_529408880781045_1811213216533970944_n1.jpg',
        namePlace: 'Cafe Lissom Parlour',
        coordinates: '21.0251967,105.7966571', address: '317 Nguyễn Khang, Yên Hoà, Cầu Giấy, Hà Nội', describe: 'Trill Group với tổ hợp cafe, bể bơi, phòng tập gym... - là một trong những quán cà phê đẹp ở Hà Nội có view cực chất.Nếu bạn thích trải nghiệm cảm giác vừa nhâm nhi tách trà ấm nóng hay chocolate ngọt ngào, vừa ngắm Hà Nội từ trên cao thì Trill Group là sự lựa chọn không thể lý tưởng hơn.Ngoài yếu tố không gian view và không gian, đồ ăn, đồ uống ở đây cũng khá đầy đủ. Mức giá đồ ở đây trung bình từ 40k/ món.', listImage: 'https://scontent.fhan2-4.fna.fbcdn.net/v/t1.0-9/19894929_1727851937254507_562592798261651451_n.jpg?_nc_cat=105&_nc_oc=AQkq6n1qIgPS-HKiR_XSdx6zl8eV8emmDf8DitWFv_oSI26th9DZaVwqaDh1pVPnmUE&_nc_ht=scontent.fhan2-4.fna&oh=5cb59e1939491c23de9228f67227de8a&oe=5D4163F0;https://scontent.fhan2-2.fna.fbcdn.net/v/t1.0-9/26196123_1712890375417330_6363366011220792421_n.jpg?_nc_cat=106&_nc_oc=AQmiDHoe5HyHWsF8UcPEqOfn5kw6tXs5wogeWtp_SFkJuu8b_YXUu8c9KvGCYdKKwRg&_nc_ht=scontent.fhan2-2.fna&oh=1c0efdc00845b672eaa1c10ad66851fe&oe=5D2A8577;https://scontent.fhan2-1.fna.fbcdn.net/v/t1.0-9/27067159_1727855673920800_1490750529629525949_n.jpg?_nc_cat=102&_nc_oc=AQmngfrEMchuXeZvmqeWH_zIXS3z_a3OuY2ElofC2lf5H2C3qb3OrLO8p3zjJRX-w48&_nc_ht=scontent.fhan2-1.fna&oh=5b8497d805a8f964099f7e6f308b1fc8&oe=5D775640;https://scontent.fhan2-1.fna.fbcdn.net/v/t1.0-9/21430343_787903304722596_5819259620711457202_n.jpg?_nc_cat=102&_nc_oc=AQkFW_yg0DAzXT_g5RAy6eLDDluoIgbG0GPGnCMRFAfytldEUqPvq3zTXsgZsqt9WRY&_nc_ht=scontent.fhan2-1.fna&oh=100a39411f6ca734fe4010d9e0b71a0f&oe=5D6F986E;https://scontent.fhan2-4.fna.fbcdn.net/v/t1.0-9/26731167_857953407717585_6679630345867683902_n.jpg?_nc_cat=104&_nc_oc=AQntZ-6ejfMl6yAUJdUpspCAUbQsP8CtXX3j2P0EE8JMSgejBCXsoWK5tGqs66vtuNg&_nc_ht=scontent.fhan2-4.fna&oh=5f02511c455e095bc5ff5b472e8e6ae7&oe=5D33C2A4;https://scontent.fhan2-1.fna.fbcdn.net/v/t1.0-9/27067159_1727855673920800_1490750529629525949_n.jpg?_nc_cat=102&_nc_oc=AQmngfrEMchuXeZvmqeWH_zIXS3z_a3OuY2ElofC2lf5H2C3qb3OrLO8p3zjJRX-w48&_nc_ht=scontent.fhan2-1.fna&oh=5b8497d805a8f964099f7e6f308b1fc8&oe=5D775640',  time: '21 giờ trước',
    },
    {
        imageMain: 'https://www.couturetravelcompany.com/wp-content/uploads/2018/10/cafegardenista.jpg',
        namePlace: 'The YLang',
        coordinates: '21.0251967,105.7966571', address: '317 Nguyễn Khang, Yên Hoà, Cầu Giấy, Hà Nội', describe: 'Trill Group với tổ hợp cafe, bể bơi, phòng tập gym... - là một trong những quán cà phê đẹp ở Hà Nội có view cực chất.Nếu bạn thích trải nghiệm cảm giác vừa nhâm nhi tách trà ấm nóng hay chocolate ngọt ngào, vừa ngắm Hà Nội từ trên cao thì Trill Group là sự lựa chọn không thể lý tưởng hơn.Ngoài yếu tố không gian view và không gian, đồ ăn, đồ uống ở đây cũng khá đầy đủ. Mức giá đồ ở đây trung bình từ 40k/ món.', listImage: 'https://scontent.fhan2-4.fna.fbcdn.net/v/t1.0-9/19894929_1727851937254507_562592798261651451_n.jpg?_nc_cat=105&_nc_oc=AQkq6n1qIgPS-HKiR_XSdx6zl8eV8emmDf8DitWFv_oSI26th9DZaVwqaDh1pVPnmUE&_nc_ht=scontent.fhan2-4.fna&oh=5cb59e1939491c23de9228f67227de8a&oe=5D4163F0;https://scontent.fhan2-2.fna.fbcdn.net/v/t1.0-9/26196123_1712890375417330_6363366011220792421_n.jpg?_nc_cat=106&_nc_oc=AQmiDHoe5HyHWsF8UcPEqOfn5kw6tXs5wogeWtp_SFkJuu8b_YXUu8c9KvGCYdKKwRg&_nc_ht=scontent.fhan2-2.fna&oh=1c0efdc00845b672eaa1c10ad66851fe&oe=5D2A8577;https://scontent.fhan2-1.fna.fbcdn.net/v/t1.0-9/27067159_1727855673920800_1490750529629525949_n.jpg?_nc_cat=102&_nc_oc=AQmngfrEMchuXeZvmqeWH_zIXS3z_a3OuY2ElofC2lf5H2C3qb3OrLO8p3zjJRX-w48&_nc_ht=scontent.fhan2-1.fna&oh=5b8497d805a8f964099f7e6f308b1fc8&oe=5D775640;https://scontent.fhan2-1.fna.fbcdn.net/v/t1.0-9/21430343_787903304722596_5819259620711457202_n.jpg?_nc_cat=102&_nc_oc=AQkFW_yg0DAzXT_g5RAy6eLDDluoIgbG0GPGnCMRFAfytldEUqPvq3zTXsgZsqt9WRY&_nc_ht=scontent.fhan2-1.fna&oh=100a39411f6ca734fe4010d9e0b71a0f&oe=5D6F986E;https://scontent.fhan2-4.fna.fbcdn.net/v/t1.0-9/26731167_857953407717585_6679630345867683902_n.jpg?_nc_cat=104&_nc_oc=AQntZ-6ejfMl6yAUJdUpspCAUbQsP8CtXX3j2P0EE8JMSgejBCXsoWK5tGqs66vtuNg&_nc_ht=scontent.fhan2-4.fna&oh=5f02511c455e095bc5ff5b472e8e6ae7&oe=5D33C2A4;https://scontent.fhan2-1.fna.fbcdn.net/v/t1.0-9/27067159_1727855673920800_1490750529629525949_n.jpg?_nc_cat=102&_nc_oc=AQmngfrEMchuXeZvmqeWH_zIXS3z_a3OuY2ElofC2lf5H2C3qb3OrLO8p3zjJRX-w48&_nc_ht=scontent.fhan2-1.fna&oh=5b8497d805a8f964099f7e6f308b1fc8&oe=5D775640',  time: '21 giờ trước',
    },
    {
        imageMain: 'https://www.couturetravelcompany.com/wp-content/uploads/2018/10/38536397_753505978314556_1332924576119652352_o-1.jpg',
        namePlace: 'C’est Si Bon',
        coordinates: '21.0251967,105.7966571', address: '317 Nguyễn Khang, Yên Hoà, Cầu Giấy, Hà Nội', describe: 'Trill Group với tổ hợp cafe, bể bơi, phòng tập gym... - là một trong những quán cà phê đẹp ở Hà Nội có view cực chất.Nếu bạn thích trải nghiệm cảm giác vừa nhâm nhi tách trà ấm nóng hay chocolate ngọt ngào, vừa ngắm Hà Nội từ trên cao thì Trill Group là sự lựa chọn không thể lý tưởng hơn.Ngoài yếu tố không gian view và không gian, đồ ăn, đồ uống ở đây cũng khá đầy đủ. Mức giá đồ ở đây trung bình từ 40k/ món.', listImage: 'https://scontent.fhan2-4.fna.fbcdn.net/v/t1.0-9/19894929_1727851937254507_562592798261651451_n.jpg?_nc_cat=105&_nc_oc=AQkq6n1qIgPS-HKiR_XSdx6zl8eV8emmDf8DitWFv_oSI26th9DZaVwqaDh1pVPnmUE&_nc_ht=scontent.fhan2-4.fna&oh=5cb59e1939491c23de9228f67227de8a&oe=5D4163F0;https://scontent.fhan2-2.fna.fbcdn.net/v/t1.0-9/26196123_1712890375417330_6363366011220792421_n.jpg?_nc_cat=106&_nc_oc=AQmiDHoe5HyHWsF8UcPEqOfn5kw6tXs5wogeWtp_SFkJuu8b_YXUu8c9KvGCYdKKwRg&_nc_ht=scontent.fhan2-2.fna&oh=1c0efdc00845b672eaa1c10ad66851fe&oe=5D2A8577;https://scontent.fhan2-1.fna.fbcdn.net/v/t1.0-9/27067159_1727855673920800_1490750529629525949_n.jpg?_nc_cat=102&_nc_oc=AQmngfrEMchuXeZvmqeWH_zIXS3z_a3OuY2ElofC2lf5H2C3qb3OrLO8p3zjJRX-w48&_nc_ht=scontent.fhan2-1.fna&oh=5b8497d805a8f964099f7e6f308b1fc8&oe=5D775640;https://scontent.fhan2-1.fna.fbcdn.net/v/t1.0-9/21430343_787903304722596_5819259620711457202_n.jpg?_nc_cat=102&_nc_oc=AQkFW_yg0DAzXT_g5RAy6eLDDluoIgbG0GPGnCMRFAfytldEUqPvq3zTXsgZsqt9WRY&_nc_ht=scontent.fhan2-1.fna&oh=100a39411f6ca734fe4010d9e0b71a0f&oe=5D6F986E;https://scontent.fhan2-4.fna.fbcdn.net/v/t1.0-9/26731167_857953407717585_6679630345867683902_n.jpg?_nc_cat=104&_nc_oc=AQntZ-6ejfMl6yAUJdUpspCAUbQsP8CtXX3j2P0EE8JMSgejBCXsoWK5tGqs66vtuNg&_nc_ht=scontent.fhan2-4.fna&oh=5f02511c455e095bc5ff5b472e8e6ae7&oe=5D33C2A4;https://scontent.fhan2-1.fna.fbcdn.net/v/t1.0-9/27067159_1727855673920800_1490750529629525949_n.jpg?_nc_cat=102&_nc_oc=AQmngfrEMchuXeZvmqeWH_zIXS3z_a3OuY2ElofC2lf5H2C3qb3OrLO8p3zjJRX-w48&_nc_ht=scontent.fhan2-1.fna&oh=5b8497d805a8f964099f7e6f308b1fc8&oe=5D775640',  time: '21 giờ trước',
    },
    {
        imageMain: 'https://www.couturetravelcompany.com/wp-content/uploads/2018/10/coffeeholiicc1-1493201399280.jpg',
        namePlace: 'Flatform',
        coordinates: '21.0251967,105.7966571', address: '317 Nguyễn Khang, Yên Hoà, Cầu Giấy, Hà Nội', describe: 'Trill Group với tổ hợp cafe, bể bơi, phòng tập gym... - là một trong những quán cà phê đẹp ở Hà Nội có view cực chất.Nếu bạn thích trải nghiệm cảm giác vừa nhâm nhi tách trà ấm nóng hay chocolate ngọt ngào, vừa ngắm Hà Nội từ trên cao thì Trill Group là sự lựa chọn không thể lý tưởng hơn.Ngoài yếu tố không gian view và không gian, đồ ăn, đồ uống ở đây cũng khá đầy đủ. Mức giá đồ ở đây trung bình từ 40k/ món.', listImage: 'https://scontent.fhan2-4.fna.fbcdn.net/v/t1.0-9/19894929_1727851937254507_562592798261651451_n.jpg?_nc_cat=105&_nc_oc=AQkq6n1qIgPS-HKiR_XSdx6zl8eV8emmDf8DitWFv_oSI26th9DZaVwqaDh1pVPnmUE&_nc_ht=scontent.fhan2-4.fna&oh=5cb59e1939491c23de9228f67227de8a&oe=5D4163F0;https://scontent.fhan2-2.fna.fbcdn.net/v/t1.0-9/26196123_1712890375417330_6363366011220792421_n.jpg?_nc_cat=106&_nc_oc=AQmiDHoe5HyHWsF8UcPEqOfn5kw6tXs5wogeWtp_SFkJuu8b_YXUu8c9KvGCYdKKwRg&_nc_ht=scontent.fhan2-2.fna&oh=1c0efdc00845b672eaa1c10ad66851fe&oe=5D2A8577;https://scontent.fhan2-1.fna.fbcdn.net/v/t1.0-9/27067159_1727855673920800_1490750529629525949_n.jpg?_nc_cat=102&_nc_oc=AQmngfrEMchuXeZvmqeWH_zIXS3z_a3OuY2ElofC2lf5H2C3qb3OrLO8p3zjJRX-w48&_nc_ht=scontent.fhan2-1.fna&oh=5b8497d805a8f964099f7e6f308b1fc8&oe=5D775640;https://scontent.fhan2-1.fna.fbcdn.net/v/t1.0-9/21430343_787903304722596_5819259620711457202_n.jpg?_nc_cat=102&_nc_oc=AQkFW_yg0DAzXT_g5RAy6eLDDluoIgbG0GPGnCMRFAfytldEUqPvq3zTXsgZsqt9WRY&_nc_ht=scontent.fhan2-1.fna&oh=100a39411f6ca734fe4010d9e0b71a0f&oe=5D6F986E;https://scontent.fhan2-4.fna.fbcdn.net/v/t1.0-9/26731167_857953407717585_6679630345867683902_n.jpg?_nc_cat=104&_nc_oc=AQntZ-6ejfMl6yAUJdUpspCAUbQsP8CtXX3j2P0EE8JMSgejBCXsoWK5tGqs66vtuNg&_nc_ht=scontent.fhan2-4.fna&oh=5f02511c455e095bc5ff5b472e8e6ae7&oe=5D33C2A4;https://scontent.fhan2-1.fna.fbcdn.net/v/t1.0-9/27067159_1727855673920800_1490750529629525949_n.jpg?_nc_cat=102&_nc_oc=AQmngfrEMchuXeZvmqeWH_zIXS3z_a3OuY2ElofC2lf5H2C3qb3OrLO8p3zjJRX-w48&_nc_ht=scontent.fhan2-1.fna&oh=5b8497d805a8f964099f7e6f308b1fc8&oe=5D775640',  time: '21 giờ trước',
    },
];
export const LIST_CA_PHE_MAPS = [
    {
        namePlace: 'Trill Rooftop Cafe',
        address: 'Tầng 26, Tòa nhà Hei Tower, 1 Ngụy Như Kon Tum, Nhân Chính, Thanh Xuân, Hà Nội',
        coordinates: '21.0032581,105.8051799',
        describe: ' Trill Group với tổ hợp cafe, bể bơi, phòng tập gym... - là một trong những quán cà phê đẹp ở Hà Nội có view cực chất.Nếu bạn thích trải nghiệm cảm giác vừa nhâm nhi tách trà ấm nóng hay chocolate ngọt ngào, vừa ngắm Hà Nội từ trên cao thì Trill Group là sự lựa chọn không thể lý tưởng hơn.Ngoài yếu tố không gian view và không gian, đồ ăn, đồ uống ở đây cũng khá đầy đủ. Mức giá đồ ở đây trung bình từ 40k/ món.', 
        listImage: 'https://images.foody.vn/images/14369204_1513957175297868_7285838755494100992_n.jpg;https://images.foody.vn/images/trillgroup.jpg;https://images.foody.vn/images/14449222_1037816323006133_4293765315042476032_n.jpg;https://images.foody.vn/images/14134940_1584382345199200_778613661_n.jpg;https://images.foody.vn/images/13774392_971524286308546_353305157_n.jpg', 
        imageMain: 'https://images.foody.vn/images/14369204_1513957175297868_7285838755494100992_n.jpg',
        phoneNumber:'',
        type:2,
        time: '21 giờ trước',
    },
    {
        namePlace: 'Cup of Tea Cafe & Bistro CS1',
        address: '317 Nguyễn Khang – Cầu Giấy',
        coordinates: '21.0344745,105.8006222',
        describe: 'Đúng như tên gọi, đồ uống chính tại COT chính là trà và cafe, tuy nhiên nếu muốn có một bức hình sống ảo siêu chất thì bạn hãy gọi một ấm trà nóng nhé, vừa được chụp ảnh lại vừa được nhâm nhi tách trà nóng hổi thơm ngon, tội gì không thử phải không nào?Nếu như bạn còn đang đau đầu tìm cho mình các quán cafe đẹp ở Hà Nội để chụp ảnh thì chắc chắn đừng bỏ qua COT nhé. Tại đây có đến 3 tầng với các góc khác nhau, nhưng đảm bảo rằng góc nào lên hình cũng chỉ có đẹp trở lên mà thôi.', 
        listImage: 'https://www.couturetravelcompany.com/wp-content/uploads/2018/10/sontungg_ph1.jpg;https://www.couturetravelcompany.com/wp-content/uploads/2018/10/cup-of-tea-cafe-bistro-JZht3Q1uMuUTXB7e-1399632-1486879994.jpg;https://www.couturetravelcompany.com/wp-content/uploads/2018/10/hienpham_.jpg;https://www.couturetravelcompany.com/wp-content/uploads/2018/10/coffeeholiicc1.jpg;https://www.google.com/url?sa=i&source=images&cd=&cad=rja&uact=8&ved=2ahUKEwixmuGrldbhAhUKH3AKHVq0DCUQjRx6BAgBEAU&url=https%3A%2F%2Fwww.foody.vn%2Fha-noi%2Fcup-of-tea-cafe-bistro-nguyen-khang%2Fbinh-luan-2036848&psig=AOvVaw0yQm1mFdflvjrUibO_WvPF&ust=1555557473380534', 
        imageMain: 'https://www.couturetravelcompany.com/wp-content/uploads/2018/10/sontungg_ph1.jpg',
        phoneNumber:'',
        type:2,
        time: '21 giờ trước',
    }
]
export const LIST_RAP_CP_MAPS = [
    {
        namePlace: 'Cafe phim Hà Đông',
        address: 'Số 30 An Hòa, P. Mộ Lao, Hà Đông, Hà Nội',
        coordinates: '20.9838174,105.7877678',
        describe: 'Đây là địa điểm cafe phim 3D ở Hà Đông được nhiều cặp đôi lựa chọn. Trong không gian yên tĩnh và riêng tư của phòng phim riêng, bạn có thể thoải mái chuyện trò, bình luận sôi nổi về bộ phim đang chiếu mà không e ngại sẽ làm phiền tới bất cứ ai.', 
        listImage: 'https://dulichtoday.vn/wp-content/uploads/2018/09/Cafe-Phim-Ha-Dong.jpg;https://dulichtoday.vn/wp-content/uploads/2018/09/Cafe-Phim-Ha-Dong1-1.jpg;https://dulichtoday.vn/wp-content/uploads/2018/09/Cafe-Phim-3D-Kinomax.jpg', 
        imageMain: 'https://dulichtoday.vn/wp-content/uploads/2018/09/Cafe-Phim-Ha-Dong.jpg',
        phoneNumber:'',
        type:5,
        time: '21 giờ trước',
    }
]
export const LIST_HOMESTAY_MAPS = [
    {
        namePlace: 'Harmony Homestay',
        address: '36 Ngõ Phát Lộc, Quận Hoàn Kiếm, Hà Nội',
        coordinates: '21.0343805,105.8518779',
        describe: 'Dân du lịch chuyên nghiệp lỡ bị mê Hà Nội thường sẽ biết đến Harmony Homestay. Vì sao? Vị trí của homestay tại Hà Nội này thông ra 3 con phố sầm uất, là nơi tụ tập cuối tuần của giới trẻ Hà Thành gồm Lương Ngọc Quyến, Nguyễn Hữu Huân và Hàng Mắm, cách Hồ Gươm vài phút đi bộ. Khi thành phố lên đèn, các con phố này lại nhộn nhịp khó tả bởi hàng quán, quán vỉa hè cho đến các quán bar cực chất, cực tự do.', 
        listImage: 'https://blog.traveloka.com/source/uploads/sites/9/2018/11/homestay-tai-ha-noi-3-1.jpg;https://blog.traveloka.com/source/uploads/sites/9/2018/11/homestay-tai-ha-noi-3-2.jpg;https://blog.traveloka.com/source/uploads/sites/9/2018/11/homestay-tai-ha-noi-3-3.jpg;https://blog.traveloka.com/source/uploads/sites/9/2018/11/homestay-tai-ha-noi-3-4.jpg;https://blog.traveloka.com/source/uploads/sites/9/2018/11/homestay-tai-ha-noi-3-6.jpg', 
        imageMain: 'https://blog.traveloka.com/source/uploads/sites/9/2018/11/homestay-tai-ha-noi-3-1.jpg',
        phoneNumber:'',
        type:4,
        time: '21 giờ trước',
    }
]
export const LIST_TRA_SUA = [
    {
        imageMain: 'https://scontent.fhan2-4.fna.fbcdn.net/v/t1.0-9/19894929_1727851937254507_562592798261651451_n.jpg?_nc_cat=105&_nc_oc=AQkq6n1qIgPS-HKiR_XSdx6zl8eV8emmDf8DitWFv_oSI26th9DZaVwqaDh1pVPnmUE&_nc_ht=scontent.fhan2-4.fna&oh=5cb59e1939491c23de9228f67227de8a&oe=5D4163F0',
        namePlace: 'House of cha',
        phoneNumber:'024 6668 9669',
        coordinates: '20.9911435,105.795996',
        address: '182 Lương Thế Vinh', 
        type:3,
        describe: 'House of Cha - Ngôi nhà trà sữa hàng đầu Việt Nam. Nơi thư giãn, giải trí của mọi lứa tuổi và là nơi có những không gian check-in tuyệt vời nhất.', 
        listImage: 'https://scontent.fhan2-4.fna.fbcdn.net/v/t1.0-9/19894929_1727851937254507_562592798261651451_n.jpg?_nc_cat=105&_nc_oc=AQkq6n1qIgPS-HKiR_XSdx6zl8eV8emmDf8DitWFv_oSI26th9DZaVwqaDh1pVPnmUE&_nc_ht=scontent.fhan2-4.fna&oh=5cb59e1939491c23de9228f67227de8a&oe=5D4163F0;https://scontent.fhan2-2.fna.fbcdn.net/v/t1.0-9/26196123_1712890375417330_6363366011220792421_n.jpg?_nc_cat=106&_nc_oc=AQmiDHoe5HyHWsF8UcPEqOfn5kw6tXs5wogeWtp_SFkJuu8b_YXUu8c9KvGCYdKKwRg&_nc_ht=scontent.fhan2-2.fna&oh=1c0efdc00845b672eaa1c10ad66851fe&oe=5D2A8577;https://scontent.fhan2-1.fna.fbcdn.net/v/t1.0-9/27067159_1727855673920800_1490750529629525949_n.jpg?_nc_cat=102&_nc_oc=AQmngfrEMchuXeZvmqeWH_zIXS3z_a3OuY2ElofC2lf5H2C3qb3OrLO8p3zjJRX-w48&_nc_ht=scontent.fhan2-1.fna&oh=5b8497d805a8f964099f7e6f308b1fc8&oe=5D775640;https://scontent.fhan2-1.fna.fbcdn.net/v/t1.0-9/21430343_787903304722596_5819259620711457202_n.jpg?_nc_cat=102&_nc_oc=AQkFW_yg0DAzXT_g5RAy6eLDDluoIgbG0GPGnCMRFAfytldEUqPvq3zTXsgZsqt9WRY&_nc_ht=scontent.fhan2-1.fna&oh=100a39411f6ca734fe4010d9e0b71a0f&oe=5D6F986E;https://scontent.fhan2-4.fna.fbcdn.net/v/t1.0-9/26731167_857953407717585_6679630345867683902_n.jpg?_nc_cat=104&_nc_oc=AQntZ-6ejfMl6yAUJdUpspCAUbQsP8CtXX3j2P0EE8JMSgejBCXsoWK5tGqs66vtuNg&_nc_ht=scontent.fhan2-4.fna&oh=5f02511c455e095bc5ff5b472e8e6ae7&oe=5D33C2A4;https://scontent.fhan2-1.fna.fbcdn.net/v/t1.0-9/27067159_1727855673920800_1490750529629525949_n.jpg?_nc_cat=102&_nc_oc=AQmngfrEMchuXeZvmqeWH_zIXS3z_a3OuY2ElofC2lf5H2C3qb3OrLO8p3zjJRX-w48&_nc_ht=scontent.fhan2-1.fna&oh=5b8497d805a8f964099f7e6f308b1fc8&oe=5D775640', 
        time: '21 giờ trước',
    },
    {
        namePlace: 'Dingtea',
        address: '23 Phố Lê Đại Hành, Lê Đại Hành, Hai Bà Trưng, Hà Nội',
        coordinates: '21.0087887,105.8487055',
        describe: 'Ding tea Hà Nội là Fanpage chính thức chủ quản chuỗi cửa hàng Ding Tea tại khu vực Hà Nội, mọi thông tin chi tiết, thắc mắc về sản phẩm – dịch vụ sẽ được Update và giải đáp trực tiếp qua Fanpage & Tổng đài 1900 2003. Ding Tea ( Đỉnh Trà ) - 100% Authentic Taiwanese Bubble Tea Thương hiệu Trà sữa trân châu Đài Loan uy tín trên thị trường các nước Singapore, Malaysia, China, Indonesia, Phillipines, Japan... đã có mặt tại Việt Nam! Nguyên liệu sử dụng để pha chế tại toàn bộ các cơ sở Ding Tea Hà Nội đều được sản xuất với tiêu chuẩn riêng của Ding Tea, được kiểm tra và đóng gói dưới tên thương hiệu Ding Tea để đảm bảo ko sử dụng hàng trôi nổi hoặc nguyên liệu ko phải của Ding Tea. An toàn và nhập khẩu 100% từ Đài Loan.', 
        listImage: 'https://scontent.fhan2-2.fna.fbcdn.net/v/t1.0-9/51335597_2328643850488877_1753798574757380096_n.jpg?_nc_cat=111&_nc_oc=AQk5UXiC6Q2Fo5MCuhbGngEpZ-JS1ScQQ4_ZB8Y3Ha5_jBARIdkMXkMXyCZvyPrxZt4&_nc_ht=scontent.fhan2-2.fna&oh=81ce3c2172f20325ca28f7ece7a8ed15&oe=5D31F84B;https://scontent.fhan2-1.fna.fbcdn.net/v/t1.0-9/50567773_2308801679139761_1048719971598729216_n.jpg?_nc_cat=102&_nc_oc=AQkp6ihVL5duMb5zXgTHzjCOtbMMvt8pbjt1ScXxJYTbcnBGOU3HeTvRTPg0Nghhozw&_nc_ht=scontent.fhan2-1.fna&oh=225fc26f8f924d72390328faf735ba31&oe=5D3C714C;https://scontent.fhan2-1.fna.fbcdn.net/v/t1.0-9/50567773_2308801679139761_1048719971598729216_n.jpg?_nc_cat=102&_nc_oc=AQkp6ihVL5duMb5zXgTHzjCOtbMMvt8pbjt1ScXxJYTbcnBGOU3HeTvRTPg0Nghhozw&_nc_ht=scontent.fhan2-1.fna&oh=225fc26f8f924d72390328faf735ba31&oe=5D3C714C;https://scontent.fhan2-2.fna.fbcdn.net/v/t1.0-9/50416149_2305746799445249_1339172855887167488_n.jpg?_nc_cat=106&_nc_oc=AQnSfSgmAf5wfUEmYPgEt8OkrxUU5H-LlM698zefdFu93mTekUL7bFklvSR1_5wbK0g&_nc_ht=scontent.fhan2-2.fna&oh=98305d356197cbef79ebd591a4072661&oe=5D36524A;https://scontent.fhan2-4.fna.fbcdn.net/v/t1.0-9/27972250_1834816813204919_3645555182875474323_n.jpg?_nc_cat=100&_nc_oc=AQkRPgYMaEG2JC34fJiXn_-ujkQSjCyfA7Wg0nR9F-D11Jrh-fyYPOTyvRZIplus_R4&_nc_ht=scontent.fhan2-4.fna&oh=08e5d479e714dd5fd9867587e52a5839&oe=5D2D92DA', 
        imageMain: 'https://scontent.fhan2-2.fna.fbcdn.net/v/t1.0-9/51335597_2328643850488877_1753798574757380096_n.jpg?_nc_cat=111&_nc_oc=AQk5UXiC6Q2Fo5MCuhbGngEpZ-JS1ScQQ4_ZB8Y3Ha5_jBARIdkMXkMXyCZvyPrxZt4&_nc_ht=scontent.fhan2-2.fna&oh=81ce3c2172f20325ca28f7ece7a8ed15&oe=5D31F84B',
        phoneNumber:'1900 2003',
        type:3,
        time: '21 giờ trước',
    },
    {
        namePlace: 'Heekcaa',
        address: '42C Lý Thường Kiệt, Tràng Tiền, Hoàn Kiếm, Hà Nội',
        coordinates: '21.0166641,105.8410868',
        describe: 'Không gian được decor theo cách đơn giản, nhã nhặn nhưng ko kém phần đáng yêu. Giá dao động từ 30000đ - 70000đ', 
        listImage: 'https://scontent.fhan2-3.fna.fbcdn.net/v/t1.0-9/54432415_1637097529725956_7845135852115066880_n.jpg?_nc_cat=108&_nc_oc=AQl8eIlpa9_XV_NUwYd6MgnxSeJSHRV2SVSvmyEV9BtYSyGJ5LMgI5mCDFiqr8BpQtQ&_nc_ht=scontent.fhan2-3.fna&oh=cc9114089f8b31290f64525d625e9df6&oe=5D4440F2;https://scontent.fhan2-4.fna.fbcdn.net/v/t1.0-9/51308548_1583870301715346_7454797017957531648_n.jpg?_nc_cat=104&_nc_oc=AQlbGq3ccomlnKnQ8kd-XmTnbKiup-K9kwR4w-KmfjnLl9PxF3FjSwxTouXhIv9UAJw&_nc_ht=scontent.fhan2-4.fna&oh=4ce1eab0555b49978d3afc3c537ace27&oe=5D312430;https://scontent.fhan2-3.fna.fbcdn.net/v/t1.0-9/47220476_1502492386519805_1194961937285251072_n.jpg?_nc_cat=107&_nc_oc=AQnm4cLR2zE-kzRsIZ-n1ppxVpJadO1oo44iDI0EBocz0mY9K2BWi54YhYkJvn8pN6I&_nc_ht=scontent.fhan2-3.fna&oh=6b6a4a70597a585070a4244e77150414&oe=5D2F3CE0;https://scontent.fhan2-3.fna.fbcdn.net/v/t1.0-9/46497910_1487089754726735_1741055883531517952_n.jpg?_nc_cat=109&_nc_oc=AQmfZmK8mtBpmPjbUykyj2pvCnlIYnDYFmQkuy_Y96Zmk2f0xCFBd0wb1giro1MmNXk&_nc_ht=scontent.fhan2-3.fna&oh=e42f14bccf6ce093545ab16d185d00e7&oe=5D2C9A4E', 
        imageMain: 'https://scontent.fhan2-3.fna.fbcdn.net/v/t1.0-9/54432415_1637097529725956_7845135852115066880_n.jpg?_nc_cat=108&_nc_oc=AQl8eIlpa9_XV_NUwYd6MgnxSeJSHRV2SVSvmyEV9BtYSyGJ5LMgI5mCDFiqr8BpQtQ&_nc_ht=scontent.fhan2-3.fna&oh=cc9114089f8b31290f64525d625e9df6&oe=5D4440F2',
        phoneNumber:'0122 320 2020',
        type:3,
        time: '21 giờ trước',
    },
]
export const LIST_RAP_CP = [
    {
        imageMain: 'https://dulichtoday.vn/wp-content/uploads/2018/09/Cafe-Phim-Ha-Dong.jpg',
        namePlace: 'Cafe phim Hà Đông',
        coordinates: '21.0251967,105.7966571', address: '317 Nguyễn Khang, Yên Hoà, Cầu Giấy, Hà Nội', describe: 'Trill Group với tổ hợp cafe, bể bơi, phòng tập gym... - là một trong những quán cà phê đẹp ở Hà Nội có view cực chất.Nếu bạn thích trải nghiệm cảm giác vừa nhâm nhi tách trà ấm nóng hay chocolate ngọt ngào, vừa ngắm Hà Nội từ trên cao thì Trill Group là sự lựa chọn không thể lý tưởng hơn.Ngoài yếu tố không gian view và không gian, đồ ăn, đồ uống ở đây cũng khá đầy đủ. Mức giá đồ ở đây trung bình từ 40k/ món.', listImage: 'https://scontent.fhan2-4.fna.fbcdn.net/v/t1.0-9/19894929_1727851937254507_562592798261651451_n.jpg?_nc_cat=105&_nc_oc=AQkq6n1qIgPS-HKiR_XSdx6zl8eV8emmDf8DitWFv_oSI26th9DZaVwqaDh1pVPnmUE&_nc_ht=scontent.fhan2-4.fna&oh=5cb59e1939491c23de9228f67227de8a&oe=5D4163F0;https://scontent.fhan2-2.fna.fbcdn.net/v/t1.0-9/26196123_1712890375417330_6363366011220792421_n.jpg?_nc_cat=106&_nc_oc=AQmiDHoe5HyHWsF8UcPEqOfn5kw6tXs5wogeWtp_SFkJuu8b_YXUu8c9KvGCYdKKwRg&_nc_ht=scontent.fhan2-2.fna&oh=1c0efdc00845b672eaa1c10ad66851fe&oe=5D2A8577;https://scontent.fhan2-1.fna.fbcdn.net/v/t1.0-9/27067159_1727855673920800_1490750529629525949_n.jpg?_nc_cat=102&_nc_oc=AQmngfrEMchuXeZvmqeWH_zIXS3z_a3OuY2ElofC2lf5H2C3qb3OrLO8p3zjJRX-w48&_nc_ht=scontent.fhan2-1.fna&oh=5b8497d805a8f964099f7e6f308b1fc8&oe=5D775640;https://scontent.fhan2-1.fna.fbcdn.net/v/t1.0-9/21430343_787903304722596_5819259620711457202_n.jpg?_nc_cat=102&_nc_oc=AQkFW_yg0DAzXT_g5RAy6eLDDluoIgbG0GPGnCMRFAfytldEUqPvq3zTXsgZsqt9WRY&_nc_ht=scontent.fhan2-1.fna&oh=100a39411f6ca734fe4010d9e0b71a0f&oe=5D6F986E;https://scontent.fhan2-4.fna.fbcdn.net/v/t1.0-9/26731167_857953407717585_6679630345867683902_n.jpg?_nc_cat=104&_nc_oc=AQntZ-6ejfMl6yAUJdUpspCAUbQsP8CtXX3j2P0EE8JMSgejBCXsoWK5tGqs66vtuNg&_nc_ht=scontent.fhan2-4.fna&oh=5f02511c455e095bc5ff5b472e8e6ae7&oe=5D33C2A4;https://scontent.fhan2-1.fna.fbcdn.net/v/t1.0-9/27067159_1727855673920800_1490750529629525949_n.jpg?_nc_cat=102&_nc_oc=AQmngfrEMchuXeZvmqeWH_zIXS3z_a3OuY2ElofC2lf5H2C3qb3OrLO8p3zjJRX-w48&_nc_ht=scontent.fhan2-1.fna&oh=5b8497d805a8f964099f7e6f308b1fc8&oe=5D775640',  time: '21 giờ trước',
    },
    {
        imageMain: 'https://dulichtoday.vn/wp-content/uploads/2018/09/Cafe-Phim-T-Box.jpg',
        namePlace: 'T-Box Cafe Phim',
        coordinates: '21.0251967,105.7966571', address: '317 Nguyễn Khang, Yên Hoà, Cầu Giấy, Hà Nội', describe: 'Trill Group với tổ hợp cafe, bể bơi, phòng tập gym... - là một trong những quán cà phê đẹp ở Hà Nội có view cực chất.Nếu bạn thích trải nghiệm cảm giác vừa nhâm nhi tách trà ấm nóng hay chocolate ngọt ngào, vừa ngắm Hà Nội từ trên cao thì Trill Group là sự lựa chọn không thể lý tưởng hơn.Ngoài yếu tố không gian view và không gian, đồ ăn, đồ uống ở đây cũng khá đầy đủ. Mức giá đồ ở đây trung bình từ 40k/ món.', listImage: 'https://scontent.fhan2-4.fna.fbcdn.net/v/t1.0-9/19894929_1727851937254507_562592798261651451_n.jpg?_nc_cat=105&_nc_oc=AQkq6n1qIgPS-HKiR_XSdx6zl8eV8emmDf8DitWFv_oSI26th9DZaVwqaDh1pVPnmUE&_nc_ht=scontent.fhan2-4.fna&oh=5cb59e1939491c23de9228f67227de8a&oe=5D4163F0;https://scontent.fhan2-2.fna.fbcdn.net/v/t1.0-9/26196123_1712890375417330_6363366011220792421_n.jpg?_nc_cat=106&_nc_oc=AQmiDHoe5HyHWsF8UcPEqOfn5kw6tXs5wogeWtp_SFkJuu8b_YXUu8c9KvGCYdKKwRg&_nc_ht=scontent.fhan2-2.fna&oh=1c0efdc00845b672eaa1c10ad66851fe&oe=5D2A8577;https://scontent.fhan2-1.fna.fbcdn.net/v/t1.0-9/27067159_1727855673920800_1490750529629525949_n.jpg?_nc_cat=102&_nc_oc=AQmngfrEMchuXeZvmqeWH_zIXS3z_a3OuY2ElofC2lf5H2C3qb3OrLO8p3zjJRX-w48&_nc_ht=scontent.fhan2-1.fna&oh=5b8497d805a8f964099f7e6f308b1fc8&oe=5D775640;https://scontent.fhan2-1.fna.fbcdn.net/v/t1.0-9/21430343_787903304722596_5819259620711457202_n.jpg?_nc_cat=102&_nc_oc=AQkFW_yg0DAzXT_g5RAy6eLDDluoIgbG0GPGnCMRFAfytldEUqPvq3zTXsgZsqt9WRY&_nc_ht=scontent.fhan2-1.fna&oh=100a39411f6ca734fe4010d9e0b71a0f&oe=5D6F986E;https://scontent.fhan2-4.fna.fbcdn.net/v/t1.0-9/26731167_857953407717585_6679630345867683902_n.jpg?_nc_cat=104&_nc_oc=AQntZ-6ejfMl6yAUJdUpspCAUbQsP8CtXX3j2P0EE8JMSgejBCXsoWK5tGqs66vtuNg&_nc_ht=scontent.fhan2-4.fna&oh=5f02511c455e095bc5ff5b472e8e6ae7&oe=5D33C2A4;https://scontent.fhan2-1.fna.fbcdn.net/v/t1.0-9/27067159_1727855673920800_1490750529629525949_n.jpg?_nc_cat=102&_nc_oc=AQmngfrEMchuXeZvmqeWH_zIXS3z_a3OuY2ElofC2lf5H2C3qb3OrLO8p3zjJRX-w48&_nc_ht=scontent.fhan2-1.fna&oh=5b8497d805a8f964099f7e6f308b1fc8&oe=5D775640',  time: '21 giờ trước',
    },
    {
        imageMain: 'https://dulichtoday.vn/wp-content/uploads/2018/09/Cafe-Phim-3D-Kinomax.jpg',
        namePlace: 'Kinomax Cafe Phim 3D/HD',
        coordinates: '21.0251967,105.7966571', address: '317 Nguyễn Khang, Yên Hoà, Cầu Giấy, Hà Nội', describe: 'Trill Group với tổ hợp cafe, bể bơi, phòng tập gym... - là một trong những quán cà phê đẹp ở Hà Nội có view cực chất.Nếu bạn thích trải nghiệm cảm giác vừa nhâm nhi tách trà ấm nóng hay chocolate ngọt ngào, vừa ngắm Hà Nội từ trên cao thì Trill Group là sự lựa chọn không thể lý tưởng hơn.Ngoài yếu tố không gian view và không gian, đồ ăn, đồ uống ở đây cũng khá đầy đủ. Mức giá đồ ở đây trung bình từ 40k/ món.', listImage: 'https://scontent.fhan2-4.fna.fbcdn.net/v/t1.0-9/19894929_1727851937254507_562592798261651451_n.jpg?_nc_cat=105&_nc_oc=AQkq6n1qIgPS-HKiR_XSdx6zl8eV8emmDf8DitWFv_oSI26th9DZaVwqaDh1pVPnmUE&_nc_ht=scontent.fhan2-4.fna&oh=5cb59e1939491c23de9228f67227de8a&oe=5D4163F0;https://scontent.fhan2-2.fna.fbcdn.net/v/t1.0-9/26196123_1712890375417330_6363366011220792421_n.jpg?_nc_cat=106&_nc_oc=AQmiDHoe5HyHWsF8UcPEqOfn5kw6tXs5wogeWtp_SFkJuu8b_YXUu8c9KvGCYdKKwRg&_nc_ht=scontent.fhan2-2.fna&oh=1c0efdc00845b672eaa1c10ad66851fe&oe=5D2A8577;https://scontent.fhan2-1.fna.fbcdn.net/v/t1.0-9/27067159_1727855673920800_1490750529629525949_n.jpg?_nc_cat=102&_nc_oc=AQmngfrEMchuXeZvmqeWH_zIXS3z_a3OuY2ElofC2lf5H2C3qb3OrLO8p3zjJRX-w48&_nc_ht=scontent.fhan2-1.fna&oh=5b8497d805a8f964099f7e6f308b1fc8&oe=5D775640;https://scontent.fhan2-1.fna.fbcdn.net/v/t1.0-9/21430343_787903304722596_5819259620711457202_n.jpg?_nc_cat=102&_nc_oc=AQkFW_yg0DAzXT_g5RAy6eLDDluoIgbG0GPGnCMRFAfytldEUqPvq3zTXsgZsqt9WRY&_nc_ht=scontent.fhan2-1.fna&oh=100a39411f6ca734fe4010d9e0b71a0f&oe=5D6F986E;https://scontent.fhan2-4.fna.fbcdn.net/v/t1.0-9/26731167_857953407717585_6679630345867683902_n.jpg?_nc_cat=104&_nc_oc=AQntZ-6ejfMl6yAUJdUpspCAUbQsP8CtXX3j2P0EE8JMSgejBCXsoWK5tGqs66vtuNg&_nc_ht=scontent.fhan2-4.fna&oh=5f02511c455e095bc5ff5b472e8e6ae7&oe=5D33C2A4;https://scontent.fhan2-1.fna.fbcdn.net/v/t1.0-9/27067159_1727855673920800_1490750529629525949_n.jpg?_nc_cat=102&_nc_oc=AQmngfrEMchuXeZvmqeWH_zIXS3z_a3OuY2ElofC2lf5H2C3qb3OrLO8p3zjJRX-w48&_nc_ht=scontent.fhan2-1.fna&oh=5b8497d805a8f964099f7e6f308b1fc8&oe=5D775640',  time: '21 giờ trước',
    },
    {
        imageMain: 'https://dulichtoday.vn/wp-content/uploads/2018/09/Cafe-Phim-3D-Paradise-Cafe-Phim.jpg',
        namePlace: 'Paradise Coffee & Film',
        coordinates: '21.0251967,105.7966571', address: '317 Nguyễn Khang, Yên Hoà, Cầu Giấy, Hà Nội', describe: 'Trill Group với tổ hợp cafe, bể bơi, phòng tập gym... - là một trong những quán cà phê đẹp ở Hà Nội có view cực chất.Nếu bạn thích trải nghiệm cảm giác vừa nhâm nhi tách trà ấm nóng hay chocolate ngọt ngào, vừa ngắm Hà Nội từ trên cao thì Trill Group là sự lựa chọn không thể lý tưởng hơn.Ngoài yếu tố không gian view và không gian, đồ ăn, đồ uống ở đây cũng khá đầy đủ. Mức giá đồ ở đây trung bình từ 40k/ món.', listImage: 'https://scontent.fhan2-4.fna.fbcdn.net/v/t1.0-9/19894929_1727851937254507_562592798261651451_n.jpg?_nc_cat=105&_nc_oc=AQkq6n1qIgPS-HKiR_XSdx6zl8eV8emmDf8DitWFv_oSI26th9DZaVwqaDh1pVPnmUE&_nc_ht=scontent.fhan2-4.fna&oh=5cb59e1939491c23de9228f67227de8a&oe=5D4163F0;https://scontent.fhan2-2.fna.fbcdn.net/v/t1.0-9/26196123_1712890375417330_6363366011220792421_n.jpg?_nc_cat=106&_nc_oc=AQmiDHoe5HyHWsF8UcPEqOfn5kw6tXs5wogeWtp_SFkJuu8b_YXUu8c9KvGCYdKKwRg&_nc_ht=scontent.fhan2-2.fna&oh=1c0efdc00845b672eaa1c10ad66851fe&oe=5D2A8577;https://scontent.fhan2-1.fna.fbcdn.net/v/t1.0-9/27067159_1727855673920800_1490750529629525949_n.jpg?_nc_cat=102&_nc_oc=AQmngfrEMchuXeZvmqeWH_zIXS3z_a3OuY2ElofC2lf5H2C3qb3OrLO8p3zjJRX-w48&_nc_ht=scontent.fhan2-1.fna&oh=5b8497d805a8f964099f7e6f308b1fc8&oe=5D775640;https://scontent.fhan2-1.fna.fbcdn.net/v/t1.0-9/21430343_787903304722596_5819259620711457202_n.jpg?_nc_cat=102&_nc_oc=AQkFW_yg0DAzXT_g5RAy6eLDDluoIgbG0GPGnCMRFAfytldEUqPvq3zTXsgZsqt9WRY&_nc_ht=scontent.fhan2-1.fna&oh=100a39411f6ca734fe4010d9e0b71a0f&oe=5D6F986E;https://scontent.fhan2-4.fna.fbcdn.net/v/t1.0-9/26731167_857953407717585_6679630345867683902_n.jpg?_nc_cat=104&_nc_oc=AQntZ-6ejfMl6yAUJdUpspCAUbQsP8CtXX3j2P0EE8JMSgejBCXsoWK5tGqs66vtuNg&_nc_ht=scontent.fhan2-4.fna&oh=5f02511c455e095bc5ff5b472e8e6ae7&oe=5D33C2A4;https://scontent.fhan2-1.fna.fbcdn.net/v/t1.0-9/27067159_1727855673920800_1490750529629525949_n.jpg?_nc_cat=102&_nc_oc=AQmngfrEMchuXeZvmqeWH_zIXS3z_a3OuY2ElofC2lf5H2C3qb3OrLO8p3zjJRX-w48&_nc_ht=scontent.fhan2-1.fna&oh=5b8497d805a8f964099f7e6f308b1fc8&oe=5D775640',  time: '21 giờ trước',
    },
    {
        imageMain: 'https://dulichtoday.vn/wp-content/uploads/2018/09/Cafe-Phim-3D-Ho-tung-mau-2.jpg',
        namePlace: 'Cafe phim 3D Hồ Tùng Mậu',
        coordinates: '21.0251967,105.7966571', address: '317 Nguyễn Khang, Yên Hoà, Cầu Giấy, Hà Nội', describe: 'Trill Group với tổ hợp cafe, bể bơi, phòng tập gym... - là một trong những quán cà phê đẹp ở Hà Nội có view cực chất.Nếu bạn thích trải nghiệm cảm giác vừa nhâm nhi tách trà ấm nóng hay chocolate ngọt ngào, vừa ngắm Hà Nội từ trên cao thì Trill Group là sự lựa chọn không thể lý tưởng hơn.Ngoài yếu tố không gian view và không gian, đồ ăn, đồ uống ở đây cũng khá đầy đủ. Mức giá đồ ở đây trung bình từ 40k/ món.', listImage: 'https://scontent.fhan2-4.fna.fbcdn.net/v/t1.0-9/19894929_1727851937254507_562592798261651451_n.jpg?_nc_cat=105&_nc_oc=AQkq6n1qIgPS-HKiR_XSdx6zl8eV8emmDf8DitWFv_oSI26th9DZaVwqaDh1pVPnmUE&_nc_ht=scontent.fhan2-4.fna&oh=5cb59e1939491c23de9228f67227de8a&oe=5D4163F0;https://scontent.fhan2-2.fna.fbcdn.net/v/t1.0-9/26196123_1712890375417330_6363366011220792421_n.jpg?_nc_cat=106&_nc_oc=AQmiDHoe5HyHWsF8UcPEqOfn5kw6tXs5wogeWtp_SFkJuu8b_YXUu8c9KvGCYdKKwRg&_nc_ht=scontent.fhan2-2.fna&oh=1c0efdc00845b672eaa1c10ad66851fe&oe=5D2A8577;https://scontent.fhan2-1.fna.fbcdn.net/v/t1.0-9/27067159_1727855673920800_1490750529629525949_n.jpg?_nc_cat=102&_nc_oc=AQmngfrEMchuXeZvmqeWH_zIXS3z_a3OuY2ElofC2lf5H2C3qb3OrLO8p3zjJRX-w48&_nc_ht=scontent.fhan2-1.fna&oh=5b8497d805a8f964099f7e6f308b1fc8&oe=5D775640;https://scontent.fhan2-1.fna.fbcdn.net/v/t1.0-9/21430343_787903304722596_5819259620711457202_n.jpg?_nc_cat=102&_nc_oc=AQkFW_yg0DAzXT_g5RAy6eLDDluoIgbG0GPGnCMRFAfytldEUqPvq3zTXsgZsqt9WRY&_nc_ht=scontent.fhan2-1.fna&oh=100a39411f6ca734fe4010d9e0b71a0f&oe=5D6F986E;https://scontent.fhan2-4.fna.fbcdn.net/v/t1.0-9/26731167_857953407717585_6679630345867683902_n.jpg?_nc_cat=104&_nc_oc=AQntZ-6ejfMl6yAUJdUpspCAUbQsP8CtXX3j2P0EE8JMSgejBCXsoWK5tGqs66vtuNg&_nc_ht=scontent.fhan2-4.fna&oh=5f02511c455e095bc5ff5b472e8e6ae7&oe=5D33C2A4;https://scontent.fhan2-1.fna.fbcdn.net/v/t1.0-9/27067159_1727855673920800_1490750529629525949_n.jpg?_nc_cat=102&_nc_oc=AQmngfrEMchuXeZvmqeWH_zIXS3z_a3OuY2ElofC2lf5H2C3qb3OrLO8p3zjJRX-w48&_nc_ht=scontent.fhan2-1.fna&oh=5b8497d805a8f964099f7e6f308b1fc8&oe=5D775640',  time: '21 giờ trước',
    },
]
export const LIST_HOME_STAY = [

    {
        imageMain: 'https://dulichtoday.vn/wp-content/uploads/2018/04/Mei-Hideaway.jpg',
        namePlace: 'Mei Hideaway',
        coordinates: '21.0251967,105.7966571', address: '317 Nguyễn Khang, Yên Hoà, Cầu Giấy, Hà Nội', describe: 'Trill Group với tổ hợp cafe, bể bơi, phòng tập gym... - là một trong những quán cà phê đẹp ở Hà Nội có view cực chất.Nếu bạn thích trải nghiệm cảm giác vừa nhâm nhi tách trà ấm nóng hay chocolate ngọt ngào, vừa ngắm Hà Nội từ trên cao thì Trill Group là sự lựa chọn không thể lý tưởng hơn.Ngoài yếu tố không gian view và không gian, đồ ăn, đồ uống ở đây cũng khá đầy đủ. Mức giá đồ ở đây trung bình từ 40k/ món.', listImage: 'https://scontent.fhan2-4.fna.fbcdn.net/v/t1.0-9/19894929_1727851937254507_562592798261651451_n.jpg?_nc_cat=105&_nc_oc=AQkq6n1qIgPS-HKiR_XSdx6zl8eV8emmDf8DitWFv_oSI26th9DZaVwqaDh1pVPnmUE&_nc_ht=scontent.fhan2-4.fna&oh=5cb59e1939491c23de9228f67227de8a&oe=5D4163F0;https://scontent.fhan2-2.fna.fbcdn.net/v/t1.0-9/26196123_1712890375417330_6363366011220792421_n.jpg?_nc_cat=106&_nc_oc=AQmiDHoe5HyHWsF8UcPEqOfn5kw6tXs5wogeWtp_SFkJuu8b_YXUu8c9KvGCYdKKwRg&_nc_ht=scontent.fhan2-2.fna&oh=1c0efdc00845b672eaa1c10ad66851fe&oe=5D2A8577;https://scontent.fhan2-1.fna.fbcdn.net/v/t1.0-9/27067159_1727855673920800_1490750529629525949_n.jpg?_nc_cat=102&_nc_oc=AQmngfrEMchuXeZvmqeWH_zIXS3z_a3OuY2ElofC2lf5H2C3qb3OrLO8p3zjJRX-w48&_nc_ht=scontent.fhan2-1.fna&oh=5b8497d805a8f964099f7e6f308b1fc8&oe=5D775640;https://scontent.fhan2-1.fna.fbcdn.net/v/t1.0-9/21430343_787903304722596_5819259620711457202_n.jpg?_nc_cat=102&_nc_oc=AQkFW_yg0DAzXT_g5RAy6eLDDluoIgbG0GPGnCMRFAfytldEUqPvq3zTXsgZsqt9WRY&_nc_ht=scontent.fhan2-1.fna&oh=100a39411f6ca734fe4010d9e0b71a0f&oe=5D6F986E;https://scontent.fhan2-4.fna.fbcdn.net/v/t1.0-9/26731167_857953407717585_6679630345867683902_n.jpg?_nc_cat=104&_nc_oc=AQntZ-6ejfMl6yAUJdUpspCAUbQsP8CtXX3j2P0EE8JMSgejBCXsoWK5tGqs66vtuNg&_nc_ht=scontent.fhan2-4.fna&oh=5f02511c455e095bc5ff5b472e8e6ae7&oe=5D33C2A4;https://scontent.fhan2-1.fna.fbcdn.net/v/t1.0-9/27067159_1727855673920800_1490750529629525949_n.jpg?_nc_cat=102&_nc_oc=AQmngfrEMchuXeZvmqeWH_zIXS3z_a3OuY2ElofC2lf5H2C3qb3OrLO8p3zjJRX-w48&_nc_ht=scontent.fhan2-1.fna&oh=5b8497d805a8f964099f7e6f308b1fc8&oe=5D775640',  time: '21 giờ trước',
    },
    {
        imageMain: 'https://dulichtoday.vn/wp-content/uploads/2018/04/Lagom-Homestay-1.jpg',
        namePlace: 'Lagom Homestay',
        coordinates: '21.0251967,105.7966571', address: '317 Nguyễn Khang, Yên Hoà, Cầu Giấy, Hà Nội', describe: 'Trill Group với tổ hợp cafe, bể bơi, phòng tập gym... - là một trong những quán cà phê đẹp ở Hà Nội có view cực chất.Nếu bạn thích trải nghiệm cảm giác vừa nhâm nhi tách trà ấm nóng hay chocolate ngọt ngào, vừa ngắm Hà Nội từ trên cao thì Trill Group là sự lựa chọn không thể lý tưởng hơn.Ngoài yếu tố không gian view và không gian, đồ ăn, đồ uống ở đây cũng khá đầy đủ. Mức giá đồ ở đây trung bình từ 40k/ món.', listImage: 'https://scontent.fhan2-4.fna.fbcdn.net/v/t1.0-9/19894929_1727851937254507_562592798261651451_n.jpg?_nc_cat=105&_nc_oc=AQkq6n1qIgPS-HKiR_XSdx6zl8eV8emmDf8DitWFv_oSI26th9DZaVwqaDh1pVPnmUE&_nc_ht=scontent.fhan2-4.fna&oh=5cb59e1939491c23de9228f67227de8a&oe=5D4163F0;https://scontent.fhan2-2.fna.fbcdn.net/v/t1.0-9/26196123_1712890375417330_6363366011220792421_n.jpg?_nc_cat=106&_nc_oc=AQmiDHoe5HyHWsF8UcPEqOfn5kw6tXs5wogeWtp_SFkJuu8b_YXUu8c9KvGCYdKKwRg&_nc_ht=scontent.fhan2-2.fna&oh=1c0efdc00845b672eaa1c10ad66851fe&oe=5D2A8577;https://scontent.fhan2-1.fna.fbcdn.net/v/t1.0-9/27067159_1727855673920800_1490750529629525949_n.jpg?_nc_cat=102&_nc_oc=AQmngfrEMchuXeZvmqeWH_zIXS3z_a3OuY2ElofC2lf5H2C3qb3OrLO8p3zjJRX-w48&_nc_ht=scontent.fhan2-1.fna&oh=5b8497d805a8f964099f7e6f308b1fc8&oe=5D775640;https://scontent.fhan2-1.fna.fbcdn.net/v/t1.0-9/21430343_787903304722596_5819259620711457202_n.jpg?_nc_cat=102&_nc_oc=AQkFW_yg0DAzXT_g5RAy6eLDDluoIgbG0GPGnCMRFAfytldEUqPvq3zTXsgZsqt9WRY&_nc_ht=scontent.fhan2-1.fna&oh=100a39411f6ca734fe4010d9e0b71a0f&oe=5D6F986E;https://scontent.fhan2-4.fna.fbcdn.net/v/t1.0-9/26731167_857953407717585_6679630345867683902_n.jpg?_nc_cat=104&_nc_oc=AQntZ-6ejfMl6yAUJdUpspCAUbQsP8CtXX3j2P0EE8JMSgejBCXsoWK5tGqs66vtuNg&_nc_ht=scontent.fhan2-4.fna&oh=5f02511c455e095bc5ff5b472e8e6ae7&oe=5D33C2A4;https://scontent.fhan2-1.fna.fbcdn.net/v/t1.0-9/27067159_1727855673920800_1490750529629525949_n.jpg?_nc_cat=102&_nc_oc=AQmngfrEMchuXeZvmqeWH_zIXS3z_a3OuY2ElofC2lf5H2C3qb3OrLO8p3zjJRX-w48&_nc_ht=scontent.fhan2-1.fna&oh=5b8497d805a8f964099f7e6f308b1fc8&oe=5D775640',  time: '21 giờ trước',
    },
    {
        imageMain: 'https://scontent.fhan1-1.fna.fbcdn.net/v/t1.0-9/36652441_403569323485035_1877483015723024384_n.jpg?_nc_cat=106&_nc_oc=AQnV4Ujj8ZnEoFuw82dSqlGG7Ct6hNPUIDEJjIzuZz_wL42i5YWECHwFg7qX7EfIWnE&_nc_ht=scontent.fhan1-1.fna&oh=7779b2bd318820e45df4f45b3ab3616f&oe=5D3BA4C3',
        namePlace: 'Arena Village',
        coordinates: '21.0251967,105.7966571', address: '317 Nguyễn Khang, Yên Hoà, Cầu Giấy, Hà Nội', describe: 'Trill Group với tổ hợp cafe, bể bơi, phòng tập gym... - là một trong những quán cà phê đẹp ở Hà Nội có view cực chất.Nếu bạn thích trải nghiệm cảm giác vừa nhâm nhi tách trà ấm nóng hay chocolate ngọt ngào, vừa ngắm Hà Nội từ trên cao thì Trill Group là sự lựa chọn không thể lý tưởng hơn.Ngoài yếu tố không gian view và không gian, đồ ăn, đồ uống ở đây cũng khá đầy đủ. Mức giá đồ ở đây trung bình từ 40k/ món.', listImage: 'https://scontent.fhan2-4.fna.fbcdn.net/v/t1.0-9/19894929_1727851937254507_562592798261651451_n.jpg?_nc_cat=105&_nc_oc=AQkq6n1qIgPS-HKiR_XSdx6zl8eV8emmDf8DitWFv_oSI26th9DZaVwqaDh1pVPnmUE&_nc_ht=scontent.fhan2-4.fna&oh=5cb59e1939491c23de9228f67227de8a&oe=5D4163F0;https://scontent.fhan2-2.fna.fbcdn.net/v/t1.0-9/26196123_1712890375417330_6363366011220792421_n.jpg?_nc_cat=106&_nc_oc=AQmiDHoe5HyHWsF8UcPEqOfn5kw6tXs5wogeWtp_SFkJuu8b_YXUu8c9KvGCYdKKwRg&_nc_ht=scontent.fhan2-2.fna&oh=1c0efdc00845b672eaa1c10ad66851fe&oe=5D2A8577;https://scontent.fhan2-1.fna.fbcdn.net/v/t1.0-9/27067159_1727855673920800_1490750529629525949_n.jpg?_nc_cat=102&_nc_oc=AQmngfrEMchuXeZvmqeWH_zIXS3z_a3OuY2ElofC2lf5H2C3qb3OrLO8p3zjJRX-w48&_nc_ht=scontent.fhan2-1.fna&oh=5b8497d805a8f964099f7e6f308b1fc8&oe=5D775640;https://scontent.fhan2-1.fna.fbcdn.net/v/t1.0-9/21430343_787903304722596_5819259620711457202_n.jpg?_nc_cat=102&_nc_oc=AQkFW_yg0DAzXT_g5RAy6eLDDluoIgbG0GPGnCMRFAfytldEUqPvq3zTXsgZsqt9WRY&_nc_ht=scontent.fhan2-1.fna&oh=100a39411f6ca734fe4010d9e0b71a0f&oe=5D6F986E;https://scontent.fhan2-4.fna.fbcdn.net/v/t1.0-9/26731167_857953407717585_6679630345867683902_n.jpg?_nc_cat=104&_nc_oc=AQntZ-6ejfMl6yAUJdUpspCAUbQsP8CtXX3j2P0EE8JMSgejBCXsoWK5tGqs66vtuNg&_nc_ht=scontent.fhan2-4.fna&oh=5f02511c455e095bc5ff5b472e8e6ae7&oe=5D33C2A4;https://scontent.fhan2-1.fna.fbcdn.net/v/t1.0-9/27067159_1727855673920800_1490750529629525949_n.jpg?_nc_cat=102&_nc_oc=AQmngfrEMchuXeZvmqeWH_zIXS3z_a3OuY2ElofC2lf5H2C3qb3OrLO8p3zjJRX-w48&_nc_ht=scontent.fhan2-1.fna&oh=5b8497d805a8f964099f7e6f308b1fc8&oe=5D775640',  time: '21 giờ trước',
    },
    {
        imageMain: 'https://dulichtoday.vn/wp-content/uploads/2018/04/Tropical-Nordic-House.jpg',
        namePlace: 'Tropical Nordic House',
        coordinates: '21.0251967,105.7966571', address: '317 Nguyễn Khang, Yên Hoà, Cầu Giấy, Hà Nội', describe: 'Trill Group với tổ hợp cafe, bể bơi, phòng tập gym... - là một trong những quán cà phê đẹp ở Hà Nội có view cực chất.Nếu bạn thích trải nghiệm cảm giác vừa nhâm nhi tách trà ấm nóng hay chocolate ngọt ngào, vừa ngắm Hà Nội từ trên cao thì Trill Group là sự lựa chọn không thể lý tưởng hơn.Ngoài yếu tố không gian view và không gian, đồ ăn, đồ uống ở đây cũng khá đầy đủ. Mức giá đồ ở đây trung bình từ 40k/ món.', listImage: 'https://scontent.fhan2-4.fna.fbcdn.net/v/t1.0-9/19894929_1727851937254507_562592798261651451_n.jpg?_nc_cat=105&_nc_oc=AQkq6n1qIgPS-HKiR_XSdx6zl8eV8emmDf8DitWFv_oSI26th9DZaVwqaDh1pVPnmUE&_nc_ht=scontent.fhan2-4.fna&oh=5cb59e1939491c23de9228f67227de8a&oe=5D4163F0;https://scontent.fhan2-2.fna.fbcdn.net/v/t1.0-9/26196123_1712890375417330_6363366011220792421_n.jpg?_nc_cat=106&_nc_oc=AQmiDHoe5HyHWsF8UcPEqOfn5kw6tXs5wogeWtp_SFkJuu8b_YXUu8c9KvGCYdKKwRg&_nc_ht=scontent.fhan2-2.fna&oh=1c0efdc00845b672eaa1c10ad66851fe&oe=5D2A8577;https://scontent.fhan2-1.fna.fbcdn.net/v/t1.0-9/27067159_1727855673920800_1490750529629525949_n.jpg?_nc_cat=102&_nc_oc=AQmngfrEMchuXeZvmqeWH_zIXS3z_a3OuY2ElofC2lf5H2C3qb3OrLO8p3zjJRX-w48&_nc_ht=scontent.fhan2-1.fna&oh=5b8497d805a8f964099f7e6f308b1fc8&oe=5D775640;https://scontent.fhan2-1.fna.fbcdn.net/v/t1.0-9/21430343_787903304722596_5819259620711457202_n.jpg?_nc_cat=102&_nc_oc=AQkFW_yg0DAzXT_g5RAy6eLDDluoIgbG0GPGnCMRFAfytldEUqPvq3zTXsgZsqt9WRY&_nc_ht=scontent.fhan2-1.fna&oh=100a39411f6ca734fe4010d9e0b71a0f&oe=5D6F986E;https://scontent.fhan2-4.fna.fbcdn.net/v/t1.0-9/26731167_857953407717585_6679630345867683902_n.jpg?_nc_cat=104&_nc_oc=AQntZ-6ejfMl6yAUJdUpspCAUbQsP8CtXX3j2P0EE8JMSgejBCXsoWK5tGqs66vtuNg&_nc_ht=scontent.fhan2-4.fna&oh=5f02511c455e095bc5ff5b472e8e6ae7&oe=5D33C2A4;https://scontent.fhan2-1.fna.fbcdn.net/v/t1.0-9/27067159_1727855673920800_1490750529629525949_n.jpg?_nc_cat=102&_nc_oc=AQmngfrEMchuXeZvmqeWH_zIXS3z_a3OuY2ElofC2lf5H2C3qb3OrLO8p3zjJRX-w48&_nc_ht=scontent.fhan2-1.fna&oh=5b8497d805a8f964099f7e6f308b1fc8&oe=5D775640',  time: '21 giờ trước',
    },
    {
        imageMain: 'https://dulichtoday.vn/wp-content/uploads/2018/04/Au-frais.jpg',
        namePlace: 'Au frais',
        coordinates: '21.0251967,105.7966571', address: '317 Nguyễn Khang, Yên Hoà, Cầu Giấy, Hà Nội', describe: 'Trill Group với tổ hợp cafe, bể bơi, phòng tập gym... - là một trong những quán cà phê đẹp ở Hà Nội có view cực chất.Nếu bạn thích trải nghiệm cảm giác vừa nhâm nhi tách trà ấm nóng hay chocolate ngọt ngào, vừa ngắm Hà Nội từ trên cao thì Trill Group là sự lựa chọn không thể lý tưởng hơn.Ngoài yếu tố không gian view và không gian, đồ ăn, đồ uống ở đây cũng khá đầy đủ. Mức giá đồ ở đây trung bình từ 40k/ món.', listImage: 'https://scontent.fhan2-4.fna.fbcdn.net/v/t1.0-9/19894929_1727851937254507_562592798261651451_n.jpg?_nc_cat=105&_nc_oc=AQkq6n1qIgPS-HKiR_XSdx6zl8eV8emmDf8DitWFv_oSI26th9DZaVwqaDh1pVPnmUE&_nc_ht=scontent.fhan2-4.fna&oh=5cb59e1939491c23de9228f67227de8a&oe=5D4163F0;https://scontent.fhan2-2.fna.fbcdn.net/v/t1.0-9/26196123_1712890375417330_6363366011220792421_n.jpg?_nc_cat=106&_nc_oc=AQmiDHoe5HyHWsF8UcPEqOfn5kw6tXs5wogeWtp_SFkJuu8b_YXUu8c9KvGCYdKKwRg&_nc_ht=scontent.fhan2-2.fna&oh=1c0efdc00845b672eaa1c10ad66851fe&oe=5D2A8577;https://scontent.fhan2-1.fna.fbcdn.net/v/t1.0-9/27067159_1727855673920800_1490750529629525949_n.jpg?_nc_cat=102&_nc_oc=AQmngfrEMchuXeZvmqeWH_zIXS3z_a3OuY2ElofC2lf5H2C3qb3OrLO8p3zjJRX-w48&_nc_ht=scontent.fhan2-1.fna&oh=5b8497d805a8f964099f7e6f308b1fc8&oe=5D775640;https://scontent.fhan2-1.fna.fbcdn.net/v/t1.0-9/21430343_787903304722596_5819259620711457202_n.jpg?_nc_cat=102&_nc_oc=AQkFW_yg0DAzXT_g5RAy6eLDDluoIgbG0GPGnCMRFAfytldEUqPvq3zTXsgZsqt9WRY&_nc_ht=scontent.fhan2-1.fna&oh=100a39411f6ca734fe4010d9e0b71a0f&oe=5D6F986E;https://scontent.fhan2-4.fna.fbcdn.net/v/t1.0-9/26731167_857953407717585_6679630345867683902_n.jpg?_nc_cat=104&_nc_oc=AQntZ-6ejfMl6yAUJdUpspCAUbQsP8CtXX3j2P0EE8JMSgejBCXsoWK5tGqs66vtuNg&_nc_ht=scontent.fhan2-4.fna&oh=5f02511c455e095bc5ff5b472e8e6ae7&oe=5D33C2A4;https://scontent.fhan2-1.fna.fbcdn.net/v/t1.0-9/27067159_1727855673920800_1490750529629525949_n.jpg?_nc_cat=102&_nc_oc=AQmngfrEMchuXeZvmqeWH_zIXS3z_a3OuY2ElofC2lf5H2C3qb3OrLO8p3zjJRX-w48&_nc_ht=scontent.fhan2-1.fna&oh=5b8497d805a8f964099f7e6f308b1fc8&oe=5D775640',  time: '21 giờ trước',
    },
]
export const DATA_TOPIC = [
    {
        source: 'http://2sao.vietnamnetjsc.vn/images/2017/10/19/09/21/kieu-trinh.jpg',
        title: 'Mẹo nhỏ sống ảo',
        selected: false,
    },
    {
        source: 'https://znews-photo.zadn.vn/w660/Uploaded/jaroin/2016_12_06/1.jpg',
        title: 'Quán cà phê',
        selected: false,
    },
    {
        source: 'http://xuongbanghecafe.com/wp-content/uploads/2017/10/ban-ghe-Quan-tra-sua-dep-2.jpg',
        title: 'Quán trà sữa',
        selected: false,
    },
    {
        source: 'https://www.flynow.vn/blog/wp-content/uploads/2017/08/20768108_1535042529888577_6126814775052528550_n.jpg',
        title: 'Homestay',
        selected: false,
    },
    {
        source: 'https://images.foody.vn/images/12642880_1109539962413140_5293999953244283149_n.jpg',
        title: 'Rạp chiếu phim mini',
        selected: false,
    },
    {
        source: 'http://static2.yan.vn/YanNews/2167221/201709/20170924-100752-20170922-105857-duc_8963-edit_600x424_600x424.jpg',
        title: 'Khác',
        selected: false,
    },
]
export const LIST_DATA_TOPIC = [
    {
        title: 'Điểm Mặt Những Quán Cà Phê Đẹp Mê Ly',
        data: LIST_CAFE,
    },
    {
        title: 'Check-in Những Quán Trà Sữa Cực Chất',
        data: LIST_TRA_SUA,
    },
    {
        title: 'Lạc Vào Trong Những Homestay Đầy Thơ Mộng',
        data: LIST_HOME_STAY,
    },
    {
        title: 'Hẹn Hò Tại Rạp Chiếu Phim mini Đầy Lãng Mạn',
        data: LIST_RAP_CP,
    },
]
