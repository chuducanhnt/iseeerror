import {combineReducers} from 'redux';
import rootReducer from './rootReducer';
import accountReducer from './accountReducer';

export default reducer = combineReducers({
    rootReducer:rootReducer,
    accountReducer:accountReducer
});