import React, { Component } from 'react'
import {
    View, StyleSheet, ScrollView, TouchableOpacity
} from 'react-native'
//import
import STYLES from '../config/styles.config';
import colors from '../config/colors';
import { WIDTH, HEIGHT, calListImage } from '../config/Function';
import ItemImage from './ItemImage';

type Props = {
    title: string,
    onButton: Function,
}
export default ListImageCustom = (props) => {
    const { data, onButton, widthImage, marginTop, widthContainer, paddingHorizontal } = props;
    let listLeft = [];
    let listRight = [];
    data.map((value, index) => {
        if (index <= 3) {
            if (index % 2 == 0)
                listLeft.push(
                    <ItemImage
                        key={index}
                        marginTop={index==0 ? 0 : marginTop}
                        widthImage={widthImage}
                        content={value}
                        onButton={() => onButton()}
                        index={index}
                    />
                )
            else
                listRight.push(
                    <ItemImage
                        key={index}
                        marginTop={index==1 ? 0 : marginTop}
                        widthImage={widthImage}
                        content={value}
                        onButton={() => onButton()}
                        index={index}
                    />
                )
            }
        })

return (
    <View style={{
        width: widthContainer,
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingHorizontal: paddingHorizontal,
    }}>
        <View style={{
            flex: 0,
            flexDirection: 'column',
            width: widthImage,
        }}>
            {listLeft}
        </View>
        <View style={{
            flex: 0,
            flexDirection: 'column',
            width: widthImage,
        }}>
            {listRight}
        </View>
    </View>

)
}
const styles = StyleSheet.create({
    container: {
        minHeight: HEIGHT(200),
        width: WIDTH(360),
        justifyContent: 'center',
        alignItems: 'center',
    },
    text: {
        color: colors.red,
        fontSize: STYLES.fontSizeText,
    },
})

