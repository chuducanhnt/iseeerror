/**
 * This component to show account
 * @huanhtm
 */
import React, { Component } from 'react'
import {
    View, StyleSheet, Image, Text, ScrollView
} from 'react-native'

import { connect } from 'react-redux';

//import common

import { HEIGHT, WIDTH } from '../config/Function';
import ItemForAll from '../common/ItemForAll';
import FluteButton from '../common/FluteButton';


class SampleViewAll extends Component<Props> {
    onPost = (content) => {
        this.props.screenProps.navigate('Details',{content:content})
    }
    render() {
        const {content} =this.props;
        return (
            <View style={{
                flex:1,
            }}>
                <ScrollView
                    showsHorizontalScrollIndicator={false}
                    showsVerticalScrollIndicator={false}
                    style={{flex:1}}>
                    <View style={{
                        flex: 1,
                        width:WIDTH(360),
                        marginTop: HEIGHT(10),
                    }}>
                        {
                            content.data.map((value, index) => {
                                return (
                                    <ItemForAll
                                        key={index}
                                        content={value}
                                        onButton={()=>this.onPost(value)}
                                    />
                                )
                            })
                        }
                    </View>
                </ScrollView>
                <FluteButton
                    onButton={()=> this.props.screenProps.navigate("Profile")}
                />
            </View>
        )
    }
}
function mapStateToProps(state) {
    return {
        count: state.rootReducer.count,
    };
}

export default connect(mapStateToProps, {
})(SampleViewAll);