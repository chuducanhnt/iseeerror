import React, { Component } from 'react'
import {
    Text, View, StyleSheet, ImageBackground,TouchableOpacity
} from 'react-native'
import { Button } from 'react-native-elements';
import Ionicons from 'react-native-vector-icons/Ionicons';
//import
import IMAGE from '../../assets/image';
import STYLES from '../../config/styles.config';
import colors from '../../config/colors';
import TextBold from '../TextBold';
import TextNomal from '../TextNomal';

function WIDTH(x){
    return x*STYLES.widthScreen/360;
}
function HEIGHT(x){
    return x*STYLES.heightScreen/640;
}
type Props = {
    data: string,
    onButton: Function,
}
export default ListImage = (props: any) => {
    const { data,onButton } = props;
    return (
        <View style={styles.container}>
            {
                data.map((value,index)=>{
                    return(
                        <TouchableOpacity 
                            onPress={()=>onButton(index)}
                        style={[{
                            width:WIDTH(110),
                            marginBottom:WIDTH(7.5),
                            justifyContent:'center',
                            alignItems:'center',
                        },index % 3 !=2 ? {marginRight:WIDTH(7.5)} : {}]}>
                            <ImageBackground
                                source={{uri:value.source}}   
                                style={{
                                    width:WIDTH(110),
                                    height:WIDTH(110),
                                    justifyContent:'space-between',
                                    paddingVertical:HEIGHT(5),
                                    paddingHorizontal:WIDTH(5),
                                }} 
                                imageStyle={{
                                    borderRadius:WIDTH(5),
                                }}
                            >
                                {
                                    value.selected && <Ionicons name={'ios-checkmark-circle'} size={26 * STYLES.heightScreen / 640} color="#fff" />
                                }
                                {
                                    !value.selected && <View style={{width:WIDTH(26)}}/>
                                }
                                <TextBold
                                    content={value.title}
                                    fontSize={STYLES.fontSizeNormalText}
                                    color={colors.white}
                                />
                            </ImageBackground>
                        </TouchableOpacity>
                    )
                })
            }
        </View>
    )
}
const styles = StyleSheet.create({
    container: {
        //flex: 1,
        marginTop: 100*STYLES.heightScreen/640,
        width:STYLES.widthScreen,
        flexDirection: 'row',
        justifyContent:'center',
        alignItems:'center',
        flexWrap:'wrap',
    },
    text: {
        color: colors.colorWhite,
        fontSize: STYLES.fontSizeText,
        textAlign: "center",
        textAlignVertical: "center",
        fontFamily: 'Roboto-Regular',
    },
})

