import React, { Component } from 'react'
import {
    Text, View, StyleSheet, Image,
} from 'react-native'
import { Button } from 'react-native-elements';
//import
import IMAGE from '../../assets/image';
import STYLES from '../../config/styles.config';
import colors from '../../config/colors';
import TextBold from '../TextBold';
import TextNomal from '../TextNomal';

type Props = {
    title: string,
    onButton: Function,
}
export default ViewWelcome = (props: any) => {
    const { onButton,disabled } = props;
    return (
        <View style={styles.container}>
            <View style={{
                flex:0,
                justifyContent:'space-between',
                alignItems:'center',
                flexDirection:'row',
                marginBottom:5*STYLES.heightScreen/640,
            }}>
                <TextBold
                    content='Chào mừng bạn đến với I See' 
                    fontSize={STYLES.fontSize25}
                    color={colors.colorBlack}
                    width={235*STYLES.widthScreen/360}
                />
                <Button
                    onPress={()=>onButton()}
                    title="Tiếp theo"
                    disabled={disabled}
                    buttonStyle={{
                        width:STYLES.widthScreen*0.25,
                        backgroundColor:disabled ? colors.lightGray :colors.colorPink
                    }}
                />
            </View>
            <TextNomal
                    content='Chọn 3 chủ đề trở lên' 
                    fontSize={STYLES.fontSizeChatbot}
                    color={colors.colorBlack}
            />
        </View>
    )
}
const styles = StyleSheet.create({
    container: {
        flex: 0,
        marginTop: 20*STYLES.heightScreen/640,
        width:STYLES.widthScreen,
        paddingHorizontal:15*STYLES.widthScreen/360,
    },
    text: {
        color: colors.colorWhite,
        fontSize: STYLES.fontSizeText,
        textAlign: "center",
        textAlignVertical: "center",
        fontFamily: 'Roboto-Regular',
    },
})

