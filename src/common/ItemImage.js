import React, { Component } from 'react'
import {
    Text, View, StyleSheet,ActivityIndicator
} from 'react-native'
import { TouchableOpacity } from '@shoutem/ui';
import { Image } from 'react-native-elements';
import FastImage from 'react-native-fast-image';
//import
import IMAGE from '../assets/image';
import STYLES from '../config/styles.config';
import colors from '../config/colors';
import { WIDTH, HEIGHT } from '../config/Function';

type Props = {
    title: string,
    onButton: Function,
}
export default ItemImage = (props: any) => {
    const { content, onButton, index,marginTop,widthImage} = props;
    return (
        <TouchableOpacity
            onPress={()=>onButton()}
            style={{
                flex:0,
                marginTop,
            }}
        >
            <FastImage
                source={{ uri: content,priority: FastImage.priority.high, }}
                style={{
                     width: widthImage,
                     height: (index % 4==1 || index % 4==2) ? HEIGHT(120) : HEIGHT(160)   
                }}
            >
            </FastImage>
        </TouchableOpacity>

    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    text: {
        color: colors.colorWhite,
        fontSize: STYLES.fontSizeText,
        textAlign: "center",
        textAlignVertical: "center",
        fontFamily: 'Roboto-Regular',
    },
})

