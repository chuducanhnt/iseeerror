import React, { Component } from 'react'
import {
    Text, View, StyleSheet,
} from 'react-native'
import { Card, Image, Subtitle, Caption, Button, Icon, TouchableOpacity } from '@shoutem/ui';
//import
import IMAGE from '../assets/image';
import STYLES from '../config/styles.config';
import colors from '../config/colors';
import { WIDTH } from '../config/Function';

type Props = {
    title: string,
    onButton: Function,
}
export default ItemPost = (props: any) => {
    const { content, onButton, styleImage="medium-portrait" } = props;
    return (
        <TouchableOpacity
            onPress={()=>onButton()}
            style={{flex:0, marginHorizontal:WIDTH(5)}}
        >
            <Card>
                <Image
                    styleName={styleImage}
                    source={{ uri: content.imageMain }}
                />
                <View styleName="content">
                    <Subtitle
                        numberOfLines={1}
                    >{content.namePlace}</Subtitle>
                    <View styleName="horizontal v-center space-between">
                        <Caption>{content.time}</Caption>
                    </View>
                </View>
            </Card>
        </TouchableOpacity>

    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    text: {
        color: colors.colorWhite,
        fontSize: STYLES.fontSizeText,
        textAlign: "center",
        textAlignVertical: "center",
        fontFamily: 'Roboto-Regular',
    },
})

