import React, { Component } from 'react'
import {
  Text, View, StyleSheet, Image,
} from 'react-native'

//import
import IMAGE from '../assets/image';
import STYLES from '../config/styles.config';
import colors from '../config/colors';

type Props = {
    color:string,
    content:string,
    fontSize:string,
}
export default TextBold = (props: any) => {
    const {color,fontSize,content}= props;  
    return (
        <Text style={[styles.text,{
            color,fontSize
        }]}>{content}</Text>
    )
}
const styles = StyleSheet.create({
      text: {
        color: colors.colorBlack,
        fontSize: STYLES.fontSizeText,
        fontFamily: 'Roboto-Regular',
      },
})

