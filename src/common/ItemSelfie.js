import React, { Component } from 'react'
import {
    Text,StyleSheet,
} from 'react-native'
import { View,Tile, Title, ImageBackground, Caption, Button, Icon, TouchableOpacity } from '@shoutem/ui';
//import
import IMAGE from '../assets/image';
import STYLES from '../config/styles.config';
import colors from '../config/colors';
import { WIDTH } from '../config/Function';

type Props = {
    content: string,
    onButton: Function,
}
export default ItemSelfie = (props: any) => {
    const { content, onButton } = props;
    return (
        <TouchableOpacity
            onPress={() => onButton()}
            style={{ flex: 0 }}
        >
            <ImageBackground
                styleName="large-banner"
                source={{ uri: content.imageMain }}
            >
                <Tile>
                    <View styleName="actions">
                        {/* <Button styleName="tight clear"><Icon name="add-to-favorites" /></Button> */}
                    </View>
                    <Title>{content.namePlace}</Title>
                    <Caption>{content.address}</Caption>
                </Tile>
            </ImageBackground>
        </TouchableOpacity>

    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    text: {
        color: colors.colorWhite,
        fontSize: STYLES.fontSizeText,
        textAlign: "center",
        textAlignVertical: "center",
        fontFamily: 'Roboto-Regular',
    },
})

