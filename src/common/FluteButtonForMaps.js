import React, { Component } from 'react'
import {
    Text, View, StyleSheet, Image, Linking
} from 'react-native'
import ImagePicker from 'react-native-image-picker';
import ActionButton from 'react-native-action-button';
import Foundation from 'react-native-vector-icons/Foundation';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
//import
import IMAGE from '../assets/image';
import STYLES from '../config/styles.config';
import colors from '../config/colors';
import { WIDTH } from '../config/Function';

type Props = {
    title: string,
    onButton: Function,
}
export default FluteButtonForMaps = (props: any) => {
    const { onButton } = props;
    return (
        <ActionButton 
            renderIcon={()=>(
                <MaterialIcons name={'filter-list'} size={WIDTH(20)} color="#000" />
            )}
            buttonColor="#ffff">
            <ActionButton.Item buttonColor='#ffff' title="Tất cả"
                onPress={() => onButton(0)}>
                <Image
                    style={{ width: WIDTH(30), height: WIDTH(30) }}
                    source={IMAGE.logoAll}></Image>
            </ActionButton.Item>
            <ActionButton.Item buttonColor='#ffff' title="Quán cà phê"
                onPress={() => onButton(2)}>
                <Image
                    style={{ width: WIDTH(30), height: WIDTH(30) }}
                    source={IMAGE.logoCafe}></Image>
            </ActionButton.Item>
            <ActionButton.Item buttonColor='#ffff' title="Quán trà sữa"
                 onPress={() => onButton(3)}>
                <Image
                    style={{ width: WIDTH(30), height: WIDTH(30) }}
                    source={IMAGE.logoTraSua}></Image>
            </ActionButton.Item>
            <ActionButton.Item  buttonColor='#ffff' title="Homestay"
                 onPress={() => onButton(4)}>
                 <Image
                    style={{ width: WIDTH(30), height: WIDTH(30) }}
                    source={IMAGE.logoHomeStay}></Image>
            </ActionButton.Item>
            <ActionButton.Item  buttonColor='#ffff' title="Rạp chiếu phim mini"
                 onPress={() => onButton(5)}>
                 <Image
                    style={{ width: WIDTH(30), height: WIDTH(30) }}
                    source={IMAGE.logoRapCP}></Image>
            </ActionButton.Item>
            <ActionButton.Item buttonColor='#ffff' title="Khác"
                 onPress={() => onButton(6)}>
                 <Image
                    style={{ width: WIDTH(30), height: WIDTH(30) }}
                    source={IMAGE.logoKhac}></Image>
            </ActionButton.Item>
        </ActionButton>
    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    text: {
        color: colors.colorWhite,
        fontSize: STYLES.fontSizeText,
        textAlign: "center",
        textAlignVertical: "center",
        fontFamily: 'Roboto-Regular',
    },
})

