import React, { Component } from 'react'
import {
    View, StyleSheet, ScrollView, TouchableOpacity
} from 'react-native'
import Swiper from 'react-native-swiper';
//import
import STYLES from '../../config/styles.config';
import colors from '../../config/colors';
import { WIDTH, HEIGHT } from '../../config/Function';
import ItemSelfie from '../ItemSelfie';

type Props = {
    title: string,
    onButton: Function,
}
export default ListSelfie = (props) => {
    const { data, onButton } = props;
    return (
        <View style={{
            width: WIDTH(360),
            // paddingHorizontal: WIDTH(10),
            marginTop: HEIGHT(20)
        }}>
            <Swiper
                autoplay
                style={styles.container}>
                {
                    data.map((value,index)=>{
                        return (
                            <ItemSelfie
                                key={index}
                                content={value}
                                onButton={() => onButton(value)}
                            />
                        )
                    })
                }
            </Swiper>
        </View>

    )
}
const styles = StyleSheet.create({
    container: {
        minHeight: HEIGHT(200),
        width: WIDTH(360),
        justifyContent: 'center',
        alignItems: 'center',
    },
    text: {
        color: colors.red,
        fontSize: STYLES.fontSizeText,
    },
})

