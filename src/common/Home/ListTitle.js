import React, { Component } from 'react'
import {
    View, StyleSheet, ScrollView, TouchableOpacity
} from 'react-native'
import Swiper from 'react-native-swiper';
import { Text } from '@shoutem/ui';
//import
import STYLES from '../../config/styles.config';
import colors from '../../config/colors';
import { WIDTH, HEIGHT } from '../../config/Function';

type Props = {
    title: string,
    onButton: Function,
}
export default ListTitle = (props) => {
    const { data, onButton,idTitle } = props;
    return (
        <ScrollView
            horizontal={true}
            showsHorizontalScrollIndicator={false}
            showsVerticalScrollIndicator={false}
            style={{
                flexGrow:0,
                flexShrink:0,
                paddingLeft:WIDTH(10),
                marginRight:WIDTH(10),
                paddingVertical:HEIGHT(8),
            }}>
                {
                    data.map((value, index) => {
                        let fontWeight=index==idTitle ? '500' : 'normal';
                        return (
                            <TouchableOpacity
                                onPress={() => onButton(index)}
                                style={{
                                    backgroundColor:'#EFEFEF',
                                    borderRadius: HEIGHT(10),
                                    marginRight:WIDTH(10),
                                    paddingVertical: HEIGHT(5),
                                    paddingHorizontal:WIDTH(10),
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                }}>
                                <Text style={{
                                    fontWeight
                                }}>{value}</Text>
                            </TouchableOpacity>
                        )
                    })
                }
           
        </ScrollView>

    )
}
const styles = StyleSheet.create({
    container: {
        minHeight: HEIGHT(200),
        width: WIDTH(360),
        justifyContent: 'center',
        alignItems: 'center',
    },
    text: {
        color: colors.red,
        fontSize: STYLES.fontSizeText,
    },
})

