import React, { Component } from 'react'
import {
    Text, View, StyleSheet, Image, Linking
} from 'react-native'
import ImagePicker from 'react-native-image-picker';
import ActionButton from 'react-native-action-button';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
//import
import IMAGE from '../assets/image';
import STYLES from '../config/styles.config';
import colors from '../config/colors';
import { WIDTH, HEIGHT } from '../config/Function';

type Props = {
    title: string,
    onButton: Function,
}
export default ExpanderMaps = (props: any) => {
    const { onButton } = props;
    return (
        <ActionButton 
            shadowStyle={{
                marginRight:WIDTH(85),
            }}
            renderIcon={()=>(
                <Ionicons name={'ios-expand'} size={WIDTH(20)} color="#000" />
            )}
            buttonColor="#ffff">
            <ActionButton.Item buttonColor='#ffff' 
                onPress={() => onButton(5000)}>
                <Text>5 Km</Text>
            </ActionButton.Item>
            <ActionButton.Item buttonColor='#ffff' 
                onPress={() => onButton(3000)}>
                <Text>3 Km</Text>
            </ActionButton.Item>
            <ActionButton.Item buttonColor='#ffff' 
                 onPress={() => onButton(1000)}>
                 <Text>1 Km</Text>
            </ActionButton.Item>
            <ActionButton.Item buttonColor='#ffff' 
                 onPress={() => onButton(500)}>
                 <Text>500 m</Text>
            </ActionButton.Item>
        </ActionButton>
    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    text: {
        color: colors.colorWhite,
        fontSize: STYLES.fontSizeText,
        textAlign: "center",
        textAlignVertical: "center",
        fontFamily: 'Roboto-Regular',
    },
})

