import React, { Component } from 'react'
import {
    Text, View, StyleSheet, Image, Linking
} from 'react-native'
import ImagePicker from 'react-native-image-picker';
import ActionButton from 'react-native-action-button';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

//import
import IMAGE from '../assets/image';
import STYLES from '../config/styles.config';
import colors from '../config/colors';
import { WIDTH } from '../config/Function';

type Props = {
    title: string,
    onButton: Function,
}
export default FluteButton = (props: any) => {
    const { onButton } = props;
    return (
        <ActionButton
             buttonColor="rgba(231,76,60,1)">
            <ActionButton.Item buttonColor='#FAD748' title="Camera 360"
                onPress={() => Linking.openURL('https://play.google.com/store/apps/details?id=vStudio.Android.Camera360')}>
                <Image
                    style={{ width: WIDTH(30), height: WIDTH(30) }}
                    source={{ uri: 'https://lh3.googleusercontent.com/ev3sivRB4rH4pr-cPrRWRJB71oHCZK8jlr0CXBYFWsmNqXSWUGYrLlDL5QzwzUZEVGY=s180-rw' }}></Image>
            </ActionButton.Item>
            <ActionButton.Item buttonColor='#5CB7B9' title="B612"
                onPress={() => Linking.openURL('https://play.google.com/store/apps/details?id=com.linecorp.b612.android')}>
                <Image
                    style={{ width: WIDTH(30), height: WIDTH(30) }}
                    source={{ uri: 'https://lh3.googleusercontent.com/eqkbG00skAcMecPqexaLSlZKlmH8OFlKbFwkZ7e8A78s4_8Fuo8xswrYlz0H4YRD8Xg=s180-rw' }}></Image>
            </ActionButton.Item>
            <ActionButton.Item buttonColor='#ffff' title="U Like"
                onPress={() => Linking.openURL('https://play.google.com/store/apps/details?id=com.gorgeous.liteinternational')}>
                <Image
                    style={{ width: WIDTH(30), height: WIDTH(30) }}
                    source={{ uri: 'https://lh3.googleusercontent.com/kcy-J6uHCgtjk4xZwUkMlj93hBnza4cIN-YZ_dYiwIcf701OgXwFt__-_i6ZKGLzRYA=s180-rw' }}></Image>
            </ActionButton.Item>
            <ActionButton.Item buttonColor='#ffff' title="Camera"
                onPress={() => {
                    const options = {
                        quality: 1.0,
                        maxWidth: 500,
                        maxHeight: 500,
                        storageOptions: {
                            skipBackup: true,
                        },
                    };
                    ImagePicker.launchCamera(options, (response) => {
                    })
                }
                }>
                <Ionicons name={'md-camera'} size={WIDTH(25)} color="#000" />
            </ActionButton.Item>
            <ActionButton.Item buttonColor='#ffff' title="Tài khoản"
                onPress={() => onButton()}>
                <MaterialIcons name={'account-circle'} size={WIDTH(30)} color="#000" />
            </ActionButton.Item>
        </ActionButton>
    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    text: {
        color: colors.colorWhite,
        fontSize: STYLES.fontSizeText,
        textAlign: "center",
        textAlignVertical: "center",
        fontFamily: 'Roboto-Regular',
    },
})

