import React, { Component } from 'react'
import {
    Text, View, StyleSheet, Linking
} from 'react-native'
import { Card, Image, Subtitle, Caption, Title, Icon, TouchableOpacity } from '@shoutem/ui';
//import
import IMAGE from '../assets/image';
import STYLES from '../config/styles.config';
import colors from '../config/colors';
import { WIDTH, HEIGHT, calListImage } from '../config/Function';
import ListImageCustom from './ListImageCustom';
import Ionicons from 'react-native-vector-icons/Ionicons';
type Props = {
    title: string,
    onButton: Function,
}
export default ItemForAll = (props: any) => {
    const { content, onButton } = props;
    let linkImage = calListImage(content);
    return (
        <View

            style={{
                marginVertical: HEIGHT(5),
                width: WIDTH(360), paddingHorizontal: WIDTH(10),
                justifyContent: 'center',
                alignItems: 'center',
            }}
        >
            <View style={{
                flex: 0,
                borderBottomRightRadius: WIDTH(5),
                borderBottomLeftRadius: WIDTH(5),
                backgroundColor: '#ffff',
                elevation: 3,

            }}>
                <TouchableOpacity
                    onPress={() => onButton()}
                    style={{ flex: 0 }}
                >
                    <ListImageCustom
                        data={calListImage(content)}
                        onButton={(value) => onButton(value)}
                        widthImage={WIDTH(165)}
                        marginTop={WIDTH(10)}
                        widthContainer={WIDTH(340)}
                        paddingHorizontal={0}
                    />
                    <View style={{
                        flex: 0,
                        paddingHorizontal: WIDTH(10)
                    }}>
                        <Subtitle
                            numberOfLines={1}
                        >{content.namePlace}</Subtitle>
                        <Caption>{content.address}</Caption>
                    </View>
                </TouchableOpacity>

                <TouchableOpacity
                    onPress={() => {
                        let res = 'google.navigation:q=' + content.coordinates;
                        Linking.openURL(res);
                    }}
                    style={{
                        flex: 0,
                        flexDirection: 'row',
                        paddingHorizontal: WIDTH(10),
                        alignItems: 'center',
                        //  justifyContent: 'center',
                    }}
                >
                    <Caption>{'Chỉ đường'}</Caption>
                    <View style={{
                        marginLeft: WIDTH(3),
                        backgroundColor: colors.white,
                        height: WIDTH(35),
                        width: WIDTH(35),
                        alignItems: 'center',
                        justifyContent: 'center',
                        borderRadius: WIDTH(35),
                    }}>
                        <Ionicons name={'md-open'} size={WIDTH(20)} color="red" />
                    </View>

                </TouchableOpacity>
            </View>
        </View>

    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    text: {
        color: colors.colorWhite,
        fontSize: STYLES.fontSizeText,
        textAlign: "center",
        textAlignVertical: "center",
        fontFamily: 'Roboto-Regular',
    },
})

