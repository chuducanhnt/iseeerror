import React, { Component } from 'react'
import {
    Text, StyleSheet, TouchableWithoutFeedback
} from 'react-native'
import Ionicons from 'react-native-vector-icons/Ionicons';
import { View, NavigationBar, Title, ImageBackground, Caption, Button, TouchableOpacity, } from '@shoutem/ui';
//import
import IMAGE from '../assets/image';
import STYLES from '../config/styles.config';
import colors from '../config/colors';
import { WIDTH, HEIGHT } from '../config/Function';

type Props = {
    content: string,
    onButton: Function,
}
export default ItemForDetails = (props: any) => {
    const { content, linkImage, onButton, onTouchable, index ,onShowMaps} = props;
    return (
        <TouchableWithoutFeedback onPress={() => onTouchable(true, index)}>
            <ImageBackground
                source={{ uri: linkImage }}
                style={{ width: WIDTH(360), height: HEIGHT(220) }}
            >
                <NavigationBar
                    styleName="clear"
                    leftComponent={(
                        <Button
                            onPress={() => onButton()}
                        >
                            <Ionicons name={'md-arrow-round-back'} size={HEIGHT(26)} color="#FFFF" />
                        </Button>
                    )}
                    rightComponent={(
                        <TouchableOpacity
                            onPress={() => onShowMaps()}
                            style={{
                                flex: 0,
                                marginRight:WIDTH(10),
                                flexDirection:'row',
                                alignItems: 'center',
                                 justifyContent: 'center',
                            }}
                        >
                            <Title style={{ color: '#ffff' }}>{'Chỉ đường'}</Title>
                            <View style={{
                                marginLeft:WIDTH(3),
                                backgroundColor: colors.white,
                                height:WIDTH(35),
                                width:WIDTH(35),
                                alignItems: 'center',
                                justifyContent: 'center',
                                borderRadius:WIDTH(35),
                            }}>
                                <Ionicons name={'md-open'} size={WIDTH(20)} color="red" />
                            </View>
                            
                        </TouchableOpacity>
                    )}
                />

                <Title style={{ color: '#ffff' }}>{content.namePlace}</Title>
                <Caption style={{ color: '#ffff' }}>{content.address}</Caption>
            </ImageBackground>
        </TouchableWithoutFeedback>
    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    text: {
        color: colors.colorWhite,
        fontSize: STYLES.fontSizeText,
        textAlign: "center",
        textAlignVertical: "center",
        fontFamily: 'Roboto-Regular',
    },
})

